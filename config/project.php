<?php

return [
    'default_locale' => 'ru',
    'locales' => ['ru', 'en', 'kg'],
    'news_images_upload_path' => storage_path() . '/app/public/news_images',
    'media_upload_path' => storage_path() . '/app/public/uploaded_images',
    'tokenTelegramBot' => '663127863:AAHSCC0UKoDgeb-7nla3uG0LGliInrpeo_w',
    'chatId' => '-260960790',

    'default_currency' => 'сом',
    'currencies' => ['сом','usd'],


    'localesData' => [
        [
            'locale' => 'ru',
            'desc' => 'Русский'
        ],

        [
            'locale' => 'kg',
            'desc' => 'Кыргызский'
        ],

        [
            'locale' => 'en',
            'desc' => 'Английский'
        ],
    ],

    'menus' => [
        'top' => [
            'title' => 'Верхнее меню',
            'slug' => ''
        ],

        'footer' => [
            'title' => 'Футер',
            'slug' => ''
        ]
    ]
];