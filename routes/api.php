<?php

use App\Http\Resources\ServiceResource;
use App\Models\Service;
use Illuminate\Support\Facades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API для фронта(Vue or etc).
|--------------------------------------------------------------------------
| Функции возвращают ответ в формате JSON
|
| Пример успешного ответа:
|
| {
|    "success": true,
|    "data": {}
| }
|
*/

/*
|--------------------------------------------------------------------------
| Главная страница (StarterKit example home page)
|--------------------------------------------------------------------------
*/

//$router->get('v1/auth/logined', ['uses' => 'Api\AuthController@postRegister']);

//Route::get('v1/auth/login','Api\AuthController@getRegister');
//Route::get("v1","Api\AuthController@getRegister");
//Route::post("v1/auth/register", "Api\AuthController@postRegister")->middleware('cors');
//$router->post('v1/auth/login', ['uses' => 'Api\AuthController@login'])->middleware('cors');
Route::group(['middleware' => ['web']], function () {
    Route::get('login/{provider}', 'Auth\AuthController@redirectToProvider')->name('home.social.auth');
    Route::get('login/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
});

Route::group(['prefix' => 'v1','middleware' => 'api'/* 'middleware' => 'cors'*//*,'tokenCheck'*/], function ($router) {

    $router->post('auth/register', 'Api\AuthController@postRegister')->name('home.register');
    $router->post('auth/login', 'Api\AuthController@login')->name('home.login');
    $router->get('auth/confirm/{code}', 'Api\AuthController@confirm')->name('home.register.confirm');

//    Route::get('/login/{provider?}',[
//        'uses' => 'AuthController@getSocialAuth',
//        'as'   => 'auth.getSocialAuth'
//    ]);



    $router->get('/', 'Api\HomeController@index')->name('home.index');
    $router->get('main', 'Api\HomeController@main')->name('home.main');

    $router->get('service/create', 'Api\ServiceController@create')->name('home.service.create');
    $router->post('service/store', 'Api\ServiceController@store')->name('home.service.store');
    $router->get('service/{categoryId}/category', 'Api\ServiceController@getCategoryServices')->name('home.service.get.category');
    $router->get('service/{serviceId}', 'Api\ServiceController@getServiceItem')->name('home.service.get.service.item');
    $router->get('service/{userId}/user/{status}', 'Api\ServiceController@getUserServices')->name('home.service.get.user');
    $router->get('service/{serviceId}/up','Api\ServiceController@serviceUp')->name('home.service.up');
    $router->get('service/{serviceId}/stop','Api\ServiceController@serviceStop')->name('home.service.stop');
    $router->post('service/{serviceId}/update','Api\ServiceController@serviceUpdate')->name('home.service.update');
    $router->get('service/{serviceId}/delete','Api\ServiceController@serviceDelete')->name('home.service.delete');
    $router->post('{serviceId}/service/{userId}/user','Api\ServiceController@serviceRating')->name('home.service.user.rating');

    $router->get('announcement/create', 'Api\AnnouncementController@create')->name('home.announcement.create');
    $router->post('announcement/store', 'Api\AnnouncementController@store')->name('home.announcement.store');
    $router->get('announcement/{userId}/user/{status}', 'Api\AnnouncementController@getUserAnnouncements')->name('home.announcement.get.user');
    $router->get('ads/{userId}/user','Api\AnnouncementController@getAdsForUser')->name('home.ads.get.user');
    $router->get('announcement/{announcementId}/up','Api\AnnouncementController@announcementUp')->name('home.announcement.up');
    $router->get('announcement/{announcementId}/delete','Api\AnnouncementController@announcementDelete')->name('home.announcement.delete');

    $router->get('user/{userId}/profile','Api\AuthController@profile')->name('home.user.profile');
    $router->post('user/{userId}/profile/update','Api\AuthController@profileUpdate')->name('home.user.profile.update');

});

//Route::get('/service',function(){
//    return new ServiceResource(Service::find(1));
//});

//$router->get('v1/pages/common/{slug}', ['as' => 'pages.common', 'uses' => 'Api\PageController@getCommonPage']);

//
//Route::group(['prefix' => 'home'], function($router)
//{
//    // Все контакты
//    $router->get('/allContacts',       'BackendAPI\Examples\HomeController@allContacts');
//    // Два контакта
//    $router->get('/twoContacts',       'BackendAPI\Examples\HomeController@twoContacts');
//});
////
/////*
////|--------------------------------------------------------------------------
////| Отдаем JSON 404 если в API нет такого роута.
////|--------------------------------------------------------------------------
////*/
////
//Route::get('/{api_route_not_found?}', function () {
//    return JSON::error404();
//})->name('api')->where('api_route_not_found', '.*');