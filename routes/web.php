<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Админка
|--------------------------------------------------------------------------
| Роуты связанные с админкой
|
*/

Route::get('admin/auth/login', 'Backend\AuthController@getLogin')->name('admin.get.login');
Route::post('admin/auth/login', 'Backend\AuthController@postLogin')->name('admin.post.login');
Route::get('admin/auth/logout', 'Backend\AuthController@logout')->name('admin.logout');


Route::group(['prefix' => 'admin', 'middleware' => 'adminMiddleware:admins'], function () {
    Route::get('/', 'Backend\HomeController@index')->name('admin.home');

    Route::group(['prefix' => 'users/admins/profile'], function () {
        Route::get('/', 'Backend\Users\Admins\AdminProfileController@profile')->name('admin.users.admins.profile');
        Route::post('update', 'Backend\Users\Admins\AdminProfileController@update')->name('admin.users.admins.profile.update');
    });

    Route::group(['prefix' => 'users/admins', 'middleware' => 'adminPermissionMiddleware:manage_admins'], function () {

        Route::get('/', 'Backend\Users\Admins\AdminController@index')->name('admin.users.admins');
        Route::get('get-list', 'Backend\Users\Admins\AdminController@getList')->name('admin.users.admins.list');
        Route::get('create', 'Backend\Users\Admins\AdminController@create')->name('admin.users.admins.create');
        Route::post('store', 'Backend\Users\Admins\AdminController@store')->name('admin.users.admins.store');
        Route::get('{userId}/edit', 'Backend\Users\Admins\AdminController@edit')->name('admin.users.admins.edit');
        Route::post('{userId}/update', 'Backend\Users\Admins\AdminController@update')->name('admin.users.admins.update');

        Route::group(['prefix' => 'roles'], function () {

            Route::get('/', 'Backend\Users\Admins\RoleController@index')->name('admin.users.admins.roles');
            Route::get('get-list', 'Backend\Users\Admins\RoleController@getList')->name('admin.users.admins.roles.list');
            Route::get('create', 'Backend\Users\Admins\RoleController@create')->name('admin.users.admins.roles.create');
            Route::post('store', 'Backend\Users\Admins\RoleController@store')->name('admin.users.admins.roles.store');
            Route::get('{roleId}/edit', 'Backend\Users\Admins\RoleController@edit')->name('admin.users.admins.roles.edit');
            Route::post('{roleId}/update', 'Backend\Users\Admins\RoleController@update')->name('admin.users.admins.roles.update');
        });
    });

    Route::group(['prefix' => 'categories'], function () {

        Route::get('list', 'Backend\CategoriesController@getCategoryList')->name('admin.categories.list');
        Route::get('create', 'Backend\CategoriesController@create')->name('admin.categories.create');
        Route::post('store', 'Backend\CategoriesController@store')->name('admin.categories.store');
        Route::get('{categoryId}/edit', 'Backend\CategoriesController@edit')->name('admin.categories.edit');
        Route::post('{categoryId}/update', 'Backend\CategoriesController@update')->name('admin.categories.update');
        Route::get('{categoryId}/destroy', 'Backend\CategoriesController@destroy')->name('admin.categories.destroy');
        Route::get('{categoryId}/up', 'Backend\CategoriesController@up')->name('admin.categories.up');
        Route::get('{categoryId}/down', 'Backend\CategoriesController@down')->name('admin.categories.down');
    });

    Route::group(['prefix' => 'promotions'], function () {
        Route::get('/', 'Backend\PromotionController@index')->name('admin.promotions');
        Route::get('get-list', 'Backend\PromotionController@getList')->name('admin.promotions.list');
        Route::get('create', 'Backend\PromotionController@create')->name('admin.promotions.create');
        Route::post('store', 'Backend\PromotionController@store')->name('admin.promotions.store');
        Route::get('{promotionId}/edit', 'Backend\PromotionController@edit')->name('admin.promotions.edit');
        Route::post('{promotionId}/update', 'Backend\PromotionController@update')->name('admin.promotions.update');
        Route::get('{promotionId}/destroy', 'Backend\PromotionController@destroy')->name('admin.promotions.destroy');
        Route::get('{productId}/create', 'Backend\PromotionController@productCategoryCreate')->name('admin.promotions.product.create');
        Route::post('{productId}/product-category', 'Backend\PromotionController@productCategoryStore')->name('admin.promotions.product.store');
    });

    Route::group(['prefix' => 'content/news', 'middleware' => 'adminPermissionMiddleware:content_news'], function () {
        Route::get('/', 'Backend\Content\NewsController@index')->name('admin.content.news');
        Route::get('get-list', 'Backend\Content\NewsController@getList')->name('admin.content.news.list');
        Route::get('create', 'Backend\Content\NewsController@create')->name('admin.content.news.create');
        Route::post('store', 'Backend\Content\NewsController@store')->name('admin.content.news.store');
        Route::get('{itemId}/edit', 'Backend\Content\NewsController@edit')->name('admin.content.news.edit');
        Route::post('{itemId}/update', 'Backend\Content\NewsController@update')->name('admin.content.news.update');
        Route::get('{itemId}/destroy', 'Backend\Content\NewsController@destroy')->name('admin.content.news.destroy');
        Route::post('{itemId}/media', 'Backend\Content\NewsController@media')->name('admin.content.news.media');
        Route::get('{mediaId}/delete-media', 'Backend\Content\NewsController@deleteMedia')->name('admin.content.news.delete.media');
        Route::get('{itemId}/{mediaId}/main-media', 'Backend\Content\NewsController@mainMedia')->name('admin.content.news.main.media');
    });

    Route::group(['prefix' => 'content/pages', 'middleware' => 'adminPermissionMiddleware:content_pages'], function () {
        Route::get('/', 'Backend\Content\PageController@index')->name('admin.content.pages');
        Route::get('get-list', 'Backend\Content\PageController@getList')->name('admin.content.pages.list');
        Route::get('create', 'Backend\Content\PageController@create')->name('admin.content.pages.create');
        Route::post('store', 'Backend\Content\PageController@store')->name('admin.content.pages.store');
        Route::get('{pageId}/add-menu', 'Backend\Content\PageController@addMenu')->name('admin.content.pages.add_menu');
        Route::post('{pageId}/store-menu', 'Backend\Content\PageController@storeAddMenu')->name('admin.content.pages.add_menu.store');
        Route::get('{pageId}/edit', 'Backend\Content\PageController@edit')->name('admin.content.pages.edit');
        Route::post('{pageId}/update', 'Backend\Content\PageController@update')->name('admin.content.pages.update');
        Route::get('{pageId}/destroy', 'Backend\Content\PageController@destroy')->name('admin.content.pages.destroy');
        Route::post('{pageId}/media', 'Backend\Content\PageController@media')->name('admin.content.pages.media');
        Route::get('{mediaId}/delete-media', 'Backend\Content\PageController@deleteMedia')->name('admin.content.pages.delete.media');
        Route::get('{pageId}/{mediaId}/main-media', 'Backend\Content\PageController@mainMedia')->name('admin.content.pages.main.media');
        Route::get('{mediaId}/delete-media', 'Backend\Content\PageController@deleteMedia')->name('admin.content.pages.delete.media');
        Route::post('{mediaId}/update-media', 'Backend\Content\PageController@updateMedia')->name('admin.content.pages.update.media');
    });

    Route::group(['prefix'=>'announcements'],function(){
        Route::get('/','Backend\AnnouncementController@index')->name('admin.announcements');
        Route::get('get-list','Backend\AnnouncementController@getList')->name('admin.announcements.list');
        Route::get('{announcementId}/activate','Backend\AnnouncementController@activate')->name('admin.announcements.activate');
        Route::get('{announcementId}/delete','Backend\AnnouncementController@destroy')->name('admin.announcements.delete');
    });

    Route::group(['prefix'=>'services'],function(){
        Route::get('/','Backend\ServiceController@index')->name('admin.services');
        Route::get('get-list','Backend\ServiceController@getList')->name('admin.services.list');
        Route::get('{serviceId}/activate','Backend\ServiceController@activate')->name('admin.services.activate');
        Route::get('{serviceId}/vipToggle','Backend\ServiceController@vipToggle')->name('admin.services.vip.toggle');
        Route::get('{serviceId}/delete','Backend\ServiceController@destroy')->name('admin.services.delete');
    });

    Route::group(['prefix'=>'users'], function(){
        Route::get('/','Backend\Users\Customers\CustomerController@index')->name('admin.users.customers');
        Route::get('get-list','Backend\Users\Customers\CustomerController@getList')->name('admin.users.customers.list');
        Route::get('{userId}/activate','Backend\Users\Customers\CustomerController@activate')->name('admin.users.customers.activate');
        Route::get('{userId}/delete','Backend\Users\Customers\CustomerController@destroy')->name('admin.users.customers.delete');
    });

    Route::group(['prefix' => 'settings'], function () {

        Route::group(['prefix' => 'menu', 'middleware' => 'adminPermissionMiddleware:settings_menu'], function () {
            Route::get('/', 'Backend\Settings\MenuController@index')->name('admin.settings.menu');
            Route::get('get-list', 'Backend\Settings\MenuController@getList')->name('admin.settings.menu.list');
            Route::get('create', 'Backend\Settings\MenuController@create')->name('admin.settings.menu.create');
            Route::post('store', 'Backend\Settings\MenuController@store')->name('admin.settings.menu.store');
            Route::get('{menuId}/edit', 'Backend\Settings\MenuController@edit')->name('admin.settings.menu.edit');
            Route::post('{menuId}/update', 'Backend\Settings\MenuController@update')->name('admin.settings.menu.update');

            Route::get('{menuId}/view', 'Backend\Settings\MenuController@view')->name('admin.settings.menu.item.view');
            Route::get('{menuId}/submenu/create', 'Backend\Settings\MenuController@itemCreate')->name('admin.settings.menu.item.create');
            Route::post('{menuId}/submenu/store', 'Backend\Settings\MenuController@itemStore')->name('admin.settings.menu.item.store');
            Route::get('{menuId}/submenu/{submenuId}/edit', 'Backend\Settings\MenuController@itemEdit')->name('admin.settings.menu.item.edit');
            Route::post('{menuId}/submenu/{submenuId}/update', 'Backend\Settings\MenuController@itemUpdate')->name('admin.settings.menu.item.update');
            Route::get('{menuId}/submenu/{submenuId}/up', 'Backend\Settings\MenuController@up')->name('admin.settings.menu.up');
            Route::get('{menuId}/submenu/{submenuId}/down', 'Backend\Settings\MenuController@down')->name('admin.settings.menu.down');
            Route::get('{menuId}/submenu/{submenuId}/destroy', 'Backend\Settings\MenuController@itemDestroy')->name('admin.settings.menu.destroy');

            Route::group(['prefix' => 'manage'], function () {
                Route::get('/', 'Backend\SettingController@menu')->name('admin.menu.manage');
                Route::post('update', 'Backend\SettingController@menuUpdate')->name('admin.menu.manage.update');
                Route::get('footer', 'Backend\SettingController@footer')->name('admin.footer.manage');
                Route::post('footer-update', 'Backend\SettingController@footerUpdate')->name('admin.footer.manage.update');
            });
        });

        Route::group(['prefix' => 'seo', 'middleware' => 'adminPermissionMiddleware:settings_seo'], function () {
            Route::get('handle', 'Backend\Settings\SeoController@handle')->name('admin.settings.seo.handle');
            Route::post('update', 'Backend\Settings\SeoController@update')->name('admin.settings.seo.update');
        });

        Route::group(['prefix' => 'media'], function () {
            Route::get('model/{owner}/{modelId}', 'Backend\Settings\MediaController@getModelMedia')->name('admin.media.model');
            Route::post('model/{owner}/{modelId}/upload', 'Backend\Settings\MediaController@addMediaForModel')->name('admin.media.model.upload');
            Route::get('{mediaId}/delete', 'Backend\Settings\MediaController@deleteMediaForModel')->name('admin.media.model.delete');
            Route::get('{mediaId}/set-main', 'Backend\Settings\MediaController@setMediaMain')->name('admin.media.model.main');
            Route::get('editor-images', 'Backend\Settings\MediaController@getEditorImages')->name('admin.media.editor.images');
            Route::post('editor-images/upload', 'Backend\Settings\MediaController@addMediaForEditor')->name('admin.media.editor.upload');
            Route::get('{mediaId}/crop', 'Backend\Settings\MediaController@getCrop')->name('admin.media.get.crop');
            Route::post('{mediaId}/crop', 'Backend\Settings\MediaController@postCrop')->name('admin.media.post.crop');

            Route::get('editor-files', 'Backend\Settings\MediaController@getEditorFiles')->name('admin.media.editor.files');
            Route::post('editor-files/upload', 'Backend\Settings\MediaController@addFilesForEditor')->name('admin.media.editor.files.upload');
        });
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'Backend\SettingController@menu')->name('admin.menu');
        Route::post('update', 'Backend\SettingController@menuUpdate')->name('admin.menu.update');
        Route::get('footer', 'Backend\SettingController@footer')->name('admin.footer');
        Route::post('footer-update', 'Backend\SettingController@footerUpdate')->name('admin.footer.update');
    });


    Route::group(['prefix' => 'examples/contacts', 'middleware' => 'adminPermissionMiddleware:examples_contact_list'], function () {

        // главная страница контакт листа
        Route::get('/', 'Backend\Examples\ContactController@index')->name('admin.examples.contacts');

        // получение списка созданных данных
        Route::get('get-list', 'Backend\Examples\ContactController@getList')->name('admin.examples.contacts.list');

        // форма создания контакта
        Route::get('create', 'Backend\Examples\ContactController@create')->name('admin.examples.contacts.create');

        // прием данных с формы и запись в бд
        Route::post('store', 'Backend\Examples\ContactController@store')->name('admin.examples.contacts.store');

        // получение данных и вывод в форму для редактирования
        Route::get('{id}/edit', 'Backend\Examples\ContactController@edit')->name('admin.examples.contacts.edit');

        // обновление данных в бд пришедших с формы
        Route::post('{id}/update', 'Backend\Examples\ContactController@update')->name('admin.examples.contacts.update');

        // удаление данных с бд
        Route::get('{id}/delete', 'Backend\Examples\ContactController@delete')->name('admin.examples.contacts.delete');
    });


    Route::group(['prefix' => 'catalog'], function () {
        Route::get('/', 'Backend\CatalogController@index')->name('admin.catalog');
        Route::get('create', 'Backend\CatalogController@create')->name('admin.catalog.create');
        Route::post('store', 'Backend\CatalogController@store')->name('admin.catalog.store');
        Route::get('get-li-html', 'Backend\CatalogController@getLiHtml')->name('admin.catalog.get.html');
        Route::get('{catalogId}/show', 'Backend\CatalogController@show')->name('admin.catalog.show');
        Route::get('{catalogId}/edit', 'Backend\CatalogController@edit')->name('admin.catalog.edit');
        Route::post('{catalogId}/update', 'Backend\CatalogController@update')->name('admin.catalog.update');
        Route::get('{catalogId}/delete', 'Backend\CatalogController@delete')->name('admin.catalog.delete');
        Route::get('position', 'Backend\CatalogController@position')->name('admin.catalog.position');
        Route::get('{categoryId}/up', 'Backend\CatalogController@up')->name('admin.catalog.up');
        Route::get('{categoryId}/down', 'Backend\CatalogController@down')->name('admin.catalog.down');
        Route::any('{catalogId}/products', 'Backend\CatalogController@products')->name('admin.catalog.products');

    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'Backend\ProductsController@index')->name('admin.products');
        Route::get('create', 'Backend\ProductsController@create')->name('admin.products.create');
        Route::post('store', 'Backend\ProductsController@store')->name('admin.products.store');
        Route::get('get-list', 'Backend\ProductsController@getList')->name('admin.products.list');
        Route::get('{productId}/edit', 'Backend\ProductsController@edit')->name('admin.products.edit');
        Route::post('{productId}/update', 'Backend\ProductsController@update')->name('admin.products.update');
        Route::get('{productId}/destroy', 'Backend\ProductsController@destroy')->name('admin.products.destroy');
        Route::post('{productId}/main-image/upload', 'Backend\ProductsController@uploadMainImage')->name('admin.products.main.image.upload');

        Route::get('sort/list', 'Backend\ProductsController@sortList')->name('admin.products.sort.list');
        Route::post('sort/update', 'Backend\ProductsController@sortUpdate')->name('admin.products.sort.update');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'Backend\OrderController@index')->name('admin.orders');
        Route::get('get-list', 'Backend\OrderController@getList')->name('admin.orders.list');
        Route::post('close/{orderId}', 'Backend\OrderController@close')->name('admin.orders.close');
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('/', 'Backend\FeedbackController@index')->name('feedback');
        Route::get('get-list', 'Backend\FeedbackController@getList')->name('feedback.list');
        Route::get('{messageId}/view', 'Backend\FeedbackController@view')->name('feedback.view');
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('/', 'Backend\Users\Customers\CustomerController@index')->name('admin.users.customers');
        Route::get('get-list', 'Backend\Users\Customers\CustomerController@getList')->name('admin.users.customers.list');
        Route::get('{customerId}/edit', 'Backend\Users\Customers\CustomerController@edit')->name('admin.users.customers.edit');
        Route::post('{customerId}/update', 'Backend\Users\Customers\CustomerController@update')->name('admin.users.customers.update');
    });

});

/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------
| Главный роут для фронта. (SSR V8js)
|
*/
//Route::get('/{wildcard?}', 'Frontend\MainController@index')->name('app')->where('wildcard', '.*');


//Route::get('/', function () {
//})->middleware('site.locale');

//Route::get('change-locale/{locale}', 'Frontend\ChangeLocaleController@changeLocale')->name('changeLocale');
//
//Route::group(['prefix' => '{locale}'/*, 'middleware' => 'site.locale'*/], function () {
//    Route::feeds();

//    Route::get('auth/register', 'Frontend\AuthController@getRegister')->name('frontend.auth.register');
//    Route::post('auth/register', 'Frontend\AuthController@postRegister')->name('frontend.auth.register.post');
//    Route::get('auth/login', 'Frontend\AuthController@getLogin')->name('frontend.auth.login');
//    Route::post('auth/login', 'Frontend\AuthController@postLogin')->name('frontend.auth.login.post');
//    Route::get('auth/logout', 'Frontend\AuthController@logout')->name('frontend.auth.logout');
//    Route::get('auth/profile', 'Frontend\AuthController@profile')->name('frontend.auth.profile');
//    Route::post('auth/profile/update', 'Frontend\AuthController@profileUpdate')->name('frontend.auth.profile.update');
//
//    Route::get('auth/confirm/{code}', 'Frontend\AuthController@confirm')->name('frontend.auth.confirm');
//
//    Route::get('auth/get-email', 'Frontend\AuthController@getEmail')->name('frontend.auth.get.email');
//    Route::post('auth/post-email', 'Frontend\AuthController@postEmail')->name('frontend.auth.post.email');
//    Route::get('auth/forgot-password/{code}', 'Frontend\AuthController@forgotPassword')->name('frontend.auth.forgot.password');
//    Route::post('auth/change-password/{code}', 'Frontend\AuthController@changePassword')->name('frontend.auth.change.password');
//
//
////    Route::get('catalog/{slug}', 'Frontend\CatalogController@show')->name('show.catalog');
////    Route::get('catalog/{catalogSlug}/products', 'Frontend\CatalogController@products')->name('products.catalog');
////    Route::get('catalog/{catalogSlug}/products/{slug}', 'Frontend\ProductController@show')->name('show.product');
////    Route::get('get-catalogs', 'Frontend\CatalogController@getCatalog')->name('get.catalogs');
////    Route::get('product/{id}/modal', 'Frontend\ProductController@productModal')->name('product.modal');
////    Route::get('search/products','Frontend\SearchController@index')->name('frontend.search.index');
//
//    Route::get('sale/{saleSlug}/product/{slug}/sale', 'Frontend\ProductController@productSale')->name('product.sale');
//
//    Route::group(['prefix' => 'cart'], function () {
//        Route::get('add-to-cart/{productId}', 'Frontend\PurchaseController@addToCart')->name('purchase.add.cart');
//        Route::get('remove-from-cart/{productId}', 'Frontend\PurchaseController@removeFromCart')->name('purchase.remove.cart');
//        Route::get('get-city', 'Frontend\PurchaseController@getCity')->name('purchase.get.city');
//        Route::post('checkout', 'Frontend\PurchaseController@checkout')->name('purchase.checkout');
//    });
//
//
//    Route::group(['prefix' => 'feedback'], function () {
//        Route::post('store', 'Frontend\FeedbackController@store')->name('frontend.feedback.store');
//    });
//
//    Route::group(['prefix' => 'ulogin'], function () {
//        Route::any('/', 'Frontend\UloginController@login')->name('frontend.ulogin');
//    });
//
//    Route::group(['prefix' => 'news'], function () {
//        Route::get('{slug}', 'Frontend\NewsController@show')->name('frontend.news.show');
//    });
//
//    Route::group(['prefix' => 'user/favorite'], function () {
////        Route::get('/', 'Frontend\FavoritesController@index')->name('favorite.index');
//        Route::get('{type}/add/{id}', 'Frontend\FavoritesController@addFavorite')->name('add.favorite');
//        Route::get('{type}/remove/{id}', 'Frontend\FavoritesController@removeFavorite')->name('remove.favorite');
//    });
//
//    Route::group(['prefix' => 'search'], function () {
//        Route::get('result', 'Frontend\SearchController@search')->name('frontend.search');
//    });

    Route::get('/', 'Frontend\HomeController@index')->name('home');
//    Route::get('/{prefix}', 'Frontend\PageController@index')->name('pages');
//});



//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('{any}', function () {
    return view('frontend/main');
})->where('any','.*');
