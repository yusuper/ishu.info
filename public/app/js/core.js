let app = function () {

    var settings = {
        blockUi: {
            type: 'page',
            'element': ''
        },

        progressBarSelector: "#progressBar"
    };

    let handleClick = function () {

        $(document.body).on('click', '.handle-click', function (e) {
            e.preventDefault();
            let type = $(this).data('type');

            switch (type) {
                case 'triggerHiddenInput':
                    let input = $("#" + $(this).data('input-id'));
                    triggerHiddenInput(input);
                    break;

                case 'ajax-get':
                    var url = $(this).attr('href');
                    ajaxGet(url);
                    break;

                case 'delete-table-row':

                    var url = $(this).attr('href');
                    var title = $(this).data('confirm-title');
                    var message = $(this).data('confirm-message');
                    var cancelBtnText = $(this).data('cancel-text');
                    var confirmBtnText = $(this).data('confirm-text');
                    swal({
                        title: title,
                        text: message,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: cancelBtnText,
                        confirmButtonText: confirmBtnText
                    }).then((result) => {
                        if (result.value) {
                            ajaxGet(url)
                        }
                    })
                    break;

                case 'confirm':
                    var url = $(this).attr('href');
                    var title = $(this).data('confirm-title');
                    var message = $(this).data('confirm-message');
                    var cancelBtnText = $(this).data('cancel-text');
                    var confirmBtnText = $(this).data('confirm-text');
                    var needUrlFollow = $(this).data('follow-url');

                    swal({
                        title: title,
                        text: message,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: cancelBtnText,
                        confirmButtonText: confirmBtnText
                    }).then((result) => {
                        if (result.value) {
                            if (needUrlFollow) {
                                ajaxGet(url)
                            }

                        }
                    })

                    break;

                case 'modal':
                    var url = $(this).attr('href');
                    var modal = $($(this).data('modal'));
                    modal.modal('show');
                    loadContentInModal(url, modal)
                    break;
            }
        });
    }

    let loadContentInModal = function (url, modal) {
        modal.find('.modal-title').html('...');
        modal.find('.modal-body').html('...');

        ajaxGet(url, "#largeModal .modal-body");
    }

    let updateModalContent = function (modal, modalTitle, modalBody) {
        $(modal).find('.modal-title').html(modalTitle);
        $(modal).find('.modal-body').html(modalBody);
        $(modal).find('.modal-body').removeClass('busy');
    }

    let triggerHiddenInput = function (input) {

        input.val('');
        input.trigger('click');
        submitFormOnInputChange(input)
    }

    let submitFormOnInputChange = function (input) {
        let form = input.closest('form');

        input.change(function () {
            form.submit();
        });
    }

    let handleFormSubmit = function () {

        $('body').on('submit', 'form', function (e) {

            if ($(this).hasClass('ajax')) {
                e.preventDefault();
                ajaxPost($(this));
            }

            if ($(this).hasClass('filter-form')) {

                e.preventDefault();
                var url = $(this).attr('action') + '?' + $(this).serialize();
                var table = $(this).data('table');
                loadContentInTable(url, $(table));

            }
        });
    }

    let handlePaginationClick = function () {
        $(document.body).on('click', '.pagination .page-link', function (e) {
            e.preventDefault()
            let url = $(this).attr('href');
            let table = $("#" + $(this).closest('.pagination_placeholder').data('table-id'));
            loadContentInTable(url, table);
        });
    }

    let handleLoadContentInTable = function () {

        $.each($('.ajax-content'), function () {

            let url = $(this).attr('data-ajax-content-url');

            loadContentInTable(url, $(this));

        })

    }

    let loadContentInTable = function (url, table) {

        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',

            beforeSend: function () {
                blockUI();
            },

            success: function (response) {
                table.find('tbody').html(response.tableData);

                if (response.pagination) {
                    table.closest('.box').find('.pagination_placeholder').html(response.pagination);
                }

            },

            error: function (response) {
                console.log(response);
            },
            complete: function () {
                unblockUi();
            }

        });
    }

    let ajaxGet = function (url, blockElem) {
        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',

            beforeSend: function () {
                if (blockElem) {
                    $(blockElem).addClass('busy');
                } else {

                    blockUI();
                }

            },

            success: function (response) {
                // alert(response.type);
                switch (response.type) {
                    case 'redirect':
                        location.replace(response.redirect_url);
                        break;
                    case 'update-table-row':
                        updateTableRow($(response.table), $(response.row), response.content)
                        break;

                    case 'delete-table-row':
                        deleteTableRow($(response.table), $(response.row));
                        break;

                    case 'delete-block':
                        $(response.block).fadeOut(function () {
                            $(this).remove();
                        });
                        break;

                    case 'updateModal':
                        console.log(response)
                        updateModalContent(response.modal, response.modalTitle, response.modalContent);
                        break;

                    case 'reloadTable':
                        loadContentInTable(response.tableContentUrl, $(response.tableId))
                        break;
                }

                if (response.functions) {
                    $.each(response.functions, function (index, value) {
                        runFunction(value, response);
                    });
                }

            },

            error: function (response) {

            },
            complete: function () {
                if (blockElem) {
                    $(blockElem).removeClass('busy');
                } else {

                    unblockUi();
                }

            }

        });
    }

    let ajaxPost = function (form) {

        let url = form.attr('action');
        let formId = form.attr('id');
        let formData = new FormData(form[0]);
        let hasProgress = form.data('show-progress');

        settings.blockUi.type = form.data('ui-block-type');
        settings.blockUi.element = form.data('ui-block-element');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            xhr: function () {
                let xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        let percentComplete = evt.loaded / evt.total * 100;
                        updateProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },

            beforeSend: function () {

                blockUI();
                validationReset();
                initProgress();
            },

            success: function (response) {

                switch (response.type) {
                    case 'redirect':
                        location.replace(response.redirect_url);
                        break;

                    case 'prepend-table-row':
                        prependTableData($(response.table), response.content);
                        break;

                    case 'update-table-row':
                        updateTableRow($(response.table), $(response.row), response.content);
                        break;

                    case 'reloadTable':
                        loadContentInTable(response.tableContentUrl, $(response.tableId))
                        break;

                    case 'updateModal':
                        updateModalContent(response.modal, response.modalTitle, response.modalContent);
                        break;

                    case 'notify':
                        showNotify(response.notify_message, response.notify_type)
                        break;
                    case 'updateBlock':
                        updateBlock(response.blockSelector, response.blockContent);
                        break;
                }

                if(response.tableData){
                    $('table').find('tbody').html(response.tableData);
                }



                if (response.functions) {
                    $.each(response.functions, function (index, value) {
                        runFunction(value, response);
                    });
                }
            },

            error: function (response) {

                if (response.status == 419) {
                    location.refresh();
                }

                if (response.status == 422) {
                    validationFail(response.responseJSON, formId);
                }

                if (response.responseJSON && response.responseJSON.type) {
                    if (response.responseJSON.type == 'alert') {
                        showAlert(response.responseJSON.header, response.responseJSON.message, response.responseJSON.alert_type)
                    }
                }

                unblockUi();

            },
            complete: function () {

                unblockUi();
                resetProgress();
            }
        });
    }

    let updateBlock = function (blockSelector, blockContent) {
        $(blockSelector).html(blockContent);
    }

    let initProgress = function () {
        $(settings.progressBarSelector).find('.progress-bar').css('width', '0%');
        $(settings.progressBarSelector).find('.progress-bar').css('opacity', 1);

    }

    let updateProgress = function (percent) {
        $(settings.progressBarSelector).find('.progress-bar').css('width', percent + '%');
    }

    let resetProgress = function () {
        $(settings.progressBarSelector).find('.progress-bar').fadeOut(function () {
            $(this).css('width', 0)
        }).fadeIn();
    }

    let validationFail = function (response, formId) {
        for (input in response.errors) {

            var formGroup = $("#" + formId + " *[id='" + input + "']").closest('.form-group');

            formGroup.addClass('has-error');

            for (message in response.errors[input]) {
                formGroup.find('.help-block').append(' ' + response.errors[input][message]);
            }
        }
    }

    let validationReset = function () {
        $('.form-group').find('.help-block').html('');
        $('.form-group').removeClass('has-error');
    }

    let showAlert = function (header, message, type) {
        swal({
            type: type,
            title: header,
            text: message,

        })
    }

    let showNotify = function (message, type) {

        $.notify({
            // options
            message: message
        }, {
            // settings
            type: type,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },

            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },

        });
    }

    let prependTableData = function (table, data) {
        table.find('tbody').prepend(data);
    }

    let updateTableRow = function (table, row, data) {

        table.find(row).fadeOut(function () {
            $(this).replaceWith(data);
            $(this).fadeIn();
        });
    }

    let deleteTableRow = function (table, row) {
        table.find(row).fadeOut(function () {
            $(this).remove();

        });
    }

    let runFunction = function (functionName, response) {
        switch (functionName) {
            case 'closeModal':
                closeModal(response)
                break;

            case 'initEditor':
                initEditor();
                break;

            case 'initSelect2':
                initSelect2();
                break;
        }
    }

    let closeModal = function (data) {
        $(data.modal).modal('hide');
    }

    let blockUI = function () {

        switch (settings.blockUi.type) {
            case 'page':
                blockPage();
                break;

            case 'element':
                $(settings.blockUi.element).addClass('busy');
                break;
        }
    }

    let unblockUi = function () {
        switch (settings.blockUi.type) {
            case 'page':
                $('body').unblock();
                break;

            case 'element':
                $(settings.blockUi.element).removeClass('busy');
                break;
        }

    }

    let blockPage = function (options) {

        let el = $('body');

        options = $.extend(true, {
            opacity: 0.1,
            overlayColor: '#000000',
            state: 'primary',
            type: 'loader',
            centerX: true,
            centerY: true,
            message: 'Processing...',
            shadow: true,
            width: 'auto'
        }, options);

        let params = {
            message: '<div style="border: 1px solid #D2D6DE; border-radius: 5px; background-color: #fff; padding: 5px; color: #777777;">Processing <i class="fa fa-spinner fa-spin"></i></div>',
            centerY: options.centerY,
            centerX: options.centerX,
            css: {
                top: '30%',
                left: '50%',
                border: '0',
                padding: '0',
                backgroundColor: 'none',
                width: options.width
            },
            overlayCSS: {
                backgroundColor: options.overlayColor,
                opacity: options.opacity,
                cursor: 'wait'
            },

            onUnblock: function () {
                if (el) {
                    el.css('position', '');
                    el.css('zoom', '');
                }
            }
        };

        params.css.top = '50%';
        $.blockUI(params);
    }

    let initDatePicker = function () {
        $('.dp').datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy'
        })
    }

    let initEditor = function () {
        $('.editor').each(function () {
            editor = CKEDITOR.replace($(this).attr('id'), {
                height: 500
            });

            editor.ui.addButton('ImageManager', {
                label: "Менеджер изображений",
                command: 'showImageManager',
                toolbar: 'insert',
                icon: '/app/js/vendors/ckeditor/image_man.png'
            });

            editor.ui.addButton('FileManager', {
                label: "Менеджер файлов",
                command: 'showFileManager',
                toolbar: 'insert',
                icon: '/app/js/vendors/ckeditor/image_file.png'
            });

            editor.addCommand("showImageManager", {
                exec: function (edt) {
                    showGalleryImages(edt);
                }
            });

            editor.addCommand("showFileManager", {
                exec: function (edt) {
                    showFiles(edt);
                }
            });

            editor.on('instanceReady', function (ev) {
                ev.editor.dataProcessor.htmlFilter.addRules({
                    elements: {
                        $: function (element) {
                            if (element.name == 'a') {
                                if (!element.attributes.rel)
                                    element.attributes.rel = 'nofollow';
                            }
                        }
                    }
                });
            });
        });
    };

    return {
        init: function () {
            handleFormSubmit();
            handleLoadContentInTable();
            handlePaginationClick();
            handleClick();
            initDatePicker();
            initEditor();
        },
    }
}();


$(document).ready(function () {
    app.init();
});

let initSelect2 = function () {
    // var Select2={init:function(){$("#m_select2_1, #m_select2_1_validate").select2({placeholder:"Select a state"}),$("#m_select2_2, #m_select2_2_validate").select2({placeholder:"Select a state"}),$("#m_select2_3, #m_select2_3_validate").select2({placeholder:"Select a state"}),$("#m_select2_4").select2({placeholder:"Select a state",allowClear:!0}),$("#m_select2_5").select2({placeholder:"Select a value",data:[{id:0,text:"Enhancement"},{id:1,text:"Bug"},{id:2,text:"Duplicate"},{id:3,text:"Invalid"},{id:4,text:"Wontfix"}]}),$("#m_select2_6").select2({placeholder:"Search for git repositories",allowClear:!0,ajax:{url:"https://api.github.com/search/repositories",dataType:"json",delay:250,data:function(e){return{q:e.term,page:e.page}},processResults:function(e,t){return t.page=t.page||1,{results:e.items,pagination:{more:30*t.page<e.total_count}}},cache:!0},escapeMarkup:function(e){return e},minimumInputLength:1,templateResult:function(e){if(e.loading)return e.text;var t="<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>"+e.full_name+"</div>";return e.description&&(t+="<div class='select2-result-repository__description'>"+e.description+"</div>"),t+="<div class='select2-result-repository__statistics'><div class='select2-result-repository__forks'><i class='fa fa-flash'></i> "+e.forks_count+" Forks</div><div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> "+e.stargazers_count+" Stars</div><div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> "+e.watchers_count+" Watchers</div></div></div></div>"},templateSelection:function(e){return e.full_name||e.text}}),$("#m_select2_12_1, #m_select2_12_2, #m_select2_12_3, #m_select2_12_4").select2({placeholder:"Select an option"}),$("#m_select2_7").select2({placeholder:"Select an option"}),$("#m_select2_8").select2({placeholder:"Select an option"}),$("#m_select2_9").select2({placeholder:"Select an option",maximumSelectionLength:2}),$("#m_select2_10").select2({placeholder:"Select an option",minimumResultsForSearch:1/0}),$("#m_select2_11").select2({placeholder:"Add a tag",tags:!0}),$(".m-select2-general").select2({placeholder:"Select an option"}),$("#m_select2_modal").on("shown.bs.modal",function(){$("#m_select2_1_modal").select2({placeholder:"Select a state"}),$("#m_select2_2_modal").select2({placeholder:"Select a state"}),$("#m_select2_3_modal").select2({placeholder:"Select a state"}),$("#m_select2_4_modal").select2({placeholder:"Select a state",allowClear:!0})})}};jQuery(document).ready(function(){Select2.init()});

    let select2 = $('.select2');
    if (select2.length) {
        select2.select2({
            placeholder: "Выберите категории"
        });
    }
};



