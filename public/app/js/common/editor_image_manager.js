
var editor;
let url = $('meta[name="editor-images-url"]').attr('content');
let filesUrl = $('meta[name="editor-files-url"]').attr('content');

let showGalleryImages = function (edt) {
    editor = edt;
    loadData(url)
}

let showFiles = function (edt) {
    editor = edt;
    loadData(filesUrl);
}

let loadData = function (url) {
    $("#editorModal").find('.modal-body').html('');
    $("#editorModal").find('.modal-title').html('Загрузка');

    $.ajax({
        method: 'get',
        url: url,
        beforeSend: function () {
            $("#editorModal").show();
        },
        success: function (response) {
            $("#editorModal").find('.modal-title').html(response.modalTitle);
            $("#editorModal").find('.modal-body').html(response.content);
        },
        error: function () {

        },
        complete: function () {

        }
    });
}

$('body').on('submit', "#editorForm", function(e) {
    e.preventDefault();
    let formData = new FormData($("#editorForm")[0]);
    let url = $("#editorForm").attr('action');

    $.ajax({
        method: 'post',
        url: url,
        data: formData,
        dataType: 'json',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        xhr: function()
        {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    // console.log(evt.loaded);
                    var percentComplete = evt.loaded / evt.total * 100;
                    updateProgressBar(percentComplete);
                }
            }, false);

            return xhr;
        },
        beforeSend: function () {
            initProgress();
        },
        success: function (response) {
            $(".galley-content").html(response.content);
        },

        error: function (data) {

        },
        complete: function () {
            destroyProgress();
            $("#editorForm .input-file").val('')
        }
    });
});

$('body').on('change', $("#editorForm .input-file"), function(e) {
    $("#editorForm").submit();
});

$('body').on('click', '.insert-image', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');

    console.log(url);
    var html = '<p style="text-align: center"><img width="600" src="' + url + '" /></p>';

    editor.insertHtml(html,'unfiltered_html');
    $('#editorModal').hide();
});

$('body').on('click', '.insert-file', function(e) {
    e.preventDefault();
    let url = $(this).attr('href');
    let name = $(this).data('file-name');
    var html = '<a target="_blank" href="'+ url +'">'+ name +'</a>';

    editor.insertHtml(html,'unfiltered_html');
    $('#editorModal').hide();


});

$('body').on('click', '.close-gallery', function(e) {
    e.preventDefault();
    $("#editorModal").hide();
});

var initProgress = function () {
    if ($("#progressBar").length)
    {
        $("#progressBar").fadeIn();
        $("#progressBar").find('.progress-bar').css('width', '0%');

    }
}

var destroyProgress = function () {
    if ($("#progressBar").length)
    {
        $("#progressBar").fadeOut(function () {
            $("#progressBar").find('.progress-bar').css('width', '0%');
        });

    }
    editor.addCommand("showFileManager", {
        exec: function(edt) {
            showFiles(edt);

        }
    });
};

var updateProgressBar = function (percent) {
    if ($("#progressBar").length)
    {
        $("#progressBar").find('.progress-bar').css('width', percent + '%');
    }
};

var updateBlock = function (data) {
    $(data.response.blockId).html(data.response.blockData);

};

var closeModal = function (data) {
    $(data.response.targetModal).modal('hide');
};

$(document.body).on('click', '.has-modal-content', function (e) {
    e.preventDefault();
    var modal = $($(this).data('modal'));
    var url = $(this).data('modal-content-url');
    var blockElement = modal.find('.modal-body');

    $.ajax({
        method: 'get',
        url: url,
        dataType: 'json',

        beforeSend: function () {
            modal.find('.modal-content .modal-title').html('Загрузка');
            modal.find('.modal-content .modal-body').html('');
            mask(blockElement);
            modal.modal('show');
        },
        success: function (data) {

            modal.find('.modal-content .modal-title').html(data.response.modalTitle);
            modal.find('.modal-content .modal-body').html(data.response.modalContent);

            if (data.response.functions) {

                $.each(data.response.functions, function (index, value) {
                    window[value](data);
                });
            }
        },
        error: function (data) {
            console.log(data.status);
            unmask(blockElement);
        },
        complete: function () {
            unmask(blockElement);
        }
    });
});

var mask = function (elem) {
    if (!elem.hasClass('busy')) {
        elem.addClass('busy');
    }
};

var unmask = function (elem) {
    if (elem.hasClass('busy')) {
        elem.removeClass('busy');
    }
};

var submitForm = function (form, blockElement) {
    var url = form.attr('action');
    var method = form.attr('method');
    var formData = new FormData(form[0]);
    var formId = form.attr('id');
    var blockElem = $(blockElement);

    $.ajax({
        method: method,
        url: url,
        data: formData,
        dataType: 'json',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
            resetValidationErrors();
            mask(blockElem);
        },
        success: function (data) {

            if (data.response.redirect_url) {
                location.replace(data.response.redirect_url);
            }

            if (data.response.functions) {

                $.each(data.response.functions, function (index, value) {
                    window[value](data);
                });
            }

            if (data.response.message) {
                showMessage(
                    data.response.message.header,
                    data.response.message.message,
                    data.response.message.type
                )
            }
        },
        error: function (data) {
            //console.log(data.status);
            unmask(blockElem);
            if (data.status == 422) {
                validationFail(data.responseJSON, formId);
            }
            switch (data.status) {
                case 403:
                    // console.log(data.responseJSON.error.action);
                    if (data.responseJSON.error.action == 'message') {
                        showMessage(
                            data.responseJSON.error.message_data.header,
                            data.responseJSON.error.message_data.message,
                            data.responseJSON.error.message_data.type
                        )
                    }
                    break;
            }
        },
        complete: function () {
            unmask(blockElem)
        }
    });
};

$(document.body).on('submit', 'form', function (e) {
    if ($(this).hasClass('ajax-submit')) {
        e.preventDefault();
        var blockElem = $(this).data('block-element');
        return submitForm($(this), blockElem);
    }
});

var resetValidationErrors = function () {
    $('.form-group').find('.help-block').html('');
    $('.form-group').removeClass('has-error');
};

$(document.body).on('click', '.up-menu-item', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    simpleAjax(url);
});

$(document.body).on('click', '.down-menu-item', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    simpleAjax(url);
});

$(document.body).on('click', '.delete-menu-item', function (e) {

    e.preventDefault();
    var url = $(this).attr('href');

    swal({
        title: 'Вы уверены, что хотите удалить?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Удалить!',
        cancelButtonText: 'Отмена'
    }).then((result) => {
        if (result.value) {
        simpleAjax(url);
    } else if (result.dismiss === swal.DismissReason.cancel) {

    }
})
});

function simpleAjax(url) {
    $.get(url, function (data) {
        if (data.response.redirect_url) {
            location.replace(data.response.redirect_url);
        }
        if (data.response.functions) {
            $.each(data.response.functions, function (index, value) {
                window[value](data);
            });
        }
    });
}