$(document).ready(function () {
    // productInCart();
    if ($('.total_cart').length) {
        loadTotalPrice();
    }

    $(document.body).on('click', '.addFavorite', function (e) {
        e.preventDefault();
        let element = $(this);
        let url = element.data('url');

        let favoriteBox = $('#favoriteNotify');
        // let countFavorite = favoriteBox.text();

        $.get(url, function (data) {
            element.removeClass('addFavorite');
            element.addClass('isFavorite');
            favoriteBox.text(data.favoriteCount);
            // favoriteBox.text(parseInt(countFavorite) + 1);
            if (data.favoriteCount) {
                favoriteBox.show();
            }
        });
    });


    $(document.body).on('click', '.removeFavorite', function (e) {
        e.preventDefault();
        let element = $(this);
        let url = element.data('url');

        let favoriteBox = $('#favoriteNotify');
        let tr = $(this).closest('tr');
        $.get(url, function (data) {
            tr.remove();
            favoriteBox.text(data.favoriteCount);
            if (data.favoriteCount === 0) {
                favoriteBox.hide();
            }
        });
    });


    $(document.body).on('click', '.addToCart', function (e) {
        e.preventDefault();
        let element = $(this);
        let url = element.data('url');
        let qty = 1;

        let cartBox = $('#cartNotify');
        // let countCart = cartBox.text();

        let input = element.parent().find('input');
        if (input.length) {
            qty = input.val();
        }

        $.get(url, {'qty': qty}, function (data) {
            console.log(data.countCart);
            let span = element.find('span');
            span.text(span.data('text-added'));
            element.removeClass('addToCart');
            // cartBox.text(parseInt(countCart) + 1);
            cartBox.text(data.countCart);
            cartBox.removeClass('notNotifyBox');
        });
    });

    $(document.body).on('click', '.removeFromCart', function (e) {
        e.preventDefault();
        let cartBox = $('#cartNotify');
        let url = $(this).data('url');
        let element = $(this).closest('tr');
        $.get(url, function (data) {
            console.log(data.countCart);
            if (data.countCart) {
                cartBox.text(data.countCart);
            } else {
                location.replace('/');
            }
            element.remove();
        });
    });

    $(document.body).on('change', '.inputQty', function (e) {
        let input = $(this);
        let url = input.data('url');
        let price = input.data('price');
        let qty = input.val();
        $.get(url, {'qty': qty}, function (data) {
            console.log(data);
        });
        $('.current-price').html('<span class="current-price">' + qty * price + " сом</span>");
    });

    let productModal = $('#productModal');
    productModal.modal('hide');
    productModal.on('shown.bs.modal', function (event) {
        let modal = $(this);
        let a = $(event.relatedTarget);
        let url = a.data('url');

        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                modal.find('.modal-body').html(response.content);
            },
            error: function (response) {

            },

        });
    });

    productModal.on('hidden.bs.modal', function (event) {
        console.log(event)
        let modal = $(this);
        modal.find('.modal-body').html('');
    });


    $('.cart_quantity').bind('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');

        let qty = $(this).val();
        if (qty < 1) {qty = 1; $(this).val(qty)}
        let product_row = $(this).closest('.product_row');
        let total = parseFloat(product_row.find('.price_product').text()) * parseInt(qty);
        product_row.find('.price_sum').text(total);
        loadTotalPrice();
    });


    $('#region_id').change(function () {
        let url = $(this).data('url');
        let regionId = $(this).val();
        $.get(url, {id: regionId}, function (data) {
            $('#city_id').html(data.options);
        })
    });


    $('#checkout_order').click(function (e) {
        e.preventDefault();
        $(this).closest('.ajaxForm').submit();
    });


    $('.ajaxForm').submit(function (e) {

        e.preventDefault();
        let url = $(this).attr('action');
        let formData = new FormData($(this)[0]);
        let form = $(this);
        let blockElem = form.data('block-element');
        let formId = form.attr('id');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                blockElement(blockElem);
            },

            success: function (response) {
                switch (response.type) {
                    case 'redirect':
                        location.replace(response.redirect_url);
                        break;
                    case 'alert':
                        showAlert(response.header, response.message, response.type, response.url);
                        break;
                    case 'updateBlock':
                        $(response.blockSelector).html(response.blockContent);
                        break;
                    case 'appendBlock':
                        $(response.blockSelector).append(response.blockContent);
                        break;
                }

                if (response.functions) {
                    $.each(response.functions, function (index, value) {
                        runFunction(value, response);
                    });
                }
            },
            error: function (response) {

                if (response.status == 403) {
                    showAlert(response.responseJSON.header, response.responseJSON.message, response.responseJSON.alert_type);
                }

                if (response.status == 419) {
                    location.refresh();
                }

                if (response.status == 422) {
                    for (let input in response.responseJSON.errors) {
                        let formGroup = $("#" + formId + " *[id='" + input + "']").parent();
                        formGroup.addClass('has-error');
                        for (let message in response.responseJSON.errors[input]) {
                            formGroup.find('.help-block').html(' ' + response.responseJSON.errors[input][message]);
                        }
                    }
                }
            },
            complete: function () {
                unblockElement(blockElem);
            }
        });
    });
});

let showAlert = function (header, message, type, url) {
    swal({
        type: type,
        title: header,
        text: message,

    }).then((result) => {
        if (result) {
            if (url) {
                location.replace(url);
            }
        }

    });
};

let blockElement = function (element) {
    if (!$(element).hasClass('busy')) {
        $(element).addClass('busy');
    }

};

let unblockElement = function (element) {
    $(element).removeClass('busy');
};


let runFunction = function (functionName, response) {
    switch (functionName) {
        case 'resetCommentForm':
            $("#commentForm").find('textarea').val('');
            break;
        case 'updateContainer':
            updateContainer(response);
            break;

        case 'updateBlock':
            $(response.blockSelector).html(response.blockContent);
            break;

        case 'clearForm':
            clearForm(response);
            break;
        case 'closeModal':
            closeModal(response);
            break;

        case 'resetForm':
            resetForm(response);
            break;

        case 'initSearch':
            initSearch(response);
            break;

        case 'scrollToOpenedFaq':
            scrollToOpenedFaq();
            break;

        case 'scrollToTopOfFaq':
            scrollToTopOfFaq();
            break;

        case 'setSubscriptionCookie':
            setSubscriptionCookie();
            break;
    }
};

let clearForm = function (response) {
    let form = $(response.formSelector);
    form.trigger('reset');
};

let resetForm = function (response) {

    $(response.formForReset).find('input:text, input:file, input:password, select, textarea').val('');
    $(response.formForReset).find('input:radio, input:checkbox').prop('checked', false);
    $("#resumeUploader").find('span').html('');
};

function updateContainer(data) {
    $.each(data.html, function (index, value) {
        if (!value) {
            value = ('<p>Записи отсутствуют</p>');
            $(index).html(value).css({"margin": "0px"});
        } else {
            $(index).html(value).css({"margin": ""});
        }
    });
}

function loadTotalPrice() {
    let element = $('.product_row');
    let sumCart = 0;
    $.each(element, function (i, v) {
        sumCart += parseFloat(element.eq(i).find('.price_sum').text());
    });
    $('.sub_total_cart').text(sumCart);
    $('.total_cart').text(sumCart);
    $('#total').val(sumCart)
}

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

// $('#search-form .search').bind('input', function () {
//     // console.log($('.search-table').attr('data-url'));
//     $('#search-form').submit();
//
// });
$('#search-form .search').keyup(delay(function (e) {
    $('#search-form').submit();
}, 500));

$('.search-results').focusout(function () {
    $('.search-results').css('display', 'none');
});

$('#search-form').on('submit', function (e) {

    e.preventDefault();
    $('.search-results').css('display', 'block');
    let url = $(this).attr('action');
    let search = $(this).find('input').val();
    $.get(url, {search: search}, function (data) {
        $('.search-table').html(data.searchHtml);
    });
});


function initSearch(data) {
    let textToFind, matches_array;
    let list = $(data.idSearch);
    let originalList = list;
    let result = [];

    let inputSearch = $('input[type=search]');
    inputSearch.bind('input', function () {
        textToFind = TrimStr($(this).val());
        result = [];
        list.find('a').each(function (i, v) {
            if (matches_array = originalList.find('a').eq(i).text().match(eval("/" + textToFind + "/i"))) {
                result.push(list.find('a').eq(i));
                list.find('a').eq(i).parent().show();
                list.find('a').eq(i).html(list.find('a').eq(i).text().replace(eval("/" + matches_array[0] + "/i"), "<span style='background:yellow'>" + matches_array[0] + "</span>"));
            } else {
                list.find('a').eq(i).parent().hide();
            }
            // list.find(a).eq(i).html(list.find(a).eq(i).text().replace(eval("/" + textToFind + "/gi"), createReplacer(1, "<span style='background:#e7e507'>" + textToFind + "</span>")));
        })
    });

    function TrimStr(s) {
        s = s.replace(/^\s+/g, '');
        return s.replace(/\s+$/g, '');
    }

    let form = inputSearch.closest('form');
    $('body').on('submit', form, function (e) {
        e.preventDefault();

        if (result.length != 1) {
            // alert("Выберите одну тему");
        } else {
            location.replace(result[0].attr('href'))
        }
    });
}

let scrollToOpenedFaq = function () {
    let openedItem = $("#faq").find('.show').closest('.faq__item');

    if (openedItem.length) {
        $('html, body').animate({
            scrollTop: parseInt(openedItem.offset().top - 80)
        }, 300);
    }
};

let scrollToTopOfFaq = function () {
    $('html, body').animate({
        scrollTop: parseInt($(".page__header").offset().top - 100)
    }, 300);
};

let setSubscriptionCookie = function () {
    Cookies.set('subscribed', '1', {expires: 365});
};


//ЭТО ДЛЯ МОБИЛЬНОГО МЕНЮ

// When the user scrolls the page, execute myFunction
window.onscroll = function () {
    myFunction()
};

// Get the navbar
var navbar = document.getElementById("navbar");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

