<?php

return [
    'contact_form' => 'Contact form',
    'email_address' => 'E-Mail Address',
    'enquiry' => 'Enquiry',
    'submit' => 'Submit',
];