<?php
return [
    'home' => 'Home',
    'shopping_cart' => 'Shopping Cart',
    'item' => 'Item',
    'price' => 'Price',
    'quantity' => 'Quantity',
    'total' => 'Total',
    'what_would_you_like_to_do_next' => 'What would you like to do next?',
    'enter_your_contact_info' =>'Enter your contact info',
    'shipping' => 'Shipping',
    'enter_your_address' => 'Enter your address.',
    'checkout' => 'Checkout',
    'continue_shopping' => 'Continue Shopping',
    'make_purchase' => 'Checkout now',
    'please_check' => 'Please check the quantity of goods and fill in the fields',
    'total_cart_price' => 'Total cart price',
    'delivery' => 'Delivery',
    'count' => 'Count',
    'name' => 'Your name',
    'phone' => 'Your phone',
    'region' => 'Choose region',
    'city' => 'Choose city'
];