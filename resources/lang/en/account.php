<?php

return [
    'order_history_and_details' => 'Order History And Details',
    'image' => 'Image',
    'product_name' => 'Product Name',
    'model' => 'Model',
    'unit_price' => 'Unit Price',
    'update' => 'Update',
    'remove' => 'Remove',
    'subtotal' => 'Sub-Total',
    'flat_rate_shipping' => 'Flat Rate Shipping',
    'confirm_order' => 'Confirm Order',
    'my_address' => 'My Address',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'address' => 'Address',
    'city' => 'City',
    'post_code' => 'Post Code',
    'country' => 'Country',
    'region_state' => 'Region / State',
    'please_select' => 'Please Select',
    'my_wish_list' => 'My Wish List',
    'stock' => 'Stock',
    'action' => 'Action',
    'save' => 'Save',
];