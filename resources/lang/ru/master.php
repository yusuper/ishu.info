<?php

return [
    'cart' => 'Корзина',
    'account' => 'Мой аккаунт',
    'sign_in' => 'Вход',
    'search' => 'Поиск',
    'home' => 'Главная',
    'about_us' => 'О нас',
    'news' => 'Новости',
    'bestseller_products' => 'Хиты продаж',
    'new_products' => 'Новые продукты',
    'contacts' => 'Контакты'
];