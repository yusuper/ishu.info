
    @if($mainImage)
        <a href="{{route('admin.media.get.crop', ['mediaId' => $mainImage->id])}}" class="handle-click" data-type="modal" data-modal="#largeModal">
        <img class="img-thumbnail" src="{{asset('storage/uploaded_images/512_' . $mainImage->getOriginal('original_file_name'))}}?v{{uniqid()}}">
        </a>
    @else
        <img class="img-thumbnail" src="/app/images/no_image_placeholder.jpg">
    @endif


<div style="margin-top: 10px;">
    <a href="" class="btn btn-info btn-sm btn-block handle-click" data-type="triggerHiddenInput"
       data-input-id="mainImageInput">Добавить изображение</a>
</div>




