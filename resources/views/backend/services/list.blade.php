@foreach($services as $service)
    @include('backend.services.item')
@endforeach

@if(!$services->count())
    <tr>
        <td colspan="9" class="text-center">Данных не найдено</td>
    </tr>
@endif
