<tr class="row-{{$service->id}}">
    <td class="text-center">{{$service->id}}</td>
    <td>@if(isset($service->user->full_name)){{$service->user->full_name}}@endif</td>
    <td>
        <img width="50px" src="@if(isset($service->mainImage)){{$service->mainImage->original_file_name}}@endif">
    </td>
    <td>{{$service->phone}}</td>
    <td>{{$service->desc}}</td>
    <td>
        -
        @foreach($service->categories as $category)
            {{$category->name}}-
        @endforeach
    </td>
    <td class="text-center">
        <a class="handle-click" data-type="ajax-get" href="{{route('admin.services.vip.toggle',['serviceId'=>$service->id])}}">
            @if($service->is_vip == 1)
                <i class="fa fa-star" style="color:green"></i>
            @else
                <i class="fa fa-star" style="color:red"></i>
            @endif
        </a>
    </td>
    <td class="text-center">
        <a class="handle-click" data-type="ajax-get" href="{{route('admin.services.activate',['serviceId'=>$service->id])}}">
            @if($service->active == 1)
                <i class="fa fa-power-off" style="color:green"></i>
            @else
                <i class="fa fa-power-off" style="color:red"></i>
            @endif
        </a>
    </td>

    <td class="text-center">
        <a class="handle-click" data-type="delete-table-row" data-confirm-title="Подтвердите действие" data-cancel-text="Отмена" data-confirm-text="Удалить"
           href="{{route('admin.services.delete',['serviceId'=>$service->id])}}">
            <i class="fa fa-trash"></i>
        </a>
    </td>
    {{--<td class="text-center">--}}
    {{--<a href="{{ route('admin.users.customers.edit', ['id' => $announcement->id]) }}" data-type="modal"--}}
    {{--data-modal="#superLargeModal"--}}
    {{--class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"--}}
    {{--data-toggle="m-tooltip" data-placement="top" title="Редактировать продукт">--}}
    {{--<i class="fa fa-pencil-square-o"></i>--}}
    {{--</a>--}}
    {{--</td>--}}
</tr>
