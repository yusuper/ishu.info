@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.catalog.edit', ['catalogId' => $categoryId]) }}"
                           data-type="modal"
                           data-modal="#largeModal"
                           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="Редактировать каталог"><i
                                    class="la la-edit"></i></a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.catalog.delete', ['catalogId' => $categoryId]) }}"
                           class="m-portlet__nav-link m-portlet__nav-link--icon delete-catalog-item"
                           data-toggle="m-tooltip" data-placement="top" title="Удалить каталог">
                            <i class="la la-trash"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="m-section">
            <div class="row">
                <div class="col-8">
                    <div class="m-section__content" id="sectionsPlaceholder">
                        <div class="tree-data" style="display: none">
                            {!! $catalogs !!}
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="m-section__content">
                        @if (isset($catalog->medias))
                            <img src="{{ $catalog->medias->getThumb128Attribute('original_file_name') }}"
                                 alt="{{ $catalog->medias->original_file_name }}">
                        @endif
                    </div>
                </div>
            </div>


        </div>
    {{--<!--end::Section-->--}}
    {{--</div>--}}

    {{--<div class="m-portlet">--}}
    {{--<div class="m-portlet__head">--}}
    {{--<div class="m-portlet__head-caption">--}}
    {{--<div class="m-portlet__head-title">--}}
    {{--<h3 class="m-portlet__head-text">--}}
    {{--Продукт--}}
    {{--</h3>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="m-portlet__head-tools">--}}
    {{--<ul class="m-portlet__nav">--}}

    {{--<li class="m-portlet__nav-item">--}}
    {{--<a href="{{ route('admin.product.create', ['categoryId' => $categoryId]) }}" data-type="modal" data-modal="#superLargeModal"--}}
    {{--class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"--}}
    {{--data-toggle="m-tooltip" data-placement="top" title="Создать продукт">--}}
    {{--<i class="fa fa-plus-circle"></i>--}}
    {{--</a>--}}
    {{--</li>--}}

    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}


    <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                {{--<div class="m-portlet__body">--}}
                {{--<form action="{{route('admin.product.list',  ['categoryId' => $categoryId])}}" method="get"--}}
                {{--class="filter-form" data-table="#productTable">--}}

                {{--<div class="row">--}}
                {{--<div class="col-md-2">--}}
                {{--<div class="form-group">--}}
                {{--<label for="filter.title">Заголовок </label>--}}
                {{--<input type="text" class="form-control" id="filter.title"--}}
                {{--name="filter[title]" autocomplete="off">--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-2">--}}
                {{--<div class="form-group">--}}
                {{--<label for="filter.site_display">Отображенные</label>--}}
                {{--<select name="filter[site_display]" id="filter.site_display" class="form-control">--}}
                {{--<option value="all">Все</option>--}}
                {{--<option value="yes">Да</option>--}}
                {{--<option value="no">Нет</option>--}}
                {{--</select>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<button type="submit" class="btn btn-sm btn-success">Фильтр</button>--}}
                {{--<a href="{{route('admin.product',  ['categoryId' => $categoryId])}}" class="btn btn-sm btn-info">Сбросить</a>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}
                <form action="{{ route('admin.catalog.products', ['categoryId' => $categoryId]) }}" method="post" class="ajax" data-ui-block-type="element"
                      data-ui-block-element="#superLargeModal .modal-body" id="ajaxForm">
                    <table class="table table-bordered m-table ajax-content" id="productTable"
                           data-ajax-content-url="{{ route('admin.catalog.products', ['categoryId' => $categoryId]) }}">
                        <thead>
                        <tr>
                            <th class="text-center" width="50">Id</th>
                            <th class="text-center" width="100">Фото</th>
                            <th class="text-center">Название</th>
                            <th class="text-center">Описание</th>
                            <th class="text-center">Позиция</th>
                            {{--<th>Каталог</th>--}}
                            {{--<th>Цена</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <!--end::Section-->
    </div>
@endsection

@push('modules')
    <script src="/app/js/modules/catalogsManagements.js"></script>
    <script src="/app/js/modules/userManagement.js"></script>
@endpush
