@foreach($products as $product)
    @include('backend.catalogs.product.product_item')
@endforeach

@if(!$products->count())
    <tr>
        <td colspan="5" class="text-center">Данных не найдено</td>
    </tr>
@endif