<tr class="row-{{$item->id}}">
    <td class="text-center">{{$item->id}}</td>
    <td>{{$item->full_name}}</td>
    <td>
        <img width="50px" src="@if(isset($item->mainImage)){{$item->mainImage->original_file_name}}@endif">
    </td>
    <td>{{$item->email}}</td>
    <td>{{$item->phone}}</td>
    <td class="text-center">
        <a class="handle-click" data-type="ajax-get" href="{{route('admin.users.customers.activate',['userId'=>$item->id])}}">
            {!! $item->active !!}
        </a>
    </td>
    <td class="text-center">
        <a href="{{ route('admin.users.customers.edit', ['id' => $item->id]) }}" data-type="modal"
           data-modal="#superLargeModal"
           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
           data-toggle="m-tooltip" data-placement="top" title="Редактировать продукт">
            <i class="fa fa-pencil-square-o"></i>
        </a>
    </td>
    <td class="text-center">
        <a class="handle-click" data-type="delete-table-row" data-confirm-title="Подтвердите действие" data-cancel-text="Отмена" data-confirm-text="Удалить"
           href="{{route('admin.users.customers.delete',['userId'=>$item->id])}}">
            <i class="fa fa-trash"></i>
        </a>
    </td>
</tr>
