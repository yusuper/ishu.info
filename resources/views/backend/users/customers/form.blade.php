<form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
      data-ui-block-element=".m-portlet__body" id="ajaxForm">
    <input name="_token" type="hidden" value="{{ csrf_token() }}">
    {{--<div class="form-group">--}}
        {{--<label for="last_name">Фамилия</label>--}}
        {{--<input type="text" class="form-control" id="last_name" name="last_name"--}}
               {{--@if(isset($customer)) value="{{ $customer->last_name }}" @endif>--}}
        {{--<p class="help-block"></p>--}}
    {{--</div>--}}
    <div class="form-group">
        <label for="full_name">Имя</label>
        <input type="text" class="form-control" id="full_name" name="full_name"
               @if(isset($customer)) value="{{ $customer->full_name }}" @endif>
        <p class="help-block"></p>
    </div>
    <div class="form-group">
        <label for="phone">Телефон</label>
        <input type="text" class="form-control" id="phone" name="phone"
               @if(isset($customer)) value="{{ $customer->phone }}" @endif>
        <p class="help-block"></p>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email" name="email"
               @if(isset($customer)) value="{{ $customer->email }}" @endif>
        <p class="help-block"></p>
    </div>
    <div class="form-group">
        <label for="password">Пароль</label>
        <input type="password" class="form-control" id="password" name="password"
               @if(isset($customer)) placeholder="Измените пароль" @endif>
        <p class="help-block"></p>
    </div>

    <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
        <label for="active" class="m-checkbox">
            <input type="checkbox" name="active" id="active"
                   @if(isset($customer) && $customer->getOriginal('active')) checked @endif>
            Активен
            <span></span>
        </label>
    </div>
    <div class="form-group ">
        <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
    </div>
</form>
