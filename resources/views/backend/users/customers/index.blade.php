@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">

                </ul>
            </div>
        </div>
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">

                <div class="box">
                    <table class="table table-bordered ajax-content" id="ajaxTable"
                           data-ajax-content-url="{{ route('admin.users.customers.list') }}">
                        <thead>
                        <tr>
                            <th class="text-center" width="50">#</th>
                            <th>ФИО</th>
                            <th>Фото</th>
                            <th>E-mail</th>
                            <th>Телефон</th>
                            <th width="50" class="text-center" data-container="body" data-toggle="m-tooltip" data-placement="top" title="Активен"><i class="fa fa-power-off"></i></th>
                            <th class="text-center" width="80"><i class="fa fa-bars" aria-hidden="true"></i></th>
                            <th class="text-center" width="50"><i class="fa fa-trash" aria-hidden="true"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="pagination_placeholder" data-table-id="ajaxTable"></div>
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>
@endsection
