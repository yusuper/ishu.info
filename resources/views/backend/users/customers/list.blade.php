@foreach($items as $item)
    @include('backend.users.customers.item')
@endforeach

@if(!$items->count())
    <tr>
        <td colspan="9" class="text-center">Данных не найдено</td>
    </tr>
@endif
