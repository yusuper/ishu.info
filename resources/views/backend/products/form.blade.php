
<div class="row">
    <div class="@if($create) col-md-12 @else col-md-8 @endif">
        <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
              data-ui-block-element=".m-portlet__body" id="ajaxForm">
            {{--<div class="row">--}}
            <input name="_token" type="hidden" value="{{ csrf_token() }}">

            <ul class="nav nav-tabs " role="tablist">
                @foreach(config('project.locales') as $count => $locale)
                    <li role="presentation" class="nav-item">
                        <a class="@if($count == 0) active @endif nav-link" href="#tab-{{ $locale }}"
                           aria-controls="#tab-{{ $count }}" role="tab"
                           data-toggle="tab">{{ $locale }}</a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach(config('project.locales') as $count => $locale)
                    <div role="tabpanel" class="tab-pane @if($count == 0)  active  @endif "
                         id="tab-{{ $locale }}">
                        <div class="form-group">
                            <label for="title.{{ $locale }}">Заголовок *</label>
                            <input type="text" class="form-control" id="title.{{ $locale }}"
                                   name="title[{{ $locale }}]"
                                   @if(isset($product)) value="{{ $product->getTranslation('title', $locale) }}" @endif>
                            <p class="help-block"></p>
                        </div>

                        <div class="form-group">
                            <label for="short_desc.{{ $locale }}">Краткое описание </label>
                            <textarea rows="3" class="form-control" id="short_desc.{{ $locale }}"
                                      name="short_desc[{{ $locale }}]">@if(isset($product)){{$product->getTranslation('short_desc', $locale)}}@endif</textarea>
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group">
                            <label for="desc.{{ $locale }}">Описание товара *</label>
                            <textarea rows="4" class="form-control editor" id="desc.{{ $locale }}"
                                      name="desc[{{ $locale }}]">@if(isset($product)){{$product->getTranslation('desc', $locale)}}@endif</textarea>
                            <p class="help-block"></p>
                        </div>
                        <div class="form-group ">
                            <label for="meta_title.{{$locale}}">Meta title ({{$locale}})</label>
                            <input type="text" id="meta_title.{{$locale}}" name="meta_title[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($product)) value="{{ $product->getTranslation('meta_title', $locale) }}" @endif>
                        </div>

                        <div class="form-group ">
                            <label for="meta_keywords.{{$locale}}">Meta keywords ({{$locale}})</label>
                            <input type="text" id="meta_keywords.{{$locale}}" name="meta_keywords[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($product)) value="{{ $product->getTranslation('meta_keywords', $locale) }}" @endif>
                        </div>

                        <div class="form-group ">
                            <label for="meta_description.{{$locale}}">Meta description ({{$locale}})</label>
                            <input type="text" id="meta_description.{{$locale}}" name="meta_description[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($product)) value="{{ $product->getTranslation('meta_description', $locale) }}" @endif>
                        </div>
                    </div>
                @endforeach
                <div class="form-group">
                    <label for="price">Цена товара *</label>
                    <input type="text" class="form-control" id="price" name="price"
                           @if(isset($product)) value="{{ $product->price }}" @endif>
                    <p class="help-block"></p>
                </div>

                <div class="form-group">
                    <label for="qty">Количество товара *</label>
                    <input type="text" class="form-control" id="qty" name="qty"
                           @if(isset($product)) value="{{ $product->qty }}" @endif>
                    <p class="help-block"></p>
                </div>

                @if ($create)
                    <div class="form-group">
                        <label for="image">Загрузить фото</label>
                        <input id="image" type="file" class="form-control" name="image">
                        <p class="help-block"></p>
                    </div>
                @endif
            </div>

            <div class="form-group">
                <label for="category_id">Категория</label>
                <select id="category_id" name="category_id[]" class="form-control select2"  multiple="multiple">
                    <option value="">Выберите категорию</option>
                    @foreach($categories as $sectionItem)
                        <option value="{{$sectionItem->id}}"
                                @if(isset($product) && $product->categories->contains($sectionItem->id)) selected @endif>{{$sectionItem->getTranslation('name', 'ru')}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>

            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="new"
                           @if(isset($product) && $product->getOriginal('new')) checked @endif>
                    Добавить на 'НОВЫЕ'
                    <span></span>
                </label>
            </div>
            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="hit"
                           @if(isset($product) && $product->getOriginal('hit')) checked @endif>
                    Добавить в 'ХИТ'
                    <span></span>
                </label>
            </div>
            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="site_display"
                           @if(isset($product) && $product->getOriginal('site_display')) checked @endif>
                    Отображать на сайте
                    <span></span>
                </label>
            </div>
            <div class="form-group ">
                <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
            </div>
            {{--</div>--}}
        </form>
    </div>

    @if(!$create)
        <div class="col-md-4">
            <h4>Главное изображение</h4>
            <div id="mainImagePlaceholder">
                @include('backend.products.partials.main_image')

            </div>

            @if(isset($mainImageUploadUrl))
                <form class="ajax" method="post" action="{{$mainImageUploadUrl}}" id="mainImageForm"
                      data-ui-block-type="element" data-ui-block-element=".m-portlet__body">
                    <input type="file" class="form-control hiddenInputFile" id="mainImageInput" name="mainImage"
                           accept="image/*" style="display: none">
                </form>
            @endif
        </div>
    @endif
</div>
