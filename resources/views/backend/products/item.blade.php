<tr class="row-{{$item->id}}">
    <td class="text-center">{{$item->id}}</td>
    <td class="text-center" width="150">
        @if(isset($item->mainImage))
            <img src="{{asset('storage/uploaded_images/128_' . $item->mainImage->getOriginal('original_file_name'))}}">
        @endif
    </td>
    <td>{{$item->title}}</td>
    <td>{{ $item->short_desc}}</td>
    <td class="text-center">
        @foreach($item->categories as $category)
            {{$category->getTranslation('name', 'ru')}}
            <br>
        @endforeach
    </td>
    <td class="text-center">
        <a href="{{route('admin.promotions.product.create',['productId'=>$item->id])}}"
           data-type="modal"
           data-modal="#regularModal"
           class="handle-click"
           data-container="body"
           data-toggle="m-tooltip"
           data-placement="top"
           title="Категории">
            <i class="fa fa-star"></i>
        </a>
    </td>
    <td class="text-center">{{$item->qty}}</td>
    <td class="text-center">{{$item->price}}</td>
    {{--<td class="text-center">--}}
    {{--<a href="{{route('admin.promotions.product.create',['productId'=>$item->id])}}"--}}
    {{--data-type="modal"--}}
    {{--data-modal="#regularModal"--}}
    {{--class="handle-click"--}}
    {{--data-container="body"--}}
    {{--data-toggle="m-tooltip"--}}
    {{--data-placement="top"--}}
    {{--title="Категории">--}}
    {{--<i class="fa fa-star"></i></a>--}}
    {{--<form id="promotionProductSubmit" class="ajaxForm"--}}
    {{--action="{{route('admin.promotions.product.store',['productId'=>$item->id])}}" method="post">--}}
    {{--<input name="_token" type="hidden" value="{{ csrf_token() }}">--}}
    {{--<select class="product-promotion">--}}
    {{--@foreach($promotions as $promotion)--}}
    {{--<option value="{{$promotion->id}}">{{$promotion->title}}</option>--}}
    {{--@endforeach--}}
    {{--</select>--}}
    {{--</form>--}}
    {{--</td>--}}

    <td class="text-center">{!! $item->site_display !!}</td>
    <td class="text-center">
        <a href="{{ route('admin.products.edit', ['id' => $item->id]) }}" data-type="modal"
           data-modal="#superLargeModal"
           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
           data-toggle="m-tooltip" data-placement="top" title="Редактировать продукт">
            <i class="fa fa-pencil-square-o"></i>
        </a>|
        <a class="handle-click"
           data-type="confirm"
           data-confirm-title="Удаление продукта"
           data-confirm-message="Вы уверены, что хотите удалить продукт?"
           data-cancel-text="Отмена"
           data-confirm-text="Удалить"
           data-follow-url="true"
           href="{{route('admin.products.destroy', ['id' => $item->id])}}"><i class="fa fa-trash-o"></i>
        </a>
    </td>
</tr>