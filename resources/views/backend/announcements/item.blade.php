<tr class="row-{{$announcement->id}}">
    <td class="text-center">{{$announcement->id}}</td>
    <td>@if(isset($announcement->user->full_name)){{$announcement->user->full_name}}@endif</td>
    <td>
        <img width="50px" src="@if(isset($announcement->mainImage)){{$announcement->mainImage->original_file_name}}@endif">
    </td>
    {{--<td>{{$announcement->email}}</td>--}}
    <td>{{$announcement->phone}}</td>
    <td>{{$announcement->desc}}</td>
    <td>
        -
        @foreach($announcement->categories as $category)
            {{$category->name}}-
        @endforeach
    </td>
    <td class="text-center">
        <a class="handle-click" data-type="ajax-get" href="{{route('admin.announcements.activate',['announcementId'=>$announcement->id])}}">
            @if($announcement->active == 1)
                <i class="fa fa-power-off" style="color:green"></i>
            @else
                <i class="fa fa-power-off" style="color:red"></i>
            @endif
        </a>

    </td>
    <td class="text-center">
        <a class="handle-click" data-type="delete-table-row" data-confirm-title="Подтвердите действие" data-cancel-text="Отмена" data-confirm-text="Удалить"
           href="{{route('admin.announcements.delete',['announcementId'=>$announcement->id])}}">
            <i class="fa fa-trash"></i>
        </a>
    </td>
    {{--<td class="text-center">--}}
    {{--<a href="{{ route('admin.users.customers.edit', ['id' => $announcement->id]) }}" data-type="modal"--}}
    {{--data-modal="#superLargeModal"--}}
    {{--class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"--}}
    {{--data-toggle="m-tooltip" data-placement="top" title="Редактировать продукт">--}}
    {{--<i class="fa fa-pencil-square-o"></i>--}}
    {{--</a>--}}
    {{--</td>--}}
</tr>
