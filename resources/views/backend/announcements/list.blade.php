@foreach($announcements as $announcement)
    @include('backend.announcements.item')
@endforeach

@if(!$announcements->count())
    <tr>
        <td colspan="9" class="text-center">Данных не найдено</td>
    </tr>
@endif
