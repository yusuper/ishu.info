@foreach($feedbacks as $feedback)
    <tr class="table-row-{{ $feedback->id }}">
        <td class="text-center">{{ $feedback->id }}</td>
        <td>
            {{--<a href="{{route('feedback.view', ['feedbackId' => $feedback->id])}}">--}}
                {{ $feedback->name }}
                <br>
                {{ $feedback->phone }}
            {{--</a>--}}
        </td>
        <td @if($feedback->status) style="font-weight: 800" @endif>{{ $feedback->email }}</td>
        <td>{{ $feedback->message }}</td>
        <td>{{ date('d.m.Y H:i', strtotime($feedback->created_at)) }}</td>
    </tr>
@endforeach

@if(!$feedbacks->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif