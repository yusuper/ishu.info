@extends('backend.layouts.master')
@section('title')
    {{ $title }}
@endsection

@section('content')
<div class="table-row-{{ $feedback->id }}">
    <p class="align-middle">{{ $feedback->id }}</p>
    <p class="align-middle">{{ $feedback->email }}</p>
    <p class="align-middle">{{ $feedback->phone }}</p>
    <p class="align-middle">{{ $feedback->message }}</p>
    <p class="align-middle">{{ date('d.m.Y H:i', strtotime($feedback->created_at)) }}</p>
    <p><a href="{{route('feedback')}}">К списку сообщений</a></p>
</div>
@endsection