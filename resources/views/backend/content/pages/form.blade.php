<div class="row">
    <div class="@if($create) col-md-12 @else col-md-8 @endif">
        <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
              data-ui-block-element="#superLargeModal .modal-body" id="ajaxForm">
            <ul class="nav nav-tabs" role="tablist">
                @foreach(config('project.locales') as $count => $locale)
                    <li role="presentation" class="nav-item">
                        <a class="@if($count == 0) active @endif nav-link" href="#tab-{{ $locale }}"
                           aria-controls="#tab-{{ $locale }}" role="tab"
                           data-toggle="tab">{{ $locale }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">

                @foreach(config('project.locales') as $count => $locale)
                    <div role="tabpanel" class="tab-pane @if($count == 0)  active  @endif "
                         id="tab-{{ $locale }}">

                        <div class="form-group ">
                            <label for="title.{{$locale}}">Наименование* ({{$locale}})</label>
                            <input type="text" id="title.{{$locale}}" name="title[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($page)) value="{{ $page->getTranslation('title', $locale) }}" @endif>
                        </div>

                        <div class="form-group">
                            <label for="page_content.{{ $locale }}">Контент ({{$locale}})</label>
                            <textarea rows="2" class="form-control editor" id="page_content.{{ $locale }}"
                                      name="page_content[{{ $locale }}]">@if(isset($page)) {{ $page->getTranslation('page_content', $locale) }} @endif</textarea>
                            <p class="help-block"></p>
                        </div>

                        <div class="form-group ">
                            <label for="meta_title.{{$locale}}">Meta title ({{$locale}})</label>
                            <input type="text" id="meta_title.{{$locale}}" name="meta_title[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($page)) value="{{ $page->getTranslation('meta_title', $locale) }}" @endif>
                        </div>

                        <div class="form-group ">
                            <label for="meta_keywords.{{$locale}}">Meta keywords ({{$locale}})</label>
                            <input type="text" id="meta_keywords.{{$locale}}" name="meta_keywords[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($page)) value="{{ $page->getTranslation('meta_keywords', $locale) }}" @endif>
                        </div>

                        <div class="form-group ">
                            <label for="meta_description.{{$locale}}">Meta description ({{$locale}})</label>
                            <input type="text" id="meta_description.{{$locale}}" name="meta_description[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($page)) value="{{ $page->getTranslation('meta_description', $locale) }}" @endif>
                        </div>

                        {{--<div class="form-group ">--}}
                        {{--<label for="url.{{$locale}}">Url* ({{$locale}})</label>--}}
                        {{--<input type="text" id="url.{{$locale}}" name="url[{{$locale}}]"--}}
                        {{--class="form-control m-input"--}}
                        {{--@if(isset($page)) value="{{ $page->getTranslation('url', $locale) }}" @endif>--}}
                        {{--</div>--}}
                    </div>

                @endforeach
            </div>
            <div class="form-group ">
                <label for="prefix">Prefix</label>
                <input type="text" id='prefix' name="prefix"
                       class="form-control m-input"
                       @if(isset($page)) value="{{ $page->prefix }}" @endif>
            </div>
            {{--<div class="form-group">--}}
            {{--<label for="videos">Видео</label>--}}
            {{--<textarea rows="2" class="form-control" id="videos"--}}
            {{--name="videos"></textarea>--}}
            {{--<p class="help-block"></p>--}}
            {{--</div>--}}

            <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                <label class="m-checkbox">
                    <input type="checkbox" name="site_display"
                           @if (isset($page) && $page->site_display) checked="checked" @endif>
                    Отображать на сайте
                    <span></span>
                </label>
            </div>

            <div class="form-group  col-md-12">
                <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
            </div>
        </form>
    </div>
    @if(!$create)
        <div class="col-md-4">
            <fieldset>
                <legend>Изображения</legend>

                <form action="{{route('admin.content.pages.media',['pageId'=>$page->id])}}"
                      method="post"
                      id="formImage">
                    {{csrf_field()}}
                    <input type="file" name="image[]" class="form-input-image" style="display: none" multiple
                           accept="image/x-png,image/gif,image/jpeg">
                    <button type="button" class="btn btn-success btn-sm add-photo">Добавить фото</button>
                </form>
                <div class="media-block">

                    @foreach($medias as $row)
                        <div class="row">
                            @foreach($row as $media)
                                @include('backend.content.pages.media_item')
                            @endforeach
                        </div>
                    @endforeach

                </div>

            </fieldset>
        </div>
    @endif
</div>