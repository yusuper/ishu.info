@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.promotions.create') }}"
                           class="m-portlet__nav-link m-portlet__nav-link--icon" data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="Создать категорию">
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                {{--<div class="m-portlet__body">--}}
                    {{--<form action="{{route('admin.promotions.list')}}" method="get"--}}
                          {{--class="filter-form" data-table="#productsTable">--}}

                        {{--<div class="row">--}}
                            {{--<div class="col-md-2">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="filter.title">По названию</label>--}}
                                    {{--<input type="text" class="form-control" id="filter.title"--}}
                                           {{--name="filter[title]" autocomplete="off">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="filter.site_display">Отображенные</label>--}}
                                    {{--<select name="filter[site_display]" id="filter.site_display" class="form-control">--}}
                                        {{--<option value="all">Все</option>--}}
                                        {{--<option value="yes">Да</option>--}}
                                        {{--<option value="no">Нет</option>--}}
                                    {{--</select>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div>--}}
                            {{--<button type="submit" class="btn btn-sm btn-success">Фильтр</button>--}}
                            {{--<a href="{{route('admin.promotions')}}" class="btn btn-sm btn-info">Сбросить</a>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}

                <table class="table table-bordered m-table ajax-content" id="productsTable"
                       data-ajax-content-url="{{ route('admin.promotions.list') }}">
                    <thead>
                    <tr>
                        <th class="text-center" width="50">Id</th>
                        {{--<th class="text-center" width="150">Фото</th>--}}
                        <th>Название</th>
                        {{--<th>Описание</th>--}}
                        <th>Количество товаров</th>
                        {{--<th>Цена</th>--}}
                        {{--<th>New</th>--}}
                        {{--<th>Хит</th>--}}
                        <th width="50" class="text-center"><i class="fa fa-power-off"></i></th>
                        <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                <div class="pagination_placeholder" data-table-id="productsTable"></div>
            </div>
        </div>
        <!--end::Section-->
    </div>
@endsection

@push('modules')
    {{--<script src="/app/js/modules/specialManagement.js"></script>--}}
@endpush