<tr class="row-{{$item->id}}">
    <td class="text-center">{{$item->id}}</td>
{{--    <td>{{$item->category->name}}</td>--}}
    {{--<td class="text-center" width="150">--}}
        {{--@if(isset($item->mainImage))--}}
            {{--<img src="{{asset('storage/uploaded_images/128_' . $item->mainImage->getOriginal('original_file_name'))}}">--}}
        {{--@endif--}}
    {{--</td>--}}
    <td>{{$item->title}}</td>
    <td>{{$item->products->count()}}</td>
    <td class="text-center">{!! $item->site_display !!}</td>
    <td class="text-center">
        <a href="{{route('admin.promotions.edit', ['promotionId' => $item->id])}}"><i class="fa fa-pencil-square-o"></i></a>
        |
        <a class="handle-click"
           data-type="confirm"
           data-confirm-title="Удаление продукта"
           data-confirm-message="Вы уверены, что хотите удалить?"
           data-cancel-text="Отмена"
           data-confirm-text="Удалить"
           data-follow-url="true"
           href="{{route('admin.promotions.destroy', ['id' => $item->id])}}"><i class="fa fa-trash-o"></i>
        </a>
    </td>
</tr>