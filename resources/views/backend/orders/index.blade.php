@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-section">
            <div class="m-section__content">
                <div class="box">
                    <table class="table table-bordered m-table ajax-content" id="orderTable"
                           data-ajax-content-url="{{ route('admin.orders.list') }}">
                        <thead>
                        <tr>
                            <th class="text-center" width="50">Id</th>
                            <th class="text-center">Клиент</th>
                            <th class="text-center">Продукты</th>
                            <th class="text-center">Общая сумма</th>
                            <th class="text-center">Комментарии к заказу</th>
                            <th class="text-center">Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="pagination_placeholder" data-table-id="orderTable"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modules')
    {{--<script src="/app/js/modules/specialManagement.js"></script>--}}
@endpush