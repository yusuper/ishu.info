@foreach($orders as $order)
    @include('backend.orders.item')
@endforeach

@if(!$orders->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif