<h2>@lang('home.catalog') <i class="fa fa-bars"></i></h2>
<div class="panel-group category-products">
    <ul id="menu-v">
        @foreach($catalogs as $catalog)
            <li class="category-li"><a class="category"
                                       href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $catalog->slug]) }}">
                    {{$catalog->getTranslation('name', $currentLocale)}}
                </a>
                <ul class="sub-menu-v">
                    @foreach($catalog->children as $subCatalog)
                        <div class="sub-div">
                            <li class="sub-cat-li">
                                <a class="sub-cat"
                                   href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) }}">
                                    {{ $subCatalog->getTranslation('name', $currentLocale) }}
                                </a>
                            </li>
                            @foreach($subCatalog->children as $item)
                                <li class="sub-sub-cat-li">
                                    <a class="sub-sub-cat"
                                       href="{{ route('show.catalog',  ['locale' => $currentLocale, 'slug' => $item->slug]) }}">
                                        {{ $item->getTranslation('name', $currentLocale) }}
                                    </a>
                                </li>
                            @endforeach
                        </div>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</div>