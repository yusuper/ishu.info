@if(isset($merge))
    <tr>
        @if($merge->name)
            <th class="search-item">{{$merge->name}}</th>
        @elseif($merge->title)
            <th class="search-item">{{$merge->title}}</th>
        @endif
    </tr>
@else
    <tr class="not-found">
        <th>Не найдено</th>
    </tr>
@endif

