@extends('frontend.layouts.master')


@section('content')
    <form action="{{ route('purchase.checkout', ['locale' => $currentLocale]) }}" method="post" class="ajaxForm">
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">@lang('purchase.home')</a></li>
                    <li class="active">@lang('purchase.shopping_cart')</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">@lang('purchase.item')</td>
                        <td class="description"></td>
                        <td width="100px" class="price text-center">@lang('purchase.price')</td>
                        <td width="150px" class="quantity text-center">@lang('purchase.quantity')</td>
                        <td width="150px" class="total text-center">@lang('purchase.total')</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($products as $product)
                        <tr class="cart_quantity_product">
                            <td class="cart_product">
                                <a href="">
                                    <img width="70px"
                                         src="{{ (isset($product->mainImage) ? $product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg') }}"
                                         alt="">
                                </a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{ $product->getTranslation('title', $currentLocale) }}</a></h4>
                                {{--<p>Web ID: 1089772</p>--}}
                            </td>
                            <td class="cart_price text-center" style="font-size: 20px">
                                {{ $product->price }}
                            </td>
                            <td class="cart_quantity text-center">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up" href=""> + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity[{{$product->id}}][qty]" value="1"
                                           autocomplete="off" size="3">
                                    <a class="cart_quantity_down" href=""> - </a>
                                </div>
                            </td>
                            <td class="cart_total text-center" data-price="{{ $product->price }}">
                                <span class="cart_total_price"> {{ $product->price }}</span>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete clickEvent" href="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'cart' => null, 'productId' => $product->id]) }}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>@lang('purchase.make_purchase')</h3>
                <p>@lang('purchase.please_check')</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>@lang('purchase.total_cart_price') <span id="sum_cart"></span></li>
                            <li>@lang('purchase.delivery') <span id="delivery"></span></li>
                            <li>@lang('purchase.total') <span id="total_sum"></span></li>
                        </ul>
                        <input type="hidden" name="total" id="total">
                        <a class="btn btn-default update" id="calculate" href="">@lang('purchase.count')</a>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul class="user_info">
                            <li class="single_field zip-field">
                                <label for="name"></label>
                                <input type="text" name="name" id="name" placeholder="@lang('purchase.name')">
                                <p class="help-block"></p>
                            </li>
                            <li class="single_field zip-field">
                                <label for="phone"></label>
                                <input type="text" name="phone" id="phone" placeholder="@lang('purchase.phone')">
                                <p class="help-block"></p>
                            </li>
                        </ul>
                        <ul class="user_info">
                            <li class="single_field">
                                <label for="region_id"></label>
                                <select id="region_id" name="region_id"
                                        data-url="{{ route('purchase.get.city', ['locale' => $currentLocale]) }}">
                                    <option value="">@lang('purchase.region')</option>
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->getTranslation('name', $currentLocale) }}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="single_field">
                                <label for="city_id"></label>
                                <select id="city_id" name="city_id">
                                    <option value="">@lang('purchase.city')</option>
                                </select>
                            </li>
                        </ul>
                        <input class="btn btn-default update" type="submit" href="" value="@lang('purchase.make_purchase')">
                        {{--<a class="btn btn-default check_out" href="">Оформить заказ</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
    </form>
@endsection
