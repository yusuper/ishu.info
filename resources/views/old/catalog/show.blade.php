@extends('frontend.layouts.master')


@section('content')
    <div class="container">
        <div class="row">

            <div class="col-sm-12 {{--padding-right--}}">
                <div class="features_items"><!--features_items-->
                    <div class="catalog-child">
                        <h2 class="title pull-left catalogue">@lang('home.catalog') <i class="fa fa-bars"></i></h2>
                        {{--<div class="panel-group category-products" style="display: none;">--}}
                        {{--<ul id="menu-v">--}}
                        {{--@foreach($catalogs as $catalog)--}}
                        {{--<li class="category-li"><a class="category"--}}
                        {{--href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $catalog->slug]) }}">--}}
                        {{--{{$catalog->getTranslation('name', $currentLocale)}}--}}
                        {{--</a>--}}
                        {{--<ul class="sub-menu-v">--}}
                        {{--@foreach($catalog->children as $subCatalog)--}}
                        {{--<div class="sub-div">--}}
                        {{--<li class="sub-cat-li">--}}
                        {{--<a class="sub-cat"--}}
                        {{--href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) }}">--}}
                        {{--{{ $subCatalog->getTranslation('name', $currentLocale) }}--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--@foreach($subCatalog->children as $item)--}}
                        {{--<li class="sub-sub-cat-li">--}}
                        {{--<a class="sub-sub-cat"--}}
                        {{--href="{{ route('show.catalog',  ['locale' => $currentLocale, 'slug' => $item->slug]) }}">--}}
                        {{--{{ $item->getTranslation('name', $currentLocale) }}--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--@endforeach--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        <h2 class="title text-center">{{--@lang('catalog.category_catalog') --}}{{ $catalog->getTranslation('name', $currentLocale) }}</h2>
                        @if(isset($catalog->children))
                            @foreach($catalog->children as $child)
                                <div class="col-sm-12">
                                    {{--<div class="product-image-wrapper">--}}
                                    <div class="single-products">
                                        <div class="productinfo">
                                            {{--{{dd($child)}}--}}
                                            @if(isset($child->children) && count($child->children) != 0)
                                                <h4 class="text-center"><a
                                                            href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $child->slug]) }}">{{ $child->getTranslation('name', $currentLocale) }}</a>
                                                </h4>
                                                <div class="">
                                                    @foreach($child->children as $item)
                                                        <a href="{{ route('show.catalog',  ['locale' => $currentLocale, 'slug' => $item->slug]) }}">
                                                            <div class="col-sm-3 catalog-item{{-- pull-left--}}">
                                                                <h5 class="text-center">{{ $item->getTranslation('name', $currentLocale) }}</h5>
                                                                @if (isset($item->medias))

                                                                    <img class="catalog-item-image" width="100%"
                                                                         height="100%"
                                                                         src="{{ (isset($item->medias)) ? $item->medias->original_file_name : '' }}"
                                                                         alt="{{ (isset($item->medias)) ? $item->medias->original_file_name : '' }}"/>

                                                                @endif
                                                            </div>
                                                        </a>
                                                    @endforeach
                                                    @else
                                                        <div class="col-sm-3 catalog-item">
                                                            <a href="{{ route('show.catalog',  ['locale' => $currentLocale, 'slug' => $child->slug]) }}">
                                                                <div class="col-sm-3 catalog-item{{-- pull-left--}}">
                                                                    <h5 class="text-center">{{ $child->getTranslation('name', $currentLocale) }}</h5>
                                                                    @if (isset($item->medias))

                                                                        <img class="catalog-item-image" width="100%"
                                                                             height="100%"
                                                                             src="{{ (isset($child->medias)) ? $child->medias->original_file_name : '' }}"
                                                                             alt="{{ (isset($child->medias)) ? $child->medias->original_file_name : '' }}"/>

                                                                    @endif
                                                                </div>
                                                            </a>
                                                        </div>
                                                </div>
                                            @endif
                                        </div>

                                        {{--@if (isset($child->products))--}}
                                        {{--@foreach($child->products as $product)--}}
                                        {{--<span>--}}
                                        {{--<a href="{{ route('show.products', ['locale' => $currentLocale, 'catalogSlug' => $child->slug, 'slug' => $product->slug ]) }}">--}}
                                        {{--{{ $product->getTranslation('title', $currentLocale) }}--}}
                                        {{--</a>--}}
                                        {{--</span>--}}
                                        {{--<br>--}}
                                        {{--@endforeach--}}
                                        {{--@endif--}}

                                    </div>
                                </div>
                                {{--</div>--}}
                    </div>
                    @endforeach
                    @endif
                    <div>
                        <div class="catalog-child">
                            @foreach($catalog->products as $product)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                @if($product->mainImage)
                                                    <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$catalog->slug,'slug'=>$product->slug])}}">
                                                        <img style="height: 200px;" class="img-thumbnail"
                                                             src="{{ $product->mainImage->original_file_name }}"></a>
                                                @else
                                                    <img style="height: 200px;" class="img-thumbnail"
                                                         src="/app/images/no_image_placeholder.jpg">
                                                @endif
                                                <h2>{{ $product->price }}</h2>
                                                <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>

                                                <a href="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'cart' => true, 'productId' => $product->id]) }}"
                                                   class="btn btn-default add-to-cart clickEvent product-added-{{ $product->id }}">
                                                    <i class="fa fa-shopping-cart"></i>@lang('home.to_cart')
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>


            </div><!--features_items-->

            <div class="recommended_items"><!--recommended_items-->
                <h2 class="title text-center">@lang('home.recommended_items')</h2>

                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend1.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend2.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend3.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend1.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend2.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/template-html/images/home/recommend3.jpg" alt=""/>
                                            <h2>$56</h2>
                                            <p>Easy Polo Black Edition</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div><!--/recommended_items-->

        </div>
    </div>
    </div>

@endsection


@push('modules')
    <script>


    </script>
@endpush