@extends('frontend.layouts.master')


@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Каталог</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#">Категория</a></h4>
                                </div>
                            </div>
                        </div><!--/category-products-->

                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--products-details-->
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="view-product">
                                    {{--{{dd($product)}}--}}
                                    @if($product->mainImage)
                                        <img
                                             src="{{ $product->mainImage->original_file_name }}">
                                    @else
                                        <img
                                             src="/app/images/no_image_placeholder.jpg">
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/products-information-->
                                    <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                                    <h2>{{$product->title}}</h2>
                                    <p>Код товара: 1089772</p>
                                    <span>
                                            <span>Цена: {{$product->price}}</span>
                                            {{--<span>Осталось: {{$product->qty}} шт.</span>--}}
                                            <label>Количество:</label>
                                            <input type="text" value="3" />
                                            <button type="button" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                В корзину
                                            </button>
                                        </span>
                                    <p><b>Количество на складе:</b> {{$product->qty}}шт.</p>
                                    {{--<p><b>Состояние:</b> Новое</p>--}}
                                    {{--<p><b>Производитель:</b> D&amp;G</p>--}}
                                </div><!--/products-information-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Описание товара</h5>
                                <p>{{$product->getTranslation('desc',$currentLocale)}}</p>
                            </div>
                        </div>
                    </div><!--/products-details-->

                </div>
            </div>
        </div>
    </section>

@endsection


@push('modules')
    <script>


    </script>
@endpush