<div class="features_items"><!--features_items-->
    <h2 class="title text-center">Новые товары</h2>
    <div class="catalog-child">
        @foreach($new_products as $product)
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            @if($product->mainImage)
                                <img style="height: 160px" class="img-thumbnail" src="{{ $product->mainImage->original_file_name }}">
                            @else
                                <img style="height: 160px" class="img-thumbnail" src="/app/images/no_image_placeholder.jpg">
                            @endif
                            <h2>{{ $product->price }}</h2>
                            <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>
                            <a href="#" class="btn btn-default add-to-cart"><i
                                        class="fa fa-shopping-cart"></i>В
                                корзину</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div><!--features_items-->