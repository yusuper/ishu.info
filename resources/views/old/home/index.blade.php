@extends('frontend.layouts.master')


@section('content')
    <section>

        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        @include('frontend.layouts.catalog')
                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">@lang('home.new_items')</h2>
                        <div class="catalog-child">
                            @foreach($new_products as $product)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                @if($product->mainImage)
                                                    <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                        <img style="height: 200px;" class="img-thumbnail"
                                                             src="{{ $product->mainImage->original_file_name }}">
                                                    </a>
                                                @else
                                                    <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                        <img style="height: 200px;" class="img-thumbnail"
                                                             src="/app/images/no_image_placeholder.jpg">
                                                    </a>
                                                @endif
                                                <h2>{{ $product->price }}</h2>
                                                <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                    <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>
                                                </a>

                                                <a href="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'cart' => true, 'productId' => $product->id]) }}"
                                                   class="btn btn-default add-to-cart clickEvent product-added-{{ $product->id }}">
                                                    <i class="fa fa-shopping-cart"></i>@lang('home.to_cart')
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div><!--features_items-->


                    <div class="recommended_items"><!--recommended_items-->
                        <h2 class="title text-center">@lang('home.recommended_items')</h2>

                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($hit_products as $key => $hit_product)
                                    <div class="item {{ ($key == 0) ? 'active': '' }}">
                                        @foreach($hit_product as $product)
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            @if($product->mainImage)
                                                                <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                                    <img style="height: 290px" ; class="img-thumbnail"
                                                                         src="{{ $product->mainImage->original_file_name }}">
                                                                </a>
                                                            @else
                                                                <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                                    <img style="height: 290px" ; class="img-thumbnail"
                                                                         src="/app/images/no_image_placeholder.jpg">
                                                                </a>
                                                            @endif
                                                            <h2>{{ $product->price }}</h2>
                                                            <a href="{{route('show.products',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}">
                                                                <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>
                                                            </a>
                                                            <a href="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'cart' => true, 'productId' => $product->id]) }}"
                                                               class="btn btn-default add-to-cart clickEvent product-added-{{ $product->id }}"><i
                                                                        class="fa fa-shopping-cart"></i>@lang('home.to_cart')
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                            <a class="left recommended-item-control" href="#recommended-item-carousel"
                               data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right recommended-item-control" href="#recommended-item-carousel"
                               data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div><!--/recommended_items-->
                </div>
            </div>
        </div>
    </section>

@endsection


@push('modules')

@endpush
