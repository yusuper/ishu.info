<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/frontend/logo/logo.png" type="image/png">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <title>services</title>
    {{--<link href=/static/css/vendor.css rel=stylesheet>--}}
    <link href=/static/css/app.e6267e1ecd259a0fcb5fd81ada780916.css rel=stylesheet>
</head>
<body>
<div id=app></div>
<script type=text/javascript src=/static/js/manifest.2ae2e69a05c33dfc65f8.js></script>
<script type=text/javascript src=/static/js/vendor.3c88ed9e4a4ddceddd5f.js></script>
<script type=text/javascript src=/static/js/app.96ab740a3b271641b5a9.js></script>
<script type=text/javascript src=/static/vendor.js></script>
<script type=text/javascript src=/static/main.js></script>
{{--<script type=text/javascript src=/static/hashchange.js></script>--}}
{{--<script type=text/javascript src=/static/modernizr.js></script>--}}
</body>
</html>