<?php


namespace App\Services;


use http\Env\Request;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use App\Models\Media;

class MediaService
{
    private $image;
    private $path;
    private $thumbSizes = ['512', '256', '128'];
    private $defaultSize = 1024;
    private $media;
    public  $errorMessage;

    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    /**
     * @param UploadedFile $image
     * @param null $type
     * @param null $owner
     * @param null $modelId
     * @param null $params
     * @param null $mainImage
     * @param null $order
     * @return mixed
     */
    public function uploadImage(UploadedFile $image, $type = null, $owner = null, $modelId = null, $params = null, $mainImage = null, $order = null)
    {
        $uploadPath = config('project.media_upload_path');

        if ($image->isValid()) {
            $clientFileName = $image->getClientOriginalName();
            $originalFileName = uniqid() . '.' . $image->guessClientExtension();
            $fileSize = $image->getSize();
            $mime = $image->getClientMimeType();
            $img = Image::make($image);
            $img->save($uploadPath . '/' . $originalFileName);
            foreach ($this->thumbSizes as $size)
            {
                $this->resize($img, $size, $uploadPath . '/' . $size . '_' . $originalFileName);
            }
            return $this->media->create([
                'owner' => $owner,
                'model_id' => $modelId,
                'client_file_name' => $clientFileName,
                'original_file_name' => $originalFileName,
                'size' => $fileSize,
                'mime' => $mime,
                'type' => $type,
                'main_image'=>$mainImage,
                'params' => $params,
                'order' => $order
            ]);
        }
    }

    public function cropExistingImage($mediaId, $width, $height, $offsetX, $offsetY)
    {
        $media = $this->media->find($mediaId);

        if (!$media)
        {
            $this->errorMessage = 'Запись с ID:' . $mediaId . ' не найдена';
            return false;
        }

        if (!file_exists(storage_path('app/public/uploaded_images/' . $media->getOriginal('original_file_name'))))
        {
            $this->errorMessage = 'Изображение:' . $media->getOriginal('original_file_name') . ' не найдено';
            return false;
        }

        $image = Image::make(storage_path('app/public/uploaded_images/' . $media->getOriginal('original_file_name')));

        $image->crop($width, $height, $offsetX, $offsetY);

        $image->save(storage_path('app/public/uploaded_images/' . $media->getOriginal('original_file_name')));

        foreach ($this->thumbSizes as $size)
        {
            $this->resize($image, $size, storage_path('app/public/uploaded_images/' . $size . '_' . $media->getOriginal('original_file_name')));
        }

        return $media;

    }

    private function resize(\Intervention\Image\Image $image, $size, $path)
    {
        $image->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save($path);
    }

    public function uploadFile(UploadedFile $file, $owner = null, $modelId = null, $params = null)
    {
        $clientFileName = $file->getClientOriginalName();
        $originalFileName = uniqid() . '.' . $file->guessClientExtension();
        $fileSize = $file->getClientSize();
        $mime = $file->getClientMimeType();
        $file->storeAs('public/media', $originalFileName);
        $model = $this->media->create([
            'type' => 'file',
            'owner' => $owner,
            'model_id' => $modelId,
            'client_file_name' => $clientFileName,
            'original_file_name' => $originalFileName,
            'size' => $fileSize,
            'mime' => $mime,
            'main_image' => 0,
            'params' => $params
        ]);
        return $model;
    }

    public function uploadVideo($url, $owner = null, $modelId = null, $order = null, $params = null)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);

        if (count($my_array_of_vars)) {
            $this->media->create([
                'owner' => $owner,
                'type' => 'video',
                'model_id' => $modelId,
                'order' => $order,
                'params' => $params,
                'client_file_name' => $url,
                'video_id' => $my_array_of_vars['v']
            ]);
        }

    }

    public function editVideo(UploadedFile $image, $mediaId, $params, $order = null)
    {
        $uploadPath = config('project.media_upload_path');
        if ($image->isValid()) {
            $originalFileName = uniqid() . '.' . $image->guessClientExtension();
            $fileSize = $image->getClientSize();
            $mime = $image->getClientMimeType();
            $img = Image::make($image);
            if ($img->width() > 1024) {
                $img->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($uploadPath . '/' . $originalFileName);

            foreach ($this->thumbSizes as $size) {
                $img->resize($size, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($uploadPath . '/' . $size . '_' . $originalFileName);
            }
            $video = $this->media->where(['id' => $mediaId, 'type' => 'video'])->first();
//            dd($video);
            $video->update([
                'original_file_name' => $originalFileName,
                'size' => $fileSize,
                'mime' => $mime,
                'params' => $params,
                'order' => $order
            ]);
        }
    }

    /**
     * @param array $thumbSizes
     * @return $this
     */
    public function setThumbSizes(array $thumbSizes)
    {
        $this->thumbSizes = $thumbSizes;
        return $this;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setDefaultSize(int $size)
    {
        $this->defaultSize = $size;
        return $this;
    }

    /**
     * @param $owner
     * @param $modelId
     * @return mixed
     */
    public function getModelMedia($owner, $modelId)
    {
        return $this->media->where('owner', $owner)->where('model_id', $modelId)->orderBy('main_image', 'desc')->get();
    }

    /**
     * @param $mediaId
     */
    public function setMain($mediaId)
    {
        $media = $this->media->find($mediaId);

        if ($media) {
            $this->media->where('owner', $media->owner)->where('model_id', $media->model_id)->update(['main_image' => 0]);
            $media->main_image = 1;
            $media->save();
        }
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function getEditorMedia($ids = [])
    {
        $res = $this->media->where('owner', 'editor');
        if (count($ids)) {
            $res->whereIn('id', $ids);
        }
        return $res->get();
    }

    public function mainMedia($modelId, $mediaId, $owner)
    {
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', $owner)
            ->where('model_id', $modelId)->get();

        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', $owner)->where('model_id', '=', $modelId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }


    public function deleteMediaItem($mediaId, $owner)
    {
        $media = $this->media->where('owner', $owner)->find($mediaId);
        $path = config('project.media_upload_path');
        $originalMedia = $media->getOriginal('original_file_name');
//        dd($path);
//        dd(file_exists($path . '/' . $originalMedia));
        if ($media->type == 'image') {
            if (file_exists($path . '/' . $originalMedia)) {
                unlink($path . '/' . $originalMedia);
            }
            foreach ($this->thumbSizes as $size) {
                if (file_exists($path . '/' . $size . '_' . $originalMedia)) {
                    unlink($path . '/' . $size . '_' . $originalMedia);
                }
            }
        }
        $media->delete();
    }

    public function deleteMediaList($owner, $modelId)
    {
        $medias = $this->media->where(['owner' => $owner, 'model_id' => $modelId])->get();
        foreach ($medias as $media) {
            if ($media->type == 'image') {
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
    }

    public function deleteMainImage($owner, $modelId)
    {
        $item = $this->media->where('owner', $owner)->where('model_id', $modelId)->where('main_image', 1)->first();
        $uploadPath = config('project.media_upload_path');

        if ($item)
        {
            $path = $uploadPath . '/' . $item->getOriginal('original_file_name');
            if (file_exists($path))
            {
                @unlink($path);
            }

            foreach ($this->thumbSizes as $size)
            {
                $path = $uploadPath . '/' . $size . '_' . $item->getOriginal('original_file_name');

                if (file_exists($path))
                {
                    @unlink($path);
                }
            }

            $item->delete();
        }


    }

}