<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use stdClass;

use App\Models\Customer;

class ChangePasswordEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;
    public $params;

    /**
     * ConfirmationEmail constructor.
     * @param Customer $customer
     * @param stdClass $params
     */
    public function __construct(Customer $customer, stdClass $params)
    {
        $this->customer = $customer;
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = route('frontend.auth.forgot.password', [
            'locale' => $this->params->locale,
            'code' => $this->customer->restore_code,
            ]);

        $subject = config('project.mail_subjects.change_password.' . $this->params->locale);
        $template = 'mail.customer.change_password.' . $this->params->locale;

        return $this->subject($subject)->view($template)->with([
            'link' => $link,
            'customer' => $this->customer,
        ]);
    }
}
