<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use stdClass;

use App\Models\User;

class ConfirmationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $params;
    /**
     * @var User
     */
    private $user;

    /**
     * ConfirmationEmail constructor.
     * @param User $user
     * @param stdClass $params
     */
    public function __construct(User $user, stdClass $params)
    {
        $this->params = $params;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = 'http://ishu.info/auth/confirm/'.$this->user->confirm_code;

        $subject = config('project.mail_subjects.register_confirmation.ru'/* . $this->params->locale*/);
        $template = 'mail.customer.register_confirmation.ru'/* . $this->params->locale*/;

        return $this->subject($subject)->view($template)->with([
            'link' => $link,
            'customer' => $this->user,
//            'password' => $this->params->password
        ]);
    }
}
