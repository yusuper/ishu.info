<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use stdClass;
use App\Models\Subscriber;

class SubscriptionConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subscriber;
    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber, stdClass $params)
    {
        $this->subscriber = $subscriber;
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $link = route('frontend.auth.subscription.confirm', [
            'locale' => $this->params->locale,
            'code' => $this->subscriber->confirm_code,

        ]);

        $subject = config('project.mail_subjects.subscription_confirmation.' . $this->params->locale);
        $template = 'mail.customer.subscription_confirmation.' . $this->params->locale;

        return $this->subject($subject)->view($template)->with([
            'link' => $link,
            'password' => $this->params->password,
            'customer_registered' => $this->params->customer_registered,
            'subscriber' => $this->subscriber->name,
        ]);
    }
}
