<?php

namespace App\Mail\Feedback;

use App\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailForFeedback extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Feedback
     */
    private $feedback;

    /**
     * Create a new message instance.
     *
     * @param Feedback $feedback
     */
    public function __construct(Feedback $feedback)
    {

        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Сообщение с сайта')
            ->view('mail.feedback.feedback', ['feedback' => $this->feedback]);
    }
}
