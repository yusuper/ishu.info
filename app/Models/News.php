<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;


class News extends Model implements Feedable
{
    use Sluggable;
    use HasTranslations;

    public $translatable = ['title', 'short_content', 'long_content', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table = 'news';

    protected $fillable = [
        'category_id',
        'title',
        'short_content',
        'long_content',
        'is_pinned',
        'is_main',
        'site_display',
        'meta_description',
        'meta_keywords',
        'meta_title',
        'slug'
    ];

    public function setSiteDisplayAttribute($value)
    {
        $this->attributes['site_display'] = ($value) ? 1 : 0;
    }


    public function toFeedItem()
    {
        $locale = app()->getLocale();

        return FeedItem::create([
            'id' => $this->id,
            'title' => $this->getTranslation('title', $locale),
            'summary' => $this->getTranslation('short_content', $locale),
            'updated' => $this->updated_at,
            'link' => $locale.'/news/'.$this->slug,
            'author' => 'moet',
        ]);
    }

    public static function getFeedItems()
    {
        return News::where('site_display', true)->get();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'news')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','news')
            ->where('main_image','=',1);
    }
}
