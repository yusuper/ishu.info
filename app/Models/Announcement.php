<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Announcement extends Model
{
    use SoftDeletes;
    use Sluggable;
    use HasTranslations;
    public $translatable = ['title', 'short_desc', 'desc', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table ='announcements';

    protected $fillable = [
        'category_id',
        'user_id',
        'city_id',
        'title',
        'desc',
        'active',
        'is_view',
        'slug',
        'price',
        'discount',
        'address',
        'phone',
        'whatsapp',
        'telegram',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'up'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function categories()
    {
        return $this->belongsToMany(
            \App\Models\Category::class,
            'announcement_categories',
            'announcement_id',
            'category_id'
        )/*->withPivot('position')*/;
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','announcements')
            ->where('main_image','=',1);
    }


    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', '=','announcements')
            ->orderBy('created_at','asc');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

//    public function getActiveAttribute($value)
//    {
//        $color = ($value) ? 'green' : 'red';
//
//        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
//    }

}
