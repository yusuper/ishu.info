<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';

    protected $fillable = [
        'customer_id',
        'favoritesable_id',
        'favoritesable_type',
    ];

    public function favoritesable()
    {
        return $this->morphTo();
    }
}
