<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class City extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $table = 'cities';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'region_id',
        'name',
    ];

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
