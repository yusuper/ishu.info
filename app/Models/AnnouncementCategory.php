<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnnouncementCategory extends Model
{
    protected $table = 'announcement_categories';

    protected $fillable = [
        'category_id',
        'announcement_id'
    ];
}
