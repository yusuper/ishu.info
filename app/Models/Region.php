<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Region extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $table = 'regions';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
    ];

    public function cities()
    {
        return $this->hasMany(City::class, 'region_id');
    }
}
