<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Products extends Model
{
    use Sluggable;
    use HasTranslations;
    use SoftDeletes;



    public $translatable = ['title', 'short_desc', 'desc', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table = 'products';
    protected $fillable = [
        'title',
        'slug',
        'short_desc',
        'desc',
        'price',
        'qty',
        'site_display',
        'new',
        'hit',
        'meta_keywords',
        'meta_description',
        'meta_title',
    ];

    protected $dates = ['deleted_at'];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function promotions()
    {
        return $this->belongsToMany(
            \App\Models\Promotion::class,
            'promotion_products',
            'product_id',
            'promotion_id'
        )->withPivot('promotion_id','product_id');
    }

    public function setSiteDisplayAttribute($value)
    {
        $this->attributes['site_display'] = ($value) ? 1 : 0;
    }

    public function setNewAttribute($value)
    {
        $this->attributes['new'] = ($value) ? 1 : 0;
    }

    public function setHitAttribute($value)
    {
        $this->attributes['hit'] = ($value) ? 1 : 0;
    }

    public function getSiteDisplayAttribute($value)
    {
        $color = ($value) ? 'green' : 'red';

        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
    }

    public function getNewAttribute($value)
    {
        $color = ($value) ? 'blue' : 'transparent';

        return '<i class="fa fa-flag" style="color:'. $color .'"></i>';
    }

    public function getHitAttribute($value)
    {
        $color = ($value) ? 'gold' : 'transparent';

        return '<i class="fa fa-star" style="color:'. $color .'"></i>';
    }

//    public function category()
//    {
//        return $this->belongsTo(Category::class, 'category_id');
//    }


    public function categories()
    {
        return $this->belongsToMany(
            \App\Models\Category::class,
            'product_categories',
            'product_id',
            'category_id'
        )->withPivot('position');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','products')
            ->where('main_image',1);
    }

//    public function favorite(){
//        $this->belongsTo(Favorite::class, 'favoritesable_id');
//    }
}
