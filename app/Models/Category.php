<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Translatable\HasTranslations;
use Kalnoy\Nestedset\NodeTrait;


class Category extends Model
{
    use HasTranslations;

    use NodeTrait, Sluggable {
        Sluggable::replicate as replicateSluggable;
        NodeTrait::replicate insteadof Sluggable;
    }

    public $translatable = ['name', 'url', 'meta_description', 'meta_keywords', 'meta_title'];

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'parent_id',
        'owner',
        'url',
        'target',
        'handler',
        'meta_description',
        'meta_keywords',
        'meta_title',
        'page_id',
    ];


    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->getTranslation('name', 'ru'),
        ];
    }



    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function medias()
    {
        return $this->hasOne(Media::class, 'model_id')
            ->where('owner', 'categories')->where('main_image', 1);
    }


    public function products()
    {
        return $this->belongsToMany(
            \App\Models\Products::class,
            'product_categories',
            'category_id',
            'product_id'
        )->withPivot('position')->orderBy('position', 'asc');
    }

    public function services()
    {
        return $this->hasMany(Service::class,'category_id')
            ->orderBy('created_at','desc');
    }

    public function page(){
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function filter()
    {
        $q = $this->orderBy('created_at','desc');
        if($this->filterName){
            $q->where('name','like',"%$this->filterName%");
        }

        return $q->get();
    }

    public function setFilterName($filterName)
    {
        $this->filterName = $filterName;
        return $this;
    }
}
