<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserServiceRating extends Model
{
    protected $table = 'user_service_ratings';

    protected $fillable = [
        'user_id',
        'service_id',
        'rating'
    ];
}
