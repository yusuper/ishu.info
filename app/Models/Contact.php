<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Contact extends Model
{
    use HasTranslations;

    public $translatable = ['meta_description', 'meta_keywords', 'meta_title'];

    protected $fillable = [
        'name',
        'phone',
        'address',
        'photo',
        'meta_description',
        'meta_keywords',
        'meta_title',
    ];
}
