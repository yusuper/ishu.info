<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Translatable\HasTranslations;

class Service extends Model
{
    use Sluggable;
    use HasTranslations;
    public $translatable = ['title', 'short_desc', 'desc', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table ='services';
    protected $fillable = [
        'title',
        'category_id',
        'city_id',
        'user_id',
        'full_name',
        'profession',
        'short_desc',
        'desc',
        'active',
        'is_view',
        'slug',
        'price',
        'discount',
        'address',
        'phone',
        'whatsapp',
        'telegram',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'up'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(
            \App\Models\Category::class,
            'service_categories',
            'service_id',
            'category_id'
        )/*->withPivot('position')*/;
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','services')
            ->where('main_image',1);
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'services')
            ->orderBy('created_at','asc');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->with('mainImage');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function rating()
    {
        return $this->belongsToMany(
            \App\Models\User::class,
            'user_service_ratings',
            'service_id',
            'user_id')->withPivot('rating');
    }

//    public function getActiveAttribute($value)
//    {
//        $color = ($value) ? 'green' : 'red';
//
//        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
//    }

}
