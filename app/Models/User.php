<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword;
    protected $table = 'users';

    protected $fillable = [
        'region_id',
        'city_id',
        'full_name',
        'api_token',
        'first_name',
        'last_name',
        'phone',
        'address',
        'email',
        'password',
        'gender',
        'avatar',
        'birth_date',
        'active',
        'confirm_code',
        'restore_code',
    ];


    public function setPasswordAttribute($value)
    {
        if ($value)
        {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = date("Y-m-d", strtotime($value));
    }

    public function getGenderAttribute($value)
    {
        return ($value == 'm') ? '<i class="fa fa-male" style="color:green"></i>' : '<i class="fa fa-female" style="color:deeppink"></i>';
    }


    public function getAuthAttribute($value)
    {
        $color = ($value) ? 'green' : '#EBECF4';
        return '<i class="fa fa-file-word-o" style="color:'. $color .'"></i>';
    }

    public function getActiveAttribute($value)
    {
        $color = ($value) ? 'green' : 'red';

        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','users')
            ->where('main_image',1);
    }
    public function userServiceRating()
    {
        return $this->belongsToMany(
            \App\Models\UserServiceRating::class,
            'user_service_rating',
            'user_id',
            'service_id');
    }

    public function identities() {
        return $this->hasMany('App\SocialIdentity');
    }

}
