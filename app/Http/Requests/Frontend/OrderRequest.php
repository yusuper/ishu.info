<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\MessagesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrderRequest extends FormRequest
{
    use MessagesTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $customer = Auth::guard('customers')->user();

        if ($customer){
            return [
                "quantity" => "required",
            ];
        }
        return [
            "quantity" => "required",
            "name" => "required",
            "phone" => "required",
        ];
    }
}
