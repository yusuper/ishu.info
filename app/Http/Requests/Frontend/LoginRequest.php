<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\MessagesTrait;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    use MessagesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "auth_email" => "required|email",
            "auth_password" => "required",
        ];
    }
}
