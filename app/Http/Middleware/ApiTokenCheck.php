<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class ApiTokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('api_token',$request->api_token)->first();

        if(!$user){
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
