<?php namespace App\Http\Controllers;

use App\Mail\Customer\ChangePasswordEmail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Customer\ConfirmationEmail;

trait CustomerTrait {

    public function registerCustomer(array $data)
    {
        return $this->user->create($data);
    }

    public function sendConfirmMail(User $user,  $password = null)
    {
//        var_dump($password);
//        die;
        $params = new \stdClass;
        $params->password = $password;

        Mail::to($user->email)->queue(new ConfirmationEmail($user, $params));
    }

    public function changePasswordMail(User $user, $locale)
    {
        $params = new \stdClass;
        $params->locale = $locale;

        Mail::to($user->email)->queue(new ChangePasswordEmail($user, $params));
    }
}