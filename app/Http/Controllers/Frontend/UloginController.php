<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\CustomerTrait;
use App\Http\Controllers\ResponseTrait;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Ixudra\Curl\Facades\Curl;

class UloginController extends Controller
{
    use CustomerTrait;
    use ResponseTrait;

    public function login(Request $request, $locale)
    {
        // Get information about user.
//        $query = "http://ulogin.ru/token.php?token=" . $request->get('token') . '&host=' . $_SERVER['HTTP_HOST'];
//
//        $response = Curl::to($query)
//            ->withTimeout(5)
//            ->returnResponseObject()
//            ->get();
//
//        dd($query, $response);
//
//        $user = json_decode($response->content, TRUE);

        $data = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($data, TRUE);

        $customer = Customer::where('email', $user['email'])->first();

        if(!$customer){
            // Create new user in DB.
            Customer::create([
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'active' => 1,
                'email' => $user['email'],
                'password' => 123,
            ]);
        }

        if (!Auth::guard('customers')->attempt([
            'email' => $user['email'],
            'password' => 123,
            'active' => 1
        ])) {
            return response()->json([
                'type' => 'alert',
                'header' => __('faq.alerts.submit_error_user_reg.header'),
                'message' => __('faq.alerts.submit_error_user_reg.message'),
                'alert_type' => 'error'
            ], '403');
        }

        return redirect()->to(route('home', ['locale' => $locale]));
    }
}
