<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Favorite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavoritesController extends Controller
{
    /**
     * @var Favorite
     */
    private $favorite;

    public function __construct(Favorite $favorite)
    {
        $this->favorite = $favorite;
    }

    public function addFavorite($locale, $model, $id)
    {
        $this->favorite->create([
            'customer_id' => Auth::guard('customers')->user()->id,
            'favoritesable_id' => $id,
            'favoritesable_type' => $model,
        ]);

        $favoriteCount = $this->favorite->where([
            'customer_id' => Auth::guard('customers')->user()->id,
            'favoritesable_type' => $model,
        ])->count();

        return response()->json([
            'favoriteCount' => $favoriteCount
        ]);
    }
    public function removeFavorite($locale, $model, $id)
    {
        $favorite = $this->favorite->where([
            'customer_id' => Auth::guard('customers')->user()->id,
            'favoritesable_id' => $id,
            'favoritesable_type' => $model,
        ])->first();

        $favorite->delete();

        $favoriteCount = $this->favorite->where([
            'customer_id' => Auth::guard('customers')->user()->id,
            'favoritesable_type' => $model,
        ])->count();


        return response()->json([
            'favoriteCount' => $favoriteCount
        ]);
    }
}
