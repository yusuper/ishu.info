<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomerTrait;
use App\Http\Controllers\ResponseTrait;
use App\Http\Requests\Backend\UsersRequest;
use App\Http\Requests\Frontend\CustomerRequest;
use App\Http\Requests\Frontend\LoginRequest;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Models\City;
use App\Models\Favorite;
use App\Models\Order;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// models
use App\Models\Region;
use App\Models\Customer;
use Torann\LaravelMetaTags\Facades\MetaTag;


class AuthController extends Controller
{
    use ResponseTrait;
    use CustomerTrait;

    private $city;
    private $region;
    private $subscriber;
    private $imageService;
    /**
     * @var Favorite
     */
    private $favorite;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var User
     */
    private $user;

    public function __construct(Region $region, User $user, ImageService $imageService, City $city, Favorite $favorite, Order $order)
    {
        $this->city = $city;
        $this->region = $region;
        $this->imageService = $imageService;
        $this->favorite = $favorite;
        $this->order = $order;
        $this->user = $user;
    }

    public function getRegister($locale)
    {
        $regions = $this->region->get();
        $cities = $this->city->get();

        return view('frontend.account.index', [
            'regions' => $regions,
            'cities' => $cities
        ]);
    }

    public function postRegister(RegisterRequest $request, $locale)
    {
        $request->merge([
            'confirm_code' => md5(uniqid())
        ]);

        $customer = $this->registerCustomer($request->all());

        $this->sendConfirmMail($customer, $locale);

        return response()->json([
            'type' => 'alert',
            'alert_type' => 'success',
            'header' => 'Спасибо за регистрацию!',
            'message' => 'Перейдите на свою электронную почту для подтверждения регистрации',
            'url' => route('home', ['locale' => $locale])
        ]);
    }

    public function getLogin()
    {
        MetaTag::set('title', 'Регистрация');
        MetaTag::set('description', 'Регистрация Авторизация пользователя');
        MetaTag::set('keywords', 'Регистрация Авторизация пользователя');
        return view('frontend.account.login');
    }
//TODO this part auth
    public function profile()
    {
        $customer = Auth::guard('customers')->user();

        $orders = $this->order->with('products.categories')
            ->where('customer_id', $customer->id)
            ->orderBy('id', 'desc')->get();


        $favorites = $this->favorite
            ->with('favoritesable.mainImage', 'favoritesable.categories')
            ->where('customer_id', $customer->id)
            ->get();

        MetaTag::set('title', 'Личный кабинет');
        MetaTag::set('description', 'Личный кабинет пользователя');
        MetaTag::set('keywords', 'Личный кабинет пользователя');
        return view('frontend.account.profile', [
            'favorites' => $favorites,
            'customer' => $customer,
            'orders' => $orders,
        ]);
    }

    public function profileUpdate(CustomerRequest $request, $locale)
    {
        $customer = Auth::guard('customers')->user();
        $customer->update($request->all());

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('frontend.auth.profile', ['locale' => $locale])
        ]);
    }

    public function postLogin(LoginRequest $request, $locale)
    {
        if (!Auth::guard('customers')->attempt([
            'email' => $request->input('auth_email'),
            'password' => $request->input('auth_password'),
            'active' => 1
        ])) {
            return response()->json([
                'type' => 'alert',
                'alert_type' => 'error',
                'header' => 'Ошибка',
                'message' => 'Логин или пароль неверны или не активирован',
            ], '403');
        }

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('home', ['locale' => $locale])
        ]);
    }

    public function logout()
    {
        Auth::guard('customers')->logout();
        return redirect()->route('home', ['locale' => app()->getLocale()]);
    }

    public function confirm($code)
    {
//        var_dump($code);
//        die;

        $customer = $this->user->where('confirm_code', $code)->first();

        if ($customer) {

            Auth::guard('customers')->loginUsingId($customer->id, true);

            $customer->active = 1;
            $customer->confirm_code = null;
            $customer->save();

            return view('frontend.main');
        }

        return view('errors.404');
    }

    public function getEmail()
    {
        return view('frontend.account.get_email');
    }

    public function postEmail(Request $request, $locale)
    {
        $customer = $this->customer->where(['email' => $request->input('email'), 'active' => 1])->first();

        if($customer){

            $customer->restore_code = md5(uniqid());

            $customer->save();

            $this->changePasswordMail($customer, $locale);

            return response()->json([
                'type' => 'alert',
                'header' => 'Восстановление пароля',
                'alert_type' => 'success',
                'message' => 'Теперь перейдите на свой Email, для подтверждения',
                'url' => route('home', ['locale' => $locale])
            ]);
        }

        return response()->json([
            'type' => 'alert',
            'header' => 'Ошибка',
            'message' => 'Email не зарегистрирован или не активирован',
            'alert_type' => 'error'
        ], '400');

    }

    public function forgotPassword($locale, $code)
    {
        return view('frontend.account.change_password', ['code' => $code]);
    }

    public function changePassword(Request $request, $locale, $code)
    {
        $customer = $this->customer->where('restore_code', $code)->first();

        if ($customer) {
            $customer->password = $request->input('password');
            $customer->restore_code = null;
            $customer->save();

            return response()->json([
                'type' => 'alert',
                'header' => 'Вы успешно восстановили пароль',
                'alert_type' => 'success',
                'message' => 'Теперь можете авторизоваться',
                'url' => route('frontend.auth.login', ['locale' => $locale])
            ]);
        }

        return response()->json([
            'type' => 'alert',
            'header' => 'Ошибка',
            'message' => 'Не верный код подтверждения',
            'alert_type' => 'error'
        ], '400');

    }

}
