<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Products;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;
use Torann\LaravelMetaTags\Facades\MetaTag;

class CatalogController extends Controller
{
    private $categoryService;
    private $category;
    /**
     * @var Products
     */
    private $products;

    /**
     * CatalogController constructor.
     * @param Category $category
     * @param CategoryService $categoryService
     * @param Products $products
     */
    public function __construct(Category $category, CategoryService $categoryService, Products $products)
    {
        $this->category = $category;
        $this->categoryService = $categoryService;
        $this->products = $products;
    }

    public function show($locale, $slug)
    {
        $catalog = $this->category->with('medias', 'products', 'ancestors')
            ->descendantsAndSelf($this->category->where('slug', $slug)->first()->id)
            ->toTree()->first();

        if (!$catalog) {
            app()->abort(404, 'Страница не найдена');
        }

        MetaTag::set('title', $catalog->getTranslation('name', $locale));
        MetaTag::set('description', $catalog->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $catalog->getTranslation('meta_keywords', $locale));

        return view('frontend.catalog.show', [
            'catalog' => $catalog,
            'catalogAncestors' => $catalog->ancestors,
        ]);
    }

    public function products($locale,$catalogSlug)
    {
        $catalog = $this->category->with('medias','products.mainImage', 'ancestors')->where('slug',$catalogSlug)->first();
        $products = $catalog->products()->with('mainImage')->orderBy('created_at','desc')->paginate(8);
        if (!$catalog) {
            app()->abort(404, 'Not found');
        }
        MetaTag::set('title', $catalog->getTranslation('meta_title', $locale));
        MetaTag::set('description', $catalog->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $catalog->getTranslation('meta_keywords', $locale));
        return view('frontend.products.index',[
            'catalog' => $catalog,
            'products' => $products,
            'catalogAncestors' => $catalog->ancestors,
        ]);
    }


    public function getCatalog($locale)
    {
        $catalogs = $this->category
            ->with('medias')->where('owner', 'catalog')
            ->defaultOrder()->get()->toTree();

        $result = [];
        foreach ($catalogs as $catalog) {
            $result[$catalog->id][] = view('frontend.home.catalogs', ['catalog' => $catalog, 'locale' => $locale])->render();
        }
        return response()->json($result);
    }
}
