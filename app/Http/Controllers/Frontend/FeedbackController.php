<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\Feedback\SendMailForFeedback;
use App\Mail\Feedback\SendMailToAdminsAboutFeedback;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use App\Http\Requests\Frontend\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * @var Feedback
     */
    private $feedback;
    /**
     * @var Admin
     */
    private $admin;

    public function __construct(Feedback $feedback, Admin $admin)
    {
        $this->feedback = $feedback;
        $this->admin = $admin;
    }

    public function store(FeedbackRequest $request, $locale)
    {

        if ($request->has("butTest")) {
            if ($request->has('g-recaptcha-response')) {
                $url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";

                $secret_key = '6Lfe1YcUAAAAAMQPeyxBARG3Yf0mBUfzGmonuRVb';

                $query = $url_to_google_api . '?secret=' . $secret_key . '&response=' . $request->get('g-recaptcha-response') . '&remoteip=' . $_SERVER['REMOTE_ADDR'];

                $response = Curl::to($query)
                    ->withTimeout(5)
                    ->allowRedirect(true)
                    ->returnResponseObject()
                    ->get();

                $data = json_decode($response->content);

                if (isset($data->success)) {
                    if ($data->success) {

                        $feedback = $this->feedback->create($request->all());

                        Mail::to($feedback->email)->queue(New SendMailForFeedback($feedback));

                        $admins = $this->admin->where('active', 1)->get();
                        foreach ($admins as $admin) {
                            Mail::to($admin->email)->queue(New SendMailToAdminsAboutFeedback($admin->name));
                        }


//                        $array = [
//                            'name' => $request->name,
//                            'email' => $request->email,
//                            'message' => $request->message
//                        ];
//                        $feedback = $this->feedback->create($array);
//
//                        dd($feedback);
//                        Mail::to($feedback->email)->send(New SendMailForFeedback($feedback));
//
//                        DB::table('settings')->get();
//                        $adminsForFeedback = DB::table('admins')
//                            ->select('admins.id', 'admins.name', 'admins.email', 'permissions.alias')
//                            ->join('role_admins', 'role_admins.admin_id', '=', 'admins.id')
//                            ->join('roles', 'role_admins.role_id', '=', 'roles.id')
//                            ->join('role_permissions', 'role_permissions.role_id', '=', 'roles.id')
//                            ->join('permissions', 'role_permissions.permission_id', '=', 'permissions.id')
//                            ->where('permissions.alias', 'manage_admins')
//                            ->get();
//                        foreach ($adminsForFeedback as $admin) {
//                            Mail::to($admin->email)->send(New SendMailToAdminsAboutFeedback($admin->name));
//                        }

                        return redirect()->back()->with('message', 'Ваше сообщение успешно отправлено! Наши менеджера свяжутся с вами в ближайшее время.');
                    } else {
                        return redirect()->back()->withInput()->withErrors(['message' => 'Вы не прошли проверку "Я не робот"!']);
                    }
                }
            }
        } else {
            return redirect()->back()->withInput()->withErrors(['message' => 'Кнопка не нажата']);
        }
    }
}
