<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Media;
use App\Models\Page;
use App\Models\Products;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Torann\LaravelMetaTags\Facades\MetaTag;

class ProductController extends Controller
{
    /**
     * @var Category
     */
    private $category;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var Products
     */
    private $products;
    /**
     * @var Page
     */
    private $page;
    /**
     * @var Media
     */
    private $media;

    public function __construct(Category $category,
                                CategoryService $categoryService,
                                Products $products,
                                Page $page,
                                Media $media)
    {
        $this->category = $category;
        $this->categoryService = $categoryService;
        $this->products = $products;
        $this->page = $page;
        $this->media = $media;
    }

    public function show($locale, $catalogSlug, $slug)
    {
        $product = $this->products->with('mainImage', 'categories')->where('slug', $slug)->first();

        if (!$product) {
            app()->abort(404, 'Страница не найдена');
        }

        $categoryIds = $product->categories->pluck('id')->toArray();

        $related_product = $this->products
            ->with('mainImage', 'categories')
            ->whereHas('categories', function ($q) use($categoryIds){
                $q->whereIn('category_id', $categoryIds);
            })
            ->orderBy('created_at', 'desc')
            ->take(4)->get();

        $related_products = $related_product->shuffle();

        if($catalogSlug != 'catalogSlug'){
            $catalog = $this->category->with( 'ancestors')->where('slug',$catalogSlug)->first();
        } else {
            $catalog = $this->category->with( 'ancestors')->find($product->categories->first()->id);
        }

        MetaTag::set('title', $product->getTranslation('meta_title', $locale));
        MetaTag::set('description', $product->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $product->getTranslation('meta_keywords', $locale));
        return view('frontend.products.show', [
            'product' => $product,
            'related_products' => $related_products,
            'catalog' => $catalog,
            'catalogSlug' => $product->categories->first()->slug
        ]);
    }

    public function productModal($locale, $id)
    {
        return response()->json([
            'content' => view('frontend.products.modal', [
                'product' => $this->products->with('categories', 'mainImage')->find($id)
            ])->render()
        ]);
    }


    public function productSale($locale, $saleSlug, $slug){

        $product = $this->products->with('promotions', 'categories', 'mainImage')->where('slug', $slug)->first();

        if (!$product) {
            app()->abort(404, 'Страница не найдена');
        }

        $promotion = null;
        $promotionIds = [];
        foreach ($product->promotions as $item){
            $promotionIds[] = $item->id;
            if($item->slug == $saleSlug){
                $promotion = $item;
            }
        }

        $related_product = $this->products
            ->with('mainImage', 'promotions')
            ->whereHas('promotions', function ($q) use($promotionIds){
                $q->whereIn('promotion_id', $promotionIds);
            })
            ->orderBy('created_at', 'desc')
            ->take(4)->get();

        $related_products = $related_product->shuffle();

        MetaTag::set('title', $product->getTranslation('meta_title', $locale));
        MetaTag::set('description', $product->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $product->getTranslation('meta_keywords', $locale));
        return view('frontend.products.show', [
            'product' => $product,
            'related_products' => $related_products,
            'promotion' => $promotion,
            'catalogSlug' => $product->categories->first()->slug
        ]);
    }

//    public function singleProduct($locale,$slug)
//    {
//        $product = $this->products->where('slug',$slug)->with('mainImage','category')->first();
//        if (!$product) {
//            app()->abort(404, 'Not found');
//        }
//        $related_product = $this->products->with('mainImage')->orderBy('created_at', 'desc')->where('category_id', $product->category_id)->take(4)->get();
//        $related_products = $related_product->shuffle();
//        return view('frontend.products.show',[
//            'product'=>$product,
//            'related_products'=>$related_products
//        ]);
//    }
}
