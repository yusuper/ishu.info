<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\News;
use App\Models\Page;
use App\Models\Products;
use App\Models\Promotion;
use App\Models\PromotionProduct;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;
use Torann\LaravelMetaTags\Facades\MetaTag;

class HomeController extends Controller
{
    /**
     * @var Category
     */
    private $category;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var Products
     */
    private $products;
    /**
     * @var Page
     */
    private $page;
    /**
     * @var PromotionProduct
     */
    private $promotionProduct;
    /**
     * @var Promotion
     */
    private $promotion;
    /**
     * @var News
     */
    private $news;

    /**
     * HomeController constructor.
     * @param Category $category
     * @param PromotionProduct $promotionProduct
     * @param CategoryService $categoryService
     * @param Products $products
     * @param Page $page
     * @param Promotion $promotion
     * @param News $news
     */
    public function __construct(
        Category $category,
        PromotionProduct $promotionProduct,
        CategoryService $categoryService,
        Products $products,
        Page $page,
        Promotion $promotion,
        News $news)
    {
        $this->category = $category;
        $this->categoryService = $categoryService;
        $this->products = $products;
        $this->page = $page;
        $this->promotionProduct = $promotionProduct;
        $this->promotion = $promotion;
        $this->news = $news;
    }

    public function index()
    {

        return view('frontend.main');

//        $page = $this->page->with('mainImage')->where('prefix', null)->first();
//        if (!$page) {
//            app()->abort(404, 'Страница не найдена');
//        }
//
//        $hit_product = $this->products
//            ->with('mainImage', 'categories')
//            ->where(['site_display' => 1, 'hit' => 1])
//            ->orderBy('updated_at', 'desc')
////            ->take(10)
//            ->get();
//        $hit_products = $hit_product->chunk(2);
//
//        $promotions = $this->promotion->with('products')->where('site_display', 1)->orderBy('created_at', 'desc')->get();
//
//        $news = $this->news->orderBy('created_at', 'desc')->with('mainImage')->where('site_display', 1)->take(4)->get();
//
//        MetaTag::set('title', $page->getTranslation('meta_title', $locale));
//        MetaTag::set('description', $page->getTranslation('meta_description', $locale));
//        MetaTag::set('keywords', $page->getTranslation('meta_keywords', $locale));
//
//        return view('frontend.home.index', [
//            'hit_products' => $hit_products,
//            'promotions' => $promotions,
//            'page' => $page,
//            'news' => $news
//        ]);
    }
}
