<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ProductRequest;
use App\Models\Category;
use App\Models\Products;
use App\Models\Promotion;
use App\Services\CategoryService;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * @var Products
     */
    private $products;
    /**
     * @var MediaService
     */
    private $mediaService;

    private $owner = 'catalog';
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var Promotion
     */
    private $promotion;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Products $products,
                                MediaService $mediaService,
                                CategoryService $categoryService,
                                Promotion $promotion,
                                Category $category)
    {
        $this->products = $products;
        $this->mediaService = $mediaService;
        $this->categoryService = $categoryService;
        $this->promotion = $promotion;
        $this->category = $category;
    }

    public function index()
    {
        return view('backend.products.index', [
            'title' => 'Продукты'
        ]);
    }

    public function getList(Request $request)
    {
        $q = $this->products->with('categories')->orderBy('created_at', 'desc');

        if ($request->has('filter')) {
            foreach ($request->get('filter') as $field => $value) {
                switch ($field) {
                    case 'title':
                        if ($value) {
                            $q->where("title", 'like', "%$value%");
                        }
                        break;
                    case 'site_display':
                        if ($value != 'all') {
                            $intVal = ($value == 'yes') ? 1 : 0;
                            $q->where('site_display', $intVal);
                        }
                        break;
                }
            }
        }


        $products = $q->paginate(25);

        return response()->json([
            'tableData' => view('backend.products.list', [
                'items' => $products,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $products->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {

        $categories = $this->category->where('owner', $this->owner)->withDepth()->having('depth', '=', 2)->get();

        return response()->json([
            'functions' => ['initEditor', 'initSelect2'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание продукта',
            'modalContent' => view('backend.products.form', [
                'categories' => $categories,
                'create' => true,
                'formAction' => route('admin.products.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(ProductRequest $request)
    {
        $request->merge([
            'site_display' => $request->has('site_display'),
            'new' => $request->has('new'),
            'hit' => $request->has('hit')
        ]);

        $product = $this->products->create($request->all());

        if ($request->has('category_id')) {
            $product->categories()->attach($request->get('category_id'));
        }

        if ($request->hasFile('image')) {
            $this->mediaService->uploadImage($request->file('image'), null, 'products', $product->id, null, 1, null);
        }

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.products')
        ]);
    }

    public function edit($productId)
    {
        $product = $this->products->with('mainImage')->find($productId);
        $categories = $this->category->where('owner', $this->owner)->withDepth()->having('depth', '=', 2)->get();

        return response()->json([
            'functions' => ['initEditor', 'initSelect2'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание продукта',
            'modalContent' => view('backend.products.form', [
                'product' => $product,
                'categories' => $categories,
                'create' => false,
                'mainImage' => $product->mainImage,
                'mainImageUploadUrl' => route('admin.products.main.image.upload', ['id' => $productId]),
                'formAction' => route('admin.products.update', ['productId' => $productId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }


    public function update(ProductRequest $request, $productId)
    {
        $request->merge([
            'site_display' => $request->has('site_display'),
            'new' => $request->has('new'),
            'hit' => $request->has('hit')]);

        $product = $this->products->find($productId);
        $product->update($request->all());

        $categoriesIds = $request->has('category_id') ? $request->get('category_id') : [];
        $product->categories()->sync($categoriesIds);

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.products')
        ]);
    }

    public function destroy($productId)
    {
        $product = $this->products->with('mainImage')->find($productId);
        $this->mediaService->deleteMainImage('products', $productId);
        $product->categories()->sync([]);
        $product->delete();

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.products')
        ]);
    }

    public function uploadMainImage(Request $request, $productId)
    {
        if ($request->hasFile('mainImage')) {
            $this->mediaService->deleteMainImage('products', $productId);
            $this->mediaService->uploadImage($request->file('mainImage'), null, 'products', $productId, null, 1, null);
        }

        $product = $this->products->with('mainImage')->find($productId);

        return response()->json([
            'type' => 'updateBlock',
            'modal_content_url' => route('admin.media.get.crop', ['id' => $product->mainImage->id]),
            'modal' => '#largeModal',
            'blockSelector' => "#mainImagePlaceholder",
            'blockContent' => view('backend.products.partials.main_image', [
                'mainImage' => $product->mainImage
            ])->render()
        ]);
    }

    public function sortList()
    {
        $products = $this->products->with('categories')->orderBy('title')->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#regularModal',
            'modalTitle' => 'Настройка порядка',
            'modalContent' => view('backend.products.sort', [
                'products' => $products,
                'formAction' => route('admin.products.sort.update'),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function sortUpdate(Request $request)
    {
        $products = $request->input('sort');

        foreach ($products as $key => $value)
        {
            $product = $this->products->find($key);
            $product->update(['sort' => $value]);
        }

        return response()->json([
            'functions' => ['closeModal', 'reloadTable'],
            'modal_for_close' => '#regularModal',
            'type' => 'reloadTable',
            'tableId' => '#ajaxTable',
            'tableContentUrl' => route('admin.products.list')
        ]);
    }
}
