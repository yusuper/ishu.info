<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\PromotionRequest;
use App\Models\Products;
use App\Models\Promotion;
use App\Models\PromotionProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionController extends Controller
{
    /**
     * @var Promotion
     */
    private $promotion;
    /**
     * @var PromotionProduct
     */
    private $promotionProduct;
    /**
     * @var Products
     */
    private $products;

    public function __construct(Promotion $promotion,PromotionProduct $promotionProduct,Products $products)
    {
        $this->promotion = $promotion;
        $this->promotionProduct = $promotionProduct;
        $this->products = $products;
    }

    public function index()
    {
        $title = 'Акции';
        return view('backend.promotions.index',[
            'title'=>$title
        ]);
    }

    public function getList(Request $request)
    {
        $promotions = $this->promotion->with('products')->orderBy('created_at','desc')->paginate(25);
        return response()->json([
            'tableData' => view('backend.promotions.list', [
                'items' => $promotions,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $promotions->appends($request->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $create = true;

        return view('backend.promotions.form', [
            'title' => 'Создание категории продукта',
            'create' => $create,
            'formAction' => route('admin.promotions.store'),
            'buttonText' => 'Создать'
        ]);
    }

    public function store(PromotionRequest $request)
    {
        $request->merge([
            'site_display' => $request->has('site_display')
        ]);

        $this->promotion->create($request->all());


        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.promotions')
        ]);
    }

    public function edit($promotionId)
    {
        $create = false;
        $product_category = $this->promotion->where('id',$promotionId)->first();
        return view('backend.promotions.form', [
            'title' => 'Редактирование категории продукта',
            'create' => $create,
            'product_category'=>$product_category,
            'formAction' => route('admin.promotions.update',['promotionId'=>$promotionId]),
            'buttonText' => 'редактировать'
        ]);
    }

    public function update(PromotionRequest $request,$promotionId)
    {
        $request->merge([
            'site_display' => $request->has('site_display')
        ]);
        $promotion = $this->promotion->find($promotionId);
        $promotion->update($request->all());

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.promotions')
        ]);
    }



    public function destroy($promotionId)
    {
        $promotion = $this->promotion->find($promotionId);
        $promotion->products()->sync([]);
        $promotion->delete();

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.promotions')
        ]);
    }

    public function productCategoryCreate(Request $request, $productId)
    {
        $promotions = $this->promotion->orderBy('created_at','desc')->get();
        $route = route('admin.promotions.product.store', ['productId'=>$productId]);

        $product = $this->products->with('promotions')->find($productId);

        $ids = [];
        foreach($product->promotions as $promotion) {
            if (isset($promotion)) {
                $ids[$promotion->pivot->promotion_id][]  = $promotion->pivot->product_id;
            }
        }

        $title = 'Категории продукта '.$product->title;
        $data = [
            'type' => 'updateModal',
            'modal' => '#regularModal',
            'modalTitle' => $title,
            'modalContent' => view('backend.products.promotion_form', [
                'title' => 'Создание категорий',
                'promotions' => $promotions,
                'product'=>$product,
                'formAction' => $route,
                'buttonText' => 'Сохранить',
                'ids' => $ids
            ])->render(),
        ];

        return response()->json($data);
    }
    public function productCategoryStore($productId,Request $request){
        if(isset($request->checked)){
            $promotion = $this->promotionProduct->create([
                'promotion_id' => $request->promotionId,
                'product_id' => $productId
            ]);
            $data = [
                'type' => 'notify',
                'notify_message' => 'Продукт добавлен в категорию',
                'notify_type' => 'success'
            ];
            return response()->json($data);
        }
        $promotion = $this->promotionProduct->where([
            'promotion_id'=>$request->promotionId,
            'product_id'=>$productId
        ])->delete();
        $data = [
                'type' => 'notify',
                'notify_message' =>'Продукт удалён из категории',
                'notify_type'=>'success'
            ];
            return response()->json($data);
    }
}
