<?php

namespace App\Http\Controllers\Backend;

use App\Models\Service;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * @var Service
     */
    private $service;
    /**
     * @var ServiceCategory
     */
    private $serviceCategory;

    public function __construct(Service $service, ServiceCategory $serviceCategory)
    {
        $this->service = $service;
        $this->serviceCategory = $serviceCategory;
    }

    public function index()
    {
        $title = 'Услуги';
        return view('backend.services.index',[
            'title'=>$title
        ]);
    }

    public function getList()
    {

        $services = $this->service
            ->with('media','categories','user')
            ->orderBy('created_at','desc')
            ->paginate(20);
//        dd($announcements);
//        var_dump($services);
//        die;

        return response()->json([
            'tableData' => view('backend.services.list', [
                'services' => $services,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $services->appends($services->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function activate($serviceId)
    {
        $service = $this->service->find($serviceId);
        if($service->getOriginal('active') == 1){
            $service->active = 0;
            $service->update();
        }else{
            $service->active = 1;
            $service->update();
        }

        return response()->json([
            'type'=>'update-table-row',
            'table'=>'#ajaxTable',
            'row'=>'.row-'.$serviceId,
            'content' => view('backend.services.item',[
                'service'=>$service
            ])->render()
        ]);
    }

    public function vipToggle($serviceId)
    {
        $service = $this->service->find($serviceId);
        if($service->is_vip == 1){
            $service->is_vip = 0;
            $service->update();
        }else{
            $service->is_vip = 1;
            $service->update();
        }

        return response()->json([
            'type'=>'update-table-row',
            'table'=>'#ajaxTable',
            'row'=>'.row-'.$serviceId,
            'content' => view('backend.services.item',[
                'service'=>$service
            ])->render()
        ]);
    }


    public function destroy($serviceId)
    {
        $service = $this->service->with('media')->find($serviceId);
        $medias = $service->media;
        if ($medias->count() > 0) {
            foreach ($medias as $media) {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
        }
        $service->is_view = 0;
        $service->update();
        $service->delete();
        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $serviceId,
        ]);

    }

}
