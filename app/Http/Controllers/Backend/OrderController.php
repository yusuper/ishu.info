<?php

namespace App\Http\Controllers\Backend;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * @var Order
     */
    private $order;

    public function __construct(Order $order)
    {

        $this->order = $order;
    }

    public function index()
    {
        return view('backend.orders.index',[
            'title'=>'Заказы'
        ]);
    }

    public function getList()
    {
        $orders = $this->order->with('products')->orderBy('created_at','desc')->paginate(25);

        return response()->json([
            'tableData' => view('backend.orders.list', [
                'orders' => $orders,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $orders->appends($orders->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function close($orderId,Request $request)
    {
        $order = $this->order->where('id',$orderId)->first();
        $order->status = 1;
        $order->update();
        return response()->json([
            'type'=>'update-table-row',
            'table'=>'orderTable',
            'row'=>'.row-'.$orderId,
            'content'=>view('backend.orders.item',['order'=>$order])
        ])->render();
    }
}
