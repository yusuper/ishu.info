<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\ResponseTrait;
use App\Models\Media;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    use ResponseTrait;
    private $mediaService;
    private $media;

    public function __construct(MediaService $mediaService, Media $media)
    {
        $this->mediaService = $mediaService;
        $this->media = $media;
    }

    public function getModelMedia($owner, $modelId)
    {
        $media = $this->mediaService->getModelMedia($owner, $modelId);

        $data = [
            'modalTitle' => 'Управление изображениями',
            'modalContent' => view('backend.common.media.list', [
                'media' => $media->chunk(4),
                'formAction' => route('admin.media.model.upload', ['owner' => $owner, 'modelId' => $modelId]),
            ])->render()];

        return $this->responseJson($data);

    }

    public function addMediaForModel(Request $request, $owner, $modelId)
    {
        if ($request->hasFile('images'))
        {
            foreach ($request->file('images') as $image)
            {
                $this->mediaService->uploadImage($image, $owner, $modelId);
            }
        }

        $media = $this->mediaService->getModelMedia($owner, $modelId);

        $data = [
            'functions' => ['updateMediaList'],
            'media' => view('backend.common.media.images', [
                'media' => $media->chunk(4),
            ])->render()
        ];

        return $this->responseJson($data);
    }

    public function deleteMediaForModel($mediaId)
    {
        $this->mediaService->delete($mediaId);
    }

    public function setMediaMain($mediaId)
    {
        $this->mediaService->setMain($mediaId);
    }

    public function addMediaForEditor(Request $request)
    {
        if ($request->hasFile('images'))
        {
            foreach ($request->file('images') as $image)
            {
                $this->mediaService->uploadImage($image, null, 'editor');
            }
        }

        $images = $this->mediaService->getEditorMedia();

        $content = view('backend.common.media.editor.images.images', [
            'images' => $images->chunk(4)
        ])->render();

        return response()->json([
            'content' => $content
        ]);

    }

    public function addFilesForEditor(Request $request)
    {
        if ($request->has('files'))
        {
            foreach ($request->file('files') as $file)
            {
                $this->mediaService->uploadFile($file,  'editor');
            }
        }

        $files = $this->mediaService->getEditorMedia();

        $content = view('backend.common.media.editor.files.files', [
            'files' => $files
        ])->render();

        return response()->json([

            'content' => $content
        ]);
    }

    public function getEditorImages()
    {
        $images = $this->mediaService->getEditorMedia();

        return response()->json([
            'modalTitle' => 'Изображения',
            'content' => view('backend.common.media.editor.images.index', [
                'images' => $images->chunk(4)
            ])->render()

        ]);
    }

    public function getEditorFiles()
    {
        $files = $this->mediaService->getEditorMedia();

        return response()->json([

            'modalTitle' => 'Файлы',
            'content' => view('backend.common.media.editor.files.index', [
                'files' => $files
            ])->render()

        ]);
    }

    public function getCrop($id)
    {
        $media = $this->media->find($id);

        return response()->json([
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Обрезка изображения',
            'modalContent' => view('backend.settings.media.crop.index', [
                'media' => $media,

            ])->render()
        ]);
    }

    public function postCrop(Request $request, $id)
    {
        $width = round($request->input('width'));
        $height = round($request->input('height'));
        $offsetX = round($request->input('offset_x'));
        $offsetY = round($request->input('offset_y'));

        $media = $this->mediaService->cropExistingImage($id, $width, $height, $offsetX, $offsetY);

        if (!$media)
        {
            return response()->json([
                'type' => 'alert',
                'header' => 'Ошибка',
                'message' => $this->mediaService->errorMessage,
                'alert_type' => 'error'
            ]);
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#largeModal',
            'type' => 'updateBlock',
            'blockSelector' => "#mainImagePlaceholder",
            'blockContent' => view('backend.settings.media._material_main_image', [
                'mainImage' => $media
            ])->render()
        ]);
    }
}
