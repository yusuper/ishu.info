<?php

namespace App\Http\Controllers\Backend\Users\Customers;

use App\Http\Requests\Backend\UsersRequest;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CustomerController extends Controller
{
    private $customer;
    /**
     * @var User
     */
    private $user;

    public function __construct(Customer $customer, User $user)
    {
        $this->customer = $customer;
        $this->user = $user;
    }

    public function index()
    {
        $title = 'Список пользователей';

        return view('backend.users.customers.index', [
            'title' => $title,
        ]);
    }

    public function getList(Request $request)
    {
        $customers = $this->user->orderBy('created_at', 'desc')->paginate(25);

        return response()->json([
            'tableData' => view('backend.users.customers.list', [
                'items' => $customers,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $customers->appends($customers->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function edit($customerId)
    {
        $customer = $this->user->find($customerId);
//        dd($customer);
        return response()->json([
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование пользователя',
            'modalContent' => view('backend.users.customers.form', [
                'customer' => $customer,
                'formAction' => route('admin.users.customers.update', ['customerId' => $customerId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }


    public function update(UsersRequest $request, $customerId)
    {
        $request->merge(['active' => $request->has('active')]);

        $customer = $this->user->find($customerId);
        $customer->update($request->all());

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.users.customers')
        ]);
    }


    public function activate($userId)
    {
        $user = $this->user->find($userId);
        if ($user->getOriginal('active') == 1) {
            $user->active = 0;
            $user->update();
        } else {
            $user->active = 1;
            $user->update();
        }

        return response()->json([
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $userId,
            'content' => view('backend.users.customers.item', [
                'item' => $user
            ])->render()
        ]);
    }


    public function destroy($userId)
    {
        $user = $this->user->with('mainImage')->find($userId);
        $media = $user->mainImage;
        if ($media) {
            $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }

        $user->delete();
        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $userId,
        ]);

    }

}
