<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CatalogRequest;
use App\Models\Category;
use App\Models\Media;
use App\Services\CategoryService;
use App\Services\ImageService;
use App\Http\Controllers\Controller;
use App\Services\MediaService;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    private $category;
    private $categoryService;
    private $imageService;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * CatalogController constructor.
     * @param Category $category
     * @param CategoryService $categoryService
     * @param ImageService $imageService
     * @param Media $media
     * @param MediaService $mediaService
     */
    public function __construct(Category $category, CategoryService $categoryService, ImageService $imageService, Media $media, MediaService $mediaService)
    {
        $this->category = $category;
        $this->categoryService = $categoryService;
        $this->imageService = $imageService;
        $this->media = $media;
        $this->mediaService = $mediaService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.catalogs.index', [
            'title' => 'Каталог услуг',
            'catalogs' => $this->categoryService->getCatalogHtmlList('catalog', 'admin.catalog.show')
        ]);
    }

    public function getLiHtml(){
        return response()->json($this->categoryService->getCatalogHtmlList('catalog', 'admin.catalog.show'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        return response()->json([
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Создание каталога',
            'modalContent' => view('backend.catalogs.form', [
                'parentId' => $request->get('parentId'),
                'formAction' => route('admin.catalog.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    /**
     * @param CatalogRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(CatalogRequest $request)
    {
        $requestData = [
            'owner' => 'catalog',
            'name' => $request->input('name'),
            'meta_description' => $request->input('meta_description'),
            'meta_keywords' => $request->input('meta_keywords'),
            'meta_title' => $request->input('meta_title'),
        ];

        $category = $this->categoryService->createCategory($requestData, $request->get('parentId'));
        if ($request->hasFile('image')) {
            $this->mediaService->uploadImage($request->file('image'), 'image', 'categories', $category->id, null, 1, null);
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#largeModal',
            'type' => 'redirect',
            'redirect_url' => route('admin.catalog'),
        ]);
    }


    /**
     * @param $catalogId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($catalogId)
    {
        $catalog = $this->category->with('medias')->find($catalogId);
        return response()->json([
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Создание каталога',
            'modalContent' => view('backend.catalogs.form', [
                'parentId' => $catalog->parent_id,
                'catalog' => $catalog,
                'formAction' => route('admin.catalog.update', ['catalogId' => $catalogId]),
                'buttonText' => 'Изменить'
            ])->render()
        ]);
    }

    /**
     * @param CatalogRequest $request
     * @param $catalogId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CatalogRequest $request, $catalogId)
    {
        $requestData = [
            'name' => $request->input('name'),
            'meta_description' => $request->input('meta_description'),
            'meta_keywords' => $request->input('meta_keywords'),
            'meta_title' => $request->input('meta_title'),
        ];
        $catalog = $this->categoryService->updateCategory($catalogId, $requestData, $request->get('parentId'));

        if ($request->hasFile('image')) {

            $media = $this->media->where('owner', 'categories')->where('model_id', $catalogId)->first();
            if ($media) {
                $this->mediaService->deleteMediaItem($media->id, 'categories');
            }
            $this->mediaService->uploadImage($request->file('image'), 'image', 'categories', $catalog->id, null, 1, null);
        }


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#largeModal',
            'type' => 'redirect',
            'redirect_url' => route('admin.catalog'),
        ]);
    }

    /**
     * @param $catalogId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($catalogId)
    {
        $catalog = $this->category->with('descendants', 'products','medias')->find($catalogId);

        if($catalog->descendants->count() || $catalog->products->count()){
            return response()->json([
                'message_data' => [
                    'title'  => 'ошибка',
                    'text' => 'В катологе имеется подкатолог или продукт',
                    'type'    => 'error'
                ]
            ]);
        }

        $this->categoryService->deleteCategory($catalogId);

        $media = $this->media->where('owner', 'categories')->where('model_id', $catalogId)->first();
        if ($media) {
            $this->mediaService->deleteMediaItem($media->id, 'categories');
        }

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.catalog'),
        ]);
    }


    /**
     * @param $catalogId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($catalogId)
    {
        $catalog = $this->category->with('medias')->descendantsAndSelf($catalogId)->toTree()->first();

        $catalogs = $this->categoryService->getChildLiHtml($catalog, 'admin.catalog.show');

        if ($catalog->ancestors->count() < 2) {

            return view('backend.catalogs.show', [
                'title' => 'Каталог товаров категории ' . $catalog->getTranslation('name', 'ru'),
                'catalog' => $catalog,
                'catalogs' => $catalogs
            ]);
        } else {
            return view('backend.catalogs.product.index',[
                'title'=> $catalog->getTranslation('name', 'ru'),
                'categoryId' => $catalogId,
                'catalog' => $catalog,
                'catalogs' => $catalogs
            ]);
        }
    }

    public function position()
    {
        return response()->json($this->categoryService->getAjaxDataForModal('catalog'));
    }

    public function up(Request $request, $categoryId)
    {
        $this->categoryService->up($categoryId);

        return response()->json($this->categoryService->getAjaxDataForModal($request->input('owner')));
    }

    public function down(Request $request, $categoryId)
    {
        $this->categoryService->down($categoryId);

        return response()->json($this->categoryService->getAjaxDataForModal($request->input('owner')));
    }



    public function products(Request $request, $categoryId)
    {
        if ($request->has('position')){
            $category = $this->category->find($categoryId);

            $category->products()->sync($request->get('position'));
        }

        $category = $this->category->with('products')->find($categoryId);

        return response()->json([
            'tableData' => view('backend.catalogs.product.list', [
                'products' => $category->products,
            ])->render(),
        ]);
    }
}
