<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Http\Controllers\FormatTrait;
use App\Http\Requests\Backend\NewsCategoryRequest;
use App\Http\Requests\Backend\NewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Services\ImageService;
use App\Services\MediaService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use App\Services\CategoryService;

class NewsController extends Controller
{
    use ResponseTrait;
    use FormatTrait;

    private $news;

    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(News $news, MediaService $mediaService, CategoryService $categoryService)
    {
        $this->news = $news;
        $this->mediaService = $mediaService;
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = $this->news->orderBy('title')->get();

        return view('backend.content.news.index', [
            'title' => 'Список новостей',
            'news' => $news
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getList(Request $request)
    {
        $news = $this->news->with('mainImage')->orderBy('created_at', 'desc')->paginate(20);

        return response()->json([
            'tableData' => view('backend.content.news.list', [
                'news' => $news,
                'filters' => $request->all()
            ])->render(),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create()
    {
        $categories = $this->categoryService->categoriesForSelect('news');

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание новости',
            'modalContent' => view('backend.content.news.form', [
                'locales' => config('project.locales'),
                'categories' => $categories,
                'create' => true,
                'formAction' => route('admin.content.news.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(NewsRequest $request)
    {
        $request->merge(['site_display' => $request->input('site_display')]);

        $news = $this->news->create($request->all());

        if ($request->has('image')) {
            $thumbSizes = ['512', '256', '128'];
            $images = $request->image;
            $this->mediaService->uploadImage($images, 'image', "news", $news->id, $thumbSizes);
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.news.news_item', ['item' => $news])->render()
        ]);
    }

    /**
     * @param $itemId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($itemId)
    {
        $news = $this->news->find($itemId);
        $categories = $this->categoryService->categoriesForSelect('news');
        $medias = $news->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование новости',
            'modalContent' => view('backend.content.news.form', [
                'locales' => config('project.locales'),
                'categories' => $categories,
                'create' => false,
                'medias' => $medias,
                'news' => $news,
                'formAction' => route('admin.content.news.update', ['itemId' => $itemId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(NewsRequest $request, $itemId)
    {
        $news = $this->news->find($itemId);

        $request->merge(['site_display' => $request->has('site_display')]);
        $news->update($request->all());

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
            'content' => view('backend.content.news.news_item', ['item' => $news])->render()
        ]);
    }

    /**
     * @param $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($itemId)
    {
        $news = $this->news->find($itemId);
        $news->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
        ]);
    }

    private function fillSeoFields($newsItem)
    {
        $titleRu = $newsItem->getTranslation('title', 'ru');
        $titleEn = $newsItem->getTranslation('title', 'en');
        $titleKz = $newsItem->getTranslation('title', 'kz');

        $metaDescRu = $newsItem->getTranslation('meta_description', 'ru');
        $metaDescEn = $newsItem->getTranslation('meta_description', 'en');
        $metaDescKz = $newsItem->getTranslation('meta_description', 'kz');

        $metaDescription = [
            'ru' => (empty($metaDescRu)) ? $titleRu : $metaDescRu,
            'en' => (empty($metaDescEn)) ? $titleEn : $metaDescEn,
            'kz' => (empty($metaDescKz)) ? $titleKz : $metaDescKz
        ];

        $newsItem->meta_description = $metaDescription;

        $newsItem->save();
    }

    public function media(Request $request, MediaService $mediaService, $itemId)
    {
        $images = $request->image;
//        dd($images);
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image, 'image', "news", $itemId, $thumbSizes);
        }

        $news = $this->news->whereHas('media', function ($query) use ($itemId) {
            $query->where('model_id', $itemId)->orderBy('created_at', 'desc');
        })->get();
//        var_dump($news);
        return $this->responseJson([
            'media' => view('backend.content.news.media_list', ['news' => $news,])->render(),
        ]);
    }

    public function mainMedia($itemId, $mediaId)
    {
        $this->mediaService->mainMedia($itemId, $mediaId, 'news');
    }

    public function deleteMedia($mediaId)
    {
        $this->mediaService->deleteMediaItem($mediaId, 'news');
    }
}
