<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\FormatTrait;
use App\Http\Controllers\ResponseTrait;
use App\Http\Requests\Backend\PagesRequest;
use App\Models\Category;
use App\Models\Media;
use App\Models\Page;
use App\Services\CategoryService;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    use FormatTrait;
    use ResponseTrait;
    /**
     * @var Page
     */
    private $pages;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var Category
     */
    private $category;

    /**
     * PageController constructor.
     * @param Page $pages
     * @param Media $media
     * @param MediaService $mediaService
     * @param CategoryService $categoryService
     * @param Category $category
     */
    public function __construct(Page $pages, Media $media, MediaService $mediaService, CategoryService $categoryService, Category $category)
    {
        $this->pages = $pages;
        $this->media = $media;
        $this->mediaService = $mediaService;
        $this->categoryService = $categoryService;
        $this->category = $category;
    }

    public function index()
    {
        return view('backend.content.pages.index', [
            'title' => 'Страницы',
        ]);
    }

    public function getList(Request $request)
    {
        $pages = $this->pages->orderBy('created_at', 'desc')->paginate(20);

        return response()->json([
            'tableData' => view('backend.content.pages.list', [
                'items' => $pages
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $pages->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);

    }

    public function create()
    {
        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание страницы',
            'modalContent' => view('backend.content.pages.form', [
                'create' => true,
                'formAction' => route('admin.content.pages.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(PagesRequest $request, MediaService $mediaService)
    {
        $array = [
            'title' => $request->get('title'),
            'prefix' => $request->get('prefix'),
            'site_display' => $request->has('site_display'),
            'page_content' => $request->get('page_content'),
            'meta_keywords' => $request->get('meta_keywords'),
            'meta_description' => $request->get('meta_description'),
            'meta_title' => $request->get('meta_title'),
        ];

        $page = $this->pages->create($array);

        if ($request->has('videos') && $request->videos != null) {
            $videos = explode("\r\n", $request->videos);
            foreach ($videos as $video) {
                $mediaService->uploadVideo($video, 'pages', $page->id);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.pages.pages_item', ['page' => $page])->render()
        ]);
    }


    public function addMenu($pageId)
    {
        $page = $this->pages->with('menus')->find($pageId);

        $rootMenuIds = [];
        foreach ($page->menus as $menu) {
            $rootMenuIds[] = $menu->ancestors->first()->id;
        }

        $menusArray = [];
        $rootMenus = $this->category->where('owner', 'menus')->whereIsRoot()->get();
        foreach ($rootMenus as $menu) {
            $children = '';
            if (!in_array($menu->id, $rootMenuIds)) {
                $menuTree = $this->category->descendantsAndSelf($menu->id)->toTree()->first();
                $children = $this->buildCategoriesForSelect($menuTree->children);
            }
            $menusArray[] = [
                'rootMenu' => $menu,
                'children' => $children,
            ];
        }

        return response()->json([
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Добавление в меню',
            'modalContent' => view('backend.content.pages.add_menu_form', [
                'menusArray' => $menusArray,
                'page' => $page,
                'formAction' => route('admin.content.pages.add_menu.store', ['id' => $pageId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function storeAddMenu(Request $request, $pageId)
    {
        $page = $this->pages->with('menus.ancestors')->find($pageId);
        $array = [
            'name' => $page->toArray()['title'],
            'url' => $page->prefix,
            'owner' => 'menus',
            'page_id' => $page->id
        ];

        foreach ($request->get('category_id') as $rootId => $parentId) {
            if ($parentId) {
                $this->categoryService->createCategory($array, $parentId);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
        ]);

    }

    public function edit($pageId)
    {
        $page = $this->pages->with('media')->find($pageId);
        $medias = $page->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.content.pages.form', [
                'medias' => $medias,
                'create' => false,
                'page' => $page,
                'formAction' => route('admin.content.pages.update', ['id' => $pageId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(PagesRequest $request, $pageId, MediaService $mediaService)
    {
        $page = $this->pages->with('menus')->find($pageId);

        $array = [
            'title' => $request->get('title'),
            'prefix' => $request->get('prefix'),
            'site_display' => $request->has('site_display'),
            'page_content' => $request->get('page_content'),
            'meta_keywords' => $request->get('meta_keywords'),
            'meta_description' => $request->get('meta_description'),
            'meta_title' => $request->get('meta_title'),
        ];

        $page->update($array);

        foreach ($page->menus as $menu){
            $menu->name = $request->get('title');
            $menu->url = $request->get('url');
            $menu->save();
        }

        if ($request->has('videos') && $request->videos != null) {
            $videos = explode("\r\n", $request->videos);
            foreach ($videos as $video) {
                $mediaService->uploadVideo($video, 'video', "pages", $pageId);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $pageId,
            'content' => view('backend.content.pages.pages_item', ['page' => $page])->render()
        ]);

    }

    public function media(Request $request, MediaService $mediaService, $pageId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image, 'image', "pages", $pageId, $thumbSizes);
        }

        $pages = $this->pages->whereHas('media', function ($query) use ($pageId) {
            $query->where('model_id', $pageId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.content.pages.media_list',
                [
                    'pages' => $pages,
                ])->render(),
        ]);
    }

    public function mainMedia($pageId, $mediaId)
    {
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'pages')
            ->where('model_id', $pageId)->get();

        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'pages')->where('model_id', '=', $pageId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService, $mediaId)
    {
        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
        $mediaService->editVideo($image, $mediaId, $thumbSizes);

    }

    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'pages')->find($mediaId);
        if ($media->type == 'image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }

    public function destroy($pageId)
    {
        $medias = $this->media->where(['owner' => 'pages', 'model_id' => $pageId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $page = $this->pages->find($pageId);
        $menu = $this->category->where('page_id',$pageId)->first();
//        dd($menu);
        $menu->delete();
        $page->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $pageId,
        ]);

    }
}
