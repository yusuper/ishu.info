<?php

namespace App\Http\Controllers\Backend;


use App\Models\Announcement;
use App\Models\AnnouncementCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnnouncementController extends Controller
{
    /**
     * @var Announcement
     */
    private $announcement;

    public function __construct(Announcement $announcement,
                                AnnouncementCategory $announcementCategory)
    {
        $this->announcement = $announcement;
    }

    public function index()
    {
        $title = 'Заявки';
        return view('backend.announcements.index', [
            'title' => $title
        ]);
    }

    public function getList()
    {
        $announcements = $this->announcement
            ->with('mainImage', 'media', 'categories', 'user')
            ->orderBy('created_at', 'desc')
            ->paginate(20);
//        dd($announcements);
//        foreach($announcements as $announcement)
//        {
//            var_dump($announcement->mainImage);
//            die;
//        }


        return response()->json([
            'tableData' => view('backend.announcements.list', [
                'announcements' => $announcements,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $announcements->appends($announcements->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function activate($announcementId)
    {
        $announcement = $this->announcement->find($announcementId);
        if ($announcement->getOriginal('active') == 1) {
            $announcement->active = 0;
            $announcement->update();
        } else {
            $announcement->active = 1;
            $announcement->update();
        }

        return response()->json([
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $announcementId,
            'content' => view('backend.announcements.item', [
                'announcement' => $announcement
            ])->render()
        ]);
    }

    public function destroy($announcementId)
    {
        $announcement = $this->announcement->with('media')->find($announcementId);
        $medias = $announcement->media;
        if ($medias->count() > 0) {
            foreach ($medias as $media) {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
        }
        $announcement->is_view = 0;
        $announcement->update();
        $announcement->delete();
        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $announcementId,
        ]);

    }

}
