<?php

namespace App\Http\Controllers\Api;

use App\Services\MediaService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CustomerTrait;
use App\Http\Controllers\ResponseTrait;
use App\Http\Requests\Backend\UsersRequest;
use App\Http\Requests\Frontend\CustomerRequest;
use App\Http\Requests\Frontend\LoginRequest;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Models\City;
use App\Models\Favorite;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\ResponseTrait as ApiTrait;

// models
use App\Models\Region;
use App\Models\Customer;
use Torann\LaravelMetaTags\Facades\MetaTag;

class AuthController extends Controller
{
    use ResponseTrait;
    use CustomerTrait;
    use ApiTrait;
    use RequestValidationTrait;

    private $city;
    private $region;
    private $subscriber;

    /**
     * @var Favorite
     */
    private $favorite;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var User
     */
    private $user;
    /**
     * @var MediaService
     */
    private $mediaService;

    public function __construct(Region $region,
                                User $user,
                                MediaService $mediaService,
                                City $city, Favorite $favorite,
                                Order $order)
    {
        $this->city = $city;
        $this->region = $region;
        $this->favorite = $favorite;
        $this->order = $order;
        $this->user = $user;
        $this->mediaService = $mediaService;
    }


    public function getRegister()
    {
        $regions = $this->region->get();
        $cities = $this->city->get();
        return $this->apiResponse([
            'regions' => $regions,
            'cities' => $cities
        ]);
    }

    public function postRegister(RegisterRequest $request)
    {
//        var_dump($request->all());
//        die;
        $request->merge([
            'confirm_code' => md5(uniqid()),
            'api_token' =>Str::random(60)
        ]);

        $user = $this->registerCustomer($request->all());

        $this->sendConfirmMail($user);

        return response()->json([
            'type' => 'alert',
            'alert_type' => 'success',
            'header' => 'Спасибо за регистрацию!',
            'message' => 'Перейдите на свою электронную почту для подтверждения регистрации',
            'url' => route('home')
        ]);
    }

//    public function getLogin()
//    {
//        MetaTag::set('title', 'Регистрация');
//        MetaTag::set('description', 'Регистрация Авторизация пользователя');
//        MetaTag::set('keywords', 'Регистрация Авторизация пользователя');
//        return view('frontend.account.login');
//    }


    public function login(Request $request)
    {
        $validationRules = [
            'email' => 'required',
            'password' => 'required'
        ];

//        dd($request->all());

        $validator = $this->validateRequest($request->all(), $validationRules);

//        dd($validator->fails());

        if ($validator->fails())
        {
            return $this->apiError(422, 'Параметры запроса не верные', $validator->errors());
        }

//        dd(Auth::guard('users')->attempt([
//            'email' => $request->input('email'),
//            'password' => $request->input('password'),
//            'active' => 1
//        ]));
//        $user ='';
        $user = $this->user->with('mainImage')
            ->where('email', $request->input('email'))
//            ->where('password', $request->input('password'))
            ->where('active','=',1)
            ->first();
        if($user && Hash::check($request->input('password'), $user->password)){
            $user->api_token = Str::random(60);
            $user->save();
            if($user->mainImage){
                $mainImage = $user->mainImage;
            }else{
                $mainImage = '';
            };
        }else{
            return $this->apiError(403, 'Пользователь не найден или неактивен', $validator->errors());
        }


        return $this->apiResponse([
            'user_id' => $user->id,
            'full_name'=>$user->full_name,
            'main_image'=>$mainImage,
            'api_token'=>$user->api_token,
            'email'=>$user->email,
            'phone' => $user->phone
        ]);
    }



//TODO this part auth
    public function profile(Request $request,$userId)
    {
        $user = $this->user->with('mainImage')->find($userId);

        $mainImage = $user->mainImage;
        $user = $this->user->select('full_name','email','phone')->find($userId);


        return response()->json([
            'type' => 'success',
            'userData'=>$user,
            'mainImage'=>$mainImage
        ]);
    }

    public function profileUpdate(Request $request, $userId)
    {
        $user = $this->user->with('mainImage')->find($userId);
//        dd($user);
//        if(!isset($request->email)){
//            return $this->apiError(403, 'Введите данные');
//        }
        $media = $user->mainImage;
        $thumbSizes = ['128'];
        if($request->has('image'))
        {
//            dd(123);
            if(isset($media)){

                $this->mediaService->deleteMediaItem($media->id, 'users');
            }

            $this->mediaService->uploadImage($request->image,'image','users', $user->id,$thumbSizes,1);

        }
        if(!($request->has('email')) && !($request->has('full_name')) && !($request->has('phone')))
        {
            $user = $this->user->with('mainImage')->select('id','full_name','email','phone','api_token')->find($userId);
            $mainImage = $user->mainImage;
            return $this->apiResponse([
                'user_id' => $user->id,
                'full_name'=>$user->full_name,
                'main_image'=>$user->mainImage,
                'api_token'=>$user->api_token,
                'email'=>$user->email,
                'phone' => $user->phone
            ]);
        }
        $array = [
            'full_name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
        ];
        $user->update($array);

        $user = $this->user->with('mainImage')->select('id','full_name','email','phone','api_token')->find($userId);
        $mainImage = $user->mainImage;
//        dd($user);

        return $this->apiResponse([
            'user_id' => $user->id,
            'full_name'=>$user->full_name,
            'main_image'=>$mainImage,
            'api_token'=>$user->api_token,
            'email'=>$user->email,
            'phone' => $user->phone
        ]);
    }

//    public function postLogin(LoginRequest $request, $locale)
//    {
//        if (!Auth::guard('customers')->attempt([
//            'email' => $request->input('auth_email'),
//            'password' => $request->input('auth_password'),
//            'active' => 1
//        ])) {
//            return response()->json([
//                'type' => 'alert',
//                'alert_type' => 'error',
//                'header' => 'Ошибка',
//                'message' => 'Логин или пароль неверны или не активирован',
//            ], '403');
//        }
//
//        return response()->json([
//            'type' => 'redirect',
//            'redirect_url' => route('home', ['locale' => $locale])
//        ]);
//    }

    public function logout()
    {
        Auth::guard('customers')->logout();
        return redirect()->route('home', ['locale' => app()->getLocale()]);
    }

    public function confirm($code)
    {
//        var_dump($code);
//        die;
        $user = $this->user->where('confirm_code', $code)->first();
//        var_dump($user);
//        die;

        if ($user) {

            Auth::guard('customers')->loginUsingId($user->id, true);

            $user->active = 1;
            $user->confirm_code = null;
            $user->update();

            return response()->json('Вы успешно актививировали профиль');
        }

        return response()->json('Ваш профиль уже активирован');
    }

    public function getEmail()
    {
        return view('frontend.account.get_email');
    }

    public function postEmail(Request $request, $locale)
    {
        $user = $this->user->where(['email' => $request->input('email'), 'active' => 1])->first();

        if($user){

            $user->restore_code = md5(uniqid());

            $user->save();

            $this->changePasswordMail($user, $locale);

            return response()->json([
                'type' => 'alert',
                'header' => 'Восстановление пароля',
                'alert_type' => 'success',
                'message' => 'Теперь перейдите на свой Email, для подтверждения',
                'url' => route('home', ['locale' => $locale])
            ]);
        }

        return response()->json([
            'type' => 'alert',
            'header' => 'Ошибка',
            'message' => 'Email не зарегистрирован или не активирован',
            'alert_type' => 'error'
        ], '400');

    }

    public function forgotPassword($locale, $code)
    {
        return view('frontend.account.change_password', ['code' => $code]);
    }

    public function changePassword(Request $request, $locale, $code)
    {
        $user = $this->user->where('restore_code', $code)->first();

        if ($user) {
            $user->password = $request->input('password');
            $user->restore_code = null;
            $user->save();

            return response()->json([
                'type' => 'alert',
                'header' => 'Вы успешно восстановили пароль',
                'alert_type' => 'success',
                'message' => 'Теперь можете авторизоваться',
                'url' => route('frontend.auth.login', ['locale' => $locale])
            ]);
        }

        return response()->json([
            'type' => 'alert',
            'header' => 'Ошибка',
            'message' => 'Не верный код подтверждения',
            'alert_type' => 'error'
        ], '400');

    }
}
