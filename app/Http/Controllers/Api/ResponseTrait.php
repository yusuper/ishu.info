<?php
namespace App\Http\Controllers\Api;

trait ResponseTrait
{
    public function apiResponse($res, $additional = [])
    {
        return response()->json([
            'errors' => false,
            'code' => null,
            'message' => null,
            'additional' => $additional,
            'data' => $res
        ]);
    }

    public function apiError($code, $message, $data = [])
    {
        return response()->json([
            'errors' => true,
            'code' => $code,
            'message' => $message,
            'data' => $data
        ]);
    }
}