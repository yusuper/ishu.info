<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\City;
use App\Models\Region;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Region
     */
    private $region;
    /**
     * @var City
     */
    private $city;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var Service
     */
    private $service;

    public function __construct(User $user, Region $region, City $city, Category $category, Service $service)
    {
        $this->user = $user;
        $this->region = $region;
        $this->city = $city;
        $this->category = $category;
        $this->service = $service;
    }

    public function index()
    {
//        $regions = $this->region->get();

        $user = $this->user->where('active',1)->with('mainImage')->get();
        $categories = $this->category
            ->with('children','medias','services')
            ->where('owner','catalog')
            ->where('parent_id','=',null)
            ->get();
//        dd($categories);
        $menu = $this->category->with('children','services')->where('owner','menus')->get();
//        $services = $categories->services;
        $cities = $this->city->get();

        return response()->json([
            'alert_type'=>'success',
//            'regions'=>$regions,
            'cities'=>$cities,

//            'user'
            'categories'=>$categories,
            'menu'=>$menu
//            'services'=>$services
        ]);
    }

    public function getCategoryChildren($parentId)
    {
        $children = $this->category->where('parent_id','=',$parentId)->get();

        if(count($children) > 0) {
            return response()->json([
                'categoryChildren'=>$children
            ]);
        }

        return response()->json([
            'errors' => true,
            'code' => '404',
            'message' => 'Not found',
        ]);
    }

    public function main()
    {
        $vipServices = $this->service->with('mainImage','categories')->where(['is_vip'=>1,'active'=>1])->orderBy('created_at','desc')->get()/*->random(3)*/;

        $recentServices = $this->service->with('mainImage','categories')->where(['active'=>1,'is_view'=>1])->orderBy('created_at','desc')->take(8)->get();
        $cities = $this->city->get();

        return response()->json([
            'vipServices'=>$vipServices,
            'recentServices'=>$recentServices,
            'cities'=>$cities,
        ]);
    }

}
