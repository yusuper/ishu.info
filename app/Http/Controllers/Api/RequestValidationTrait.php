<?php

namespace App\Http\Controllers\Api;


use Illuminate\Support\Facades\Validator;

trait RequestValidationTrait
{
    use RequestValidationTrait;
    public function validateRequest(array $request, array $rules)
    {
//        dd($rules);
        return Validator::make($request, $rules);
    }
}
