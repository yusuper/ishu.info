<?php

namespace App\Http\Controllers\Api;

use App\Models\Announcement;
use App\Models\AnnouncementCategory;
use App\Models\Category;
use App\Models\City;
use App\Models\Media;
use App\Models\Service;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AnnouncementController extends Controller
{
    use RequestValidationTrait;
    /**
     * @var Announcement
     */
    private $announcement;
    /**
     * @var City
     */
    private $city;
    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var AnnouncementCategory
     */
    private $announcementCategory;
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var Service
     */
    private $service;

    public function __construct(Announcement $announcement,
                                City $city,
                                MediaService $mediaService,
                                Category $category,
                                AnnouncementCategory $announcementCategory,
                                Carbon $carbon,
                                Media $media,
                                Service $service)
    {
        $this->announcement = $announcement;
        $this->city = $city;
        $this->mediaService = $mediaService;
        $this->category = $category;
        $this->announcementCategory = $announcementCategory;
        $this->carbon = $carbon;
        $this->media = $media;
        $this->service = $service;
    }

    public function create()
    {
        $cities = $this->city->get();
//        var_dump($cities->toArray());
//        die;
        return response()->json([
            'cities' => $cities
        ]);
    }

    public function store(Request $request)
    {
//        var_dump($request->image);
//        die;
        $validationRules = [
//            'title' => 'required',
            'category' => 'required',
//            'user_id' => 'required',
            'phone' => 'required',
            'city_id' => 'required',
            'desc' => 'required'
        ];

        $validator = $this->validateRequest($request->all(), $validationRules);

        if ($validator->fails()) {
            return $this->apiError(422, 'Параметры запроса не верные', $validator->errors());
        }
        $category = $this->category->find($request->category[0]);
//        dd($category);

        $add_string = $category->name . ' на ishu.info';


        $keywords = explode(' ', $request->desc);
        $keyword = implode(',', $keywords) . $add_string;


        $user_id = $request->user_id ? $request->user_ud : 0;

        $phone = preg_replace( '/[^0-9]/', '', $request->phone );

        $announcement = $this->announcement->create([
            'desc'=>$request->desc,
            'title'=>$request->title,
//            'category_id' => $request->category_id,
            'user_id' => $user_id,
            'phone' => $phone,
            'is_view'=>1,
            'city_id' => $request->city_id,
            'meta_title' => $category->name,
            'meta_keywords' => $keyword,
            'meta_description' => $request->desc
        ]);

        foreach($request->category as $category){
            $this->announcementCategory->create([
                'category_id'=>$category,
                'announcement_id'=>$announcement->id
            ]);
        }

        $thumbSizes = ['512', '256'];
        $i = 0;
        if ($request->has('image')){
            $images = $request->image;
            foreach($images as $image){
                if($i==0){
                    $this->mediaService->uploadImage($image,'image','announcements', $announcement->id,$thumbSizes,1);
                }else{
                    $this->mediaService->uploadImage($image,'image','announcements', $announcement->id,$thumbSizes);
                }
                $i++;
            }
        }

        return response()->json('OK');

    }

    public function getUserAnnouncements($userId,$status)
    {
//        dd($userId);
        if($status == 'active'){
            $userAds = $this->announcement
                ->with('mainImage','categories')
                ->where('is_view','=',1)
                ->where('active','=',1)
                ->where('user_id',$userId)
                ->orderBy('created_at','desc')
                ->paginate(10);
        }elseif($status == 'closed'){
            $userAds = $this->announcement
                ->onlyTrashed()
                ->with('mainImage','categories')
                ->where('is_view','=',0)
                ->where('active','=',1)
                ->where('user_id',$userId)
                ->orderBy('created_at','desc')
                ->paginate(10);
        }elseif($status == 'awaiting'){
            $userAds = $this->announcement
                ->with('mainImage','categories')
                ->where('active','!=',1)
                ->where('user_id','=',$userId)
                ->orWhereNull('active')
                ->orderBy('created_at','desc')
                ->paginate(10);
        }elseif($status == 'rejected'){
            $userAds = $this->announcement
                ->onlyTrashed()
                ->with('mainImage','categories')
                ->where('active','!=',1)
                ->where('is_view','!=',1)
                ->where('user_id','=',$userId)
                ->orWhereNull('active')
                ->orderBy('created_at','desc')
                ->paginate(10);
        }
//        var_dump($userServices);
//        die;
        return response()->json([
            'userAds'=>$userAds
        ]);
    }



    public function announcementUp($announcementId)
    {
        $announcement = $this->announcement->find($announcementId);

        $now = $this->carbon->now()/*->setTimezone('Asia/Bishkek')*/->addDay()->addHour(6);

        $up = $this->carbon::parse($announcement->up);

        if($announcement->up == null || $now->diffInDays($up) >= 1 )
        {
            $announcement->up = $now->format('Y/m/d H:i:s');
            $announcement->update();
            return response()->json([
                'status'=>'OK'
            ]);
        }
        return response()->json([
            'status'=>'Error'
        ]);
    }

    public function announcementStop($announcementId)
    {
        $announcement = $this->announcement->find($announcementId);
        if($announcement && $announcement->is_view != 0){
            $announcement->is_view = 0;
            $announcement->update();
        }else{
            $announcement->is_view = 1;
            $announcement->update();
        }

        $is_view = $announcement->is_view == 1 ? true : false;

        return response()->json([
            'is_view'=>$is_view
        ]);
    }


    public function announcementDelete($announcementId)
    {
        $medias = $this->media->where(['owner'=>'announcement','model_id'=>$announcementId])->get();
//        dd($medias);
        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }

        $announcement = $this->announcement->find($announcementId);
        $announcement->is_view = 0;
        $announcement->update();
        if($announcement){
            $announcement->delete();
            return 'OK';
        }
        return 'FALSE';
    }

    public function getAdsForUser($userId)
    {
        $userServices = $this->service
            ->with('categories')
            ->where('user_id',$userId)
            ->get();
//        dd($userServices);

        $adIds = [];

        foreach($userServices as $service){

//            die;
//            dd($ad->categories);
            foreach($service->categories as $key => $category){
//                dd($category);
                $adIds[] += $category->id;
            }
        }
        $categoryIds = array_values(array_unique($adIds));

        $userAds = $this->announcement
            ->with('categories')
            ->whereHas('categories',function($q) use ($categoryIds){
                $q->whereIn('category_id',$categoryIds);
            })->paginate(10);

        return response()->json([
            'userAds'=>$userAds
        ]);
//        dd($userAds);
    }

}
