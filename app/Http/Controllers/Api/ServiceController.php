<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\City;
use App\Models\Media;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\User;
use App\Models\UserServiceRating;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ServiceController extends Controller
{
    use ResponseTrait;
    use RequestValidationTrait;
    /**
     * @var Service
     */
    private $service;
    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var City
     */
    private $city;
    /**
     * @var ServiceCategory
     */
    private $serviceCategory;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var UserServiceRating
     */
    private $userServiceRating;
    /**
     * @var User
     */
    private $user;

    public function __construct(Service $service,
                                ServiceCategory $serviceCategory,
                                Category $category,
                                MediaService $mediaService,
                                City $city,
                                Carbon $carbon,
                                Media $media,
                                UserServiceRating $userServiceRating,
                                User $user)
    {
        $this->service = $service;
        $this->mediaService = $mediaService;
        $this->city = $city;
        $this->serviceCategory = $serviceCategory;
        $this->category = $category;
        $this->carbon = $carbon;
        $this->media = $media;
        $this->userServiceRating = $userServiceRating;
        $this->user = $user;
    }

    public function create(Request $request)
    {
        return response()->json([
            'currencies'=>config('project.currencies')
        ]);
    }

    public function store(Request $request)
    {
//        var_dump($request->all());
//        die;
        $validationRules = [
            'title' => 'required',
            'category' => 'required',
            'user_id' => 'required',
            'phone' => 'required',
            'city_id' => 'required',
            'desc'=>'required'
        ];

        $validator = $this->validateRequest($request->all(), $validationRules);

        if ($validator->fails()) {
//            return $this->apiError(422, 'Параметры запроса не верные', $validator->errors());
            return 'Неверные параметры запроса';
        }

        $category = $this->category->find($request->category[0]);

        $add_string = $category->name .' на ishu.info';


        $keywords = explode(' ',$request->title);
        $keyword = implode(',',$keywords).$add_string;
//        dd($keyword);
        $phone = preg_replace( '/[^0-9]/', '', $request->phone );
        $service = $this->service->create([
                'title' =>$request->title,
                'desc'=>$request->desc,
                'user_id'=>$request->user_id,
                'phone'=>$phone,
                'city_id' => $request->city_id,
                'price'=>$request->price,
                'discount'=>$request->discount,
                'meta_title'=>$request->title,
                'meta_keywords'=>$keyword,
                'meta_description'=> $request->desc,
                'currency'=>$request->currency,
                'is_view'=>1
            ]
        );


        foreach($request->category as $category){
            $this->serviceCategory->create([
                'category_id'=>$category,
                'service_id'=>$service->id
            ]);
        }

        $thumbSizes = ['512', '256'];
        $i = 0;
        if ($request->has('image')){
//            var_dump($request->image);
//            die;
            $images = $request->image;
            foreach($images as $image){
                if($i==0){
                    $this->mediaService->uploadImage($image,'image','services', $service->id,$thumbSizes,1);
                }else{
                    $this->mediaService->uploadImage($image,'image','services', $service->id,$thumbSizes);
                }
                $i++;
            }
        }

        return response()->json('OK');
    }

    public function getCategoryServices($categoryId)
    {
        $categoryServices = $this->service
            ->with('mainImage','categories'/*,'rating'*/)
            ->where('active','=',1)
            ->where('is_view','=',1)
            ->orderBy('up','desc')
            ->orderBy('created_at','desc')
            ->whereHas('categories',function ($q) use ($categoryId) {
                $q->where('category_id',$categoryId);
            })
            ->paginate(12);


        $categoryServices->map(function ($service) {

//            $service['rating'] = 0;
            $serviceRatingAmount = 0;
//            $serviceRatingCount = 0;
            $userServiceRating = $this->userServiceRating->where(['service_id'=>$service->id])->get();
            if(count($userServiceRating)) {
//                dd($userServiceRating);
                foreach($userServiceRating as $rating) {
//                    $serviceRatingCount++;
                    $serviceRatingAmount+= $rating->rating;
                }
                $service['rating'] = intval(ceil($serviceRatingAmount / count($userServiceRating)));

            }elseif(!count($userServiceRating)){
                $service['rating'] = 0;

            }
//            dd($service);
        });

//        dd($categoryServices);

//        dd(ceil($serviceRatingAmount / $serviceRatingCount));

        $category = $this->category->with('children')->find($categoryId);
        $categoryName = $category->name;
        $categoryChildren = $category->children;
//        $categoryServices = $serviceName;
//        dd($serviceName);

        return response()->json([
            'categoryServices'=>$categoryServices,
            'categoryName'=> $categoryName,
            'categoryChildren'=>$categoryChildren
        ]);
    }


    public function getServiceItem(Request $request,$serviceId)
    {
        $header = $request->header('Authorization');
        $service = $this->service->with('media','mainImage','categories','user','city')->find($serviceId);
        $user = $this->user->where('api_token',$header)->first();
//        var_dump($header);
//        die;
        if(isset($header) && $user)
        {
//            $user = $this->user->where('api_token',$header)->first();
//            var_dump($this->user->where('api_token',$header)->first(),$service->id);
//            die;
            $userService = $this->userServiceRating->where(['user_id'=>$user->id,'service_id'=>$service->id])->first();

            if(isset($userService))
            {
                $service['voted'] = true;
            }
            else
            {
                $service['voted'] = false;
            }
        } else {
            $service['voted'] = true;
        }

        $serviceCategoryArray = [];
        foreach($service->categories as $category) {
//            dd($category->id);
            $serviceCategoryArray[] = $category->id;
        }
//        dd($serviceCategoryArray);
        $relatedServices =$this->service->whereHas('categories',function($q) use ($serviceCategoryArray) {
//            dd($q->whereIn('id',$serviceCategoryArray));
        })->with('mainImage')->get();
        //TODO
        if(count($relatedServices) >= 8) {
            $relatedServices = $relatedServices->random(8);
        }else{
            $relatedServices = 0;
        }
//        var_dump($relatedServices instanceof Service);
//        die;

        if(($relatedServices instanceof Service)) {
            $relatedServices->map(function ($service) {
                $serviceRatingAmount = 0;
                $userServiceRating = $this->userServiceRating->where(['service_id'=>$service->id])->get();
                if(count($userServiceRating)) {
                    foreach($userServiceRating as $rating) {
                        $serviceRatingAmount+= $rating->rating;
                    }
                    $service['rating'] = intval(ceil($serviceRatingAmount / count($userServiceRating)));

                }elseif(!count($userServiceRating)){
                    $service['rating'] = 0;
                }
            });
        }


        $serviceRating = $this->userServiceRating->where(['service_id'=>$service->id])->get();
        $serviceRatingAmount = 0;
        if(count($serviceRating)) {
            foreach($serviceRating as $rating) {
                $serviceRatingAmount+= $rating->rating;
            }
            $service['rating'] = intval(ceil($serviceRatingAmount / count($serviceRating)));
        }elseif(!count($serviceRating)){
            $service['rating'] = 0;
        }
//        var_dump($service);
//        die;

        return response()->json([
            'service'=>$service,
            'relatedServices'=>$relatedServices
        ]);
    }

    public function getUserServices($userId,$status)
    {

        if($status == 'active'){
            $userServices = $this->service
                ->with('mainImage','categories')
                ->where('is_view','=',1)
                ->where('active','=',1)
                ->where('user_id',$userId)
                ->orderBy('created_at','desc')
                ->paginate(10);
        }elseif($status == 'inactive'){
            $userServices = $this->service
                ->with('mainImage','categories')
                ->where('is_view','=',0)
                ->where('active','=',1)
                ->where('user_id',$userId)
                ->orderBy('created_at','desc')
                ->paginate(10);
        }elseif($status == 'awaiting'){
            $userServices = $this->service
                ->with('mainImage','categories')
                ->where('active','!=',1)
                ->where('user_id','=',$userId)
                ->orWhereNull('active')
                ->orderBy('created_at','desc')
                ->paginate(10);
        }
//        var_dump($userServices);
//        die;
        return response()->json([
            'userServices'=>$userServices
        ]);
    }

    public function serviceStop($serviceId)
    {
        $service = $this->service->find($serviceId);
        if($service && $service->is_view != 0){
            $service->is_view = 0;
            $service->update();
        }else{
            $service->is_view = 1;
            $service->update();
        }

        $is_view = $service->is_view == 1 ? true : false;

        return response()->json([
            'is_view'=>$is_view
        ]);
    }


    public function serviceUp($serviceId)
    {
        $service = $this->service->find($serviceId);
//        dd(date('Y/m/d h:i:s'));
        $now = $this->carbon->now()->setTimezone('Asia/Bishkek')->addDay();
//        $now =$now->subDay();

        $yesterday = $now->subDay(1)->format('Y-m-d h:i:s');
        $up = $this->carbon::parse($service->up);
//        dd();
        if($service->up == null || $now->diffInDays($up) > 0 )
        {
            $service->up = $now->format('Y/m/d H:i:s');
            $service->update();
            $service = $this->service->find($serviceId);
            return response()->json([
                'service'=>$service
            ]);
        }
        return $this->apiError('404','Поднятие недоступно в данный момент');
    }


    public function serviceUpdate(Request $request,$serviceId)
    {
        var_dump($serviceId,$request);
//        die;
        $service = $this->service->find($serviceId);
        if(!isset($service)) {
            return $this->apiError('404','Не найдено');
        }

        $service->desc = $request->desc;
        $service->phone = $request->phone;
        $service->update();
        $service = $this->service->find($serviceId);

        return $this->apiResponse($service);
    }


    public function serviceDelete($serviceId)
    {
        $medias = $this->media->where(['owner'=>'services','model_id'=>$serviceId])->get();
//        dd($medias);
        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }

        $service = $this->service->find($serviceId);
        if($service){
            $service->delete();
            return 'OK';
        }
        return 'FALSE';
    }

    public function serviceRating(Request $request,$serviceId,$userId)
    {
//        dd($request->all());
//        dd($serviceId);
        $serviceUserRating = $this->userServiceRating->where(['user_id'=>$userId,'service_id'=>$serviceId])->first();
        if($serviceUserRating){
            $rating = $this->userServiceRating->where('service_id','=',$serviceId)->get();
            $serviceRating = 0;
            foreach($rating as $item) {
                $serviceRating+=$item->rating;
            }
            $serviceRating = ceil($serviceRating / $rating->count());
//            dd($serviceRating);
//            $this->userServiceRating->user_id = $userId;
//            $this->userServiceRating->service_id = $serviceId;
//            $this->userServiceRating->rating = $request->rating;
//            $this->userServiceRating->update();
            return $this->apiError('404','Вы уже проголосовали за этот продукт',$serviceRating);
//            return response()->json([
//                $this->apiResponse($serviceUserRating)
//            ]);
        }
        $data = [
            'user_id'=>$userId,
            'service_id'=>$serviceId,
            'rating'=>$request->rating
        ];

        $serviceUserRating = $this->userServiceRating->create($data);

        $rating = $this->userServiceRating->where('service_id','=',$serviceId)->get();
        $serviceRating = 0;
        foreach($rating as $item) {
            $serviceRating+=$item->rating;
        }
        $serviceRating = ceil($serviceRating / $rating->count());
//        dd($serviceRating);

//        $serviceUserRating = $this->userServiceRating->where(['user_id'=>$userId,'service_id'=>$serviceId])->first();
//        dd($serviceUserRating);

        return $this->apiResponse($serviceRating);
    }

}
