<?php

namespace App\Providers;

//use App\Services\Sphinx\Engine\SphinxSearchEngine;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        resolve(EngineManager::class)->extend('sphinx', function () {
//            return new SphinxSearchEngine;
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
