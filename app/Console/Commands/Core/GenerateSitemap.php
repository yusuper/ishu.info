<?php

namespace App\Console\Commands\Core;


use App\Models\Category;
use App\Models\News;
use App\Models\Page;
use App\Models\Products;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:sitemap-generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $siteMap = Sitemap::create();

        foreach (config('project.locales') as $locale) {
            $siteMap->add(Url::create(route('home', ['locale' => $locale]))->setLastModificationDate(Carbon::now()));
            Page::where('site_display', 1)->get()->each(function (Page $page) use ($siteMap, $locale) {
                $siteMap->add(Url::create(route('pages', ['locale' => $locale, 'prefix' => $page->prefix]))->setLastModificationDate(Carbon::now()));
            });

            Category::withDepth()->where('owner', 'catalog')->get()->each(function (Category $category) use ($siteMap, $locale) {
                if ($category->depth > 1) {
                    $siteMap->add(Url::create(route('products.catalog', ['locale' => $locale, 'catalogSlug' => $category->slug]))->setLastModificationDate(Carbon::now()));
                } else {
                    $siteMap->add(Url::create(route('show.catalog', ['locale' => $locale, 'slug' => $category->slug]))->setLastModificationDate(Carbon::now()));
                }
            });

            Products::with('categories')->where('site_display', 1)->get()->each(function (Products $product) use ($siteMap, $locale) {
                $siteMap->add(Url::create(route('show.product', ['locale' => $locale, 'catalogSlug' => $product->categories->first()->slug, 'slug' => $product->slug]))->setLastModificationDate(Carbon::now()));
            });

            News::where('site_display', 1)->get()->each(function (News $news) use ($siteMap, $locale) {
                $siteMap->add(Url::create(route('frontend.news.show', ['locale' => $locale, 'prefix' => $news->slug]))->setLastModificationDate(Carbon::now()));
            });
        }
        $siteMap->writeToFile(public_path('sitemap.xml'));
    }
}
