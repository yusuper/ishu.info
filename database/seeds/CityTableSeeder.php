<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityTableSeeder extends Seeder
{
    private $city;

    public function __construct(\App\Models\City $city)
    {
        $this->city = $city;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'region_id' => 2,
                'name' => [
                    'ru' => 'Баткен',
                    'en' => 'Batken',
                    'kg' => 'Баткен',
                ],
            ],
//
//            [
//                'region_id' => 2,
//                'name' => [
//                    'ru' => 'Кызыл-Кия',
//                    'en' => 'Kyzyl-Kiya',
//                    'kg' => 'Кызыл-Кыя',
//                ],
//            ],
//
//            [
//                'region_id' => 2,
//                'name' => [
//                    'ru' => 'Сулюкта',
//                    'en' => 'Sulukta',
//                    'kg' => 'Сүлүктү',
//                ],
//            ],
//
//            [
//                'region_id' => 2,
//                'name' => [
//                    'ru' => 'Исфана',
//                    'en' => 'Isfana',
//                    'kg' => 'Исфана',
//                ],
//            ],
//
//            [
//                'region_id' => 2,
//                'name' => [
//                    'ru' => 'Айдаркен',
//                    'en' => 'Aydarken',
//                    'kg' => 'Айдаркен',
//                ],
//            ],
//
//            [
//                'region_id' => 2,
//                'name' => [
//                    'ru' => 'Кадамжай',
//                    'en' => 'Kadamjai',
//                    'kg' => 'Кадамжай',
//                ],
//            ],

            [
                'region_id' => 3,
                'name' => [
                    'ru' => 'Жалал-Абад',
                    'en' => 'Jalal-Abad',
                    'kg' => 'Жалал-Абад',
                ],
            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Кара-Куль',
//                    'en' => 'Kara-Kul',
//                    'kg' => 'Кара-Көл',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Кербен',
//                    'en' => 'Kerben',
//                    'kg' => 'Кербен',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Кок-Жангак',
//                    'en' => 'Kok-Zhangak',
//                    'kg' => 'Кок-Жангак',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Кочкор-Ата',
//                    'en' => 'Kochkor-Ata',
//                    'kg' => 'Кочкор-Ата',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Майлуу-Суу',
//                    'en' => 'Mailuu-Suu',
//                    'kg' => 'Майлуу-Суу',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Масси',
//                    'en' => 'Masy',
//                    'kg' => 'Масы',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Сузак',
//                    'en' => 'Suzak',
//                    'kg' => 'Сузак',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Таш-Кумыр',
//                    'en' => 'Tash-Kumyr',
//                    'kg' => 'Таш-Көмүр',
//                ],
//            ],

//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Токтогул',
//                    'en' => 'Toktogul',
//                    'kg' => 'Токтогул',
//                ],
//            ],
//
//            [
//                'region_id' => 3,
//                'name' => [
//                    'ru' => 'Шамалды-Сай',
//                    'en' => 'Shamaldy-Say',
//                    'kg' => 'Шамалды-Сай',
//                ],
//            ],

            [
                'region_id' => 4,
                'name' => [
                    'ru' => 'Балыкчи',
                    'en' => 'Balykchy',
                    'kg' => 'Балыкчы',
                ],
            ],

            [
                'region_id' => 4,
                'name' => [
                    'ru' => 'Каракол',
                    'en' => 'Karakol',
                    'kg' => 'Каракол',
                ],
            ],

            [
                'region_id' => 4,
                'name' => [
                    'ru' => 'Чолпон-Ата',
                    'en' => 'Cholpon-Ata',
                    'kg' => 'Чолпон-Ата',
                ],
            ],
//
//            [
//                'region_id' => 4,
//                'name' => [
//                    'ru' => 'Ак-Суу',
//                    'en' => 'Ak-Suu',
//                    'kg' => 'Ак-Суу',
//                ],
//            ],
//
//            [
//                'region_id' => 4,
//                'name' => [
//                    'ru' => 'Каджи-Сай',
//                    'en' => 'Kaji-Say',
//                    'kg' => 'Кажы-Сай',
//                ],
//            ],

            [
                'region_id' => 5,
                'name' => [
                    'ru' => 'Нарын',
                    'en' => 'Naryn',
                    'kg' => 'Нарын',
                ],
            ],

//            [
//                'region_id' => 5,
//                'name' => [
//                    'ru' => 'Кочкорка',
//                    'en' => 'Kochkor',
//                    'kg' => 'Кочкор',
//                ],
//            ],
//
//            [
//                'region_id' => 5,
//                'name' => [
//                    'ru' => 'Ат-Башы',
//                    'en' => 'At-Bashy',
//                    'kg' => 'Ат-Башы',
//                ],
//            ],
//
//            [
//                'region_id' => 5,
//                'name' => [
//                    'ru' => 'Чаек',
//                    'en' => 'Chaek',
//                    'kg' => 'Чаек',
//                ],
//            ],

            [
                'region_id' => 6,
                'name' => [
                    'ru' => 'Ош',
                    'en' => 'Osh',
                    'kg' => 'Ош',
                ],
            ],
//
//            [
//                'region_id' => 6,
//                'name' => [
//                    'ru' => 'Узген',
//                    'en' => 'Uzgen',
//                    'kg' => 'Өзгөн',
//                ],
//            ],

//            [
//                'region_id' => 6,
//                'name' => [
//                    'ru' => 'Ноокат',
//                    'en' => 'Nookat',
//                    'kg' => 'Ноокат',
//                ],
//            ],

            [
                'region_id' => 7,
                'name' => [
                    'ru' => 'Талас',
                    'en' => 'Talas',
                    'kg' => 'Талас',
                ],
            ],

//            [
//                'region_id' => 7,
//                'name' => [
//                    'ru' => 'Бакай-Ата',
//                    'en' => 'Bakai-Ata',
//                    'kg' => 'Бакай-Ата',
//                ],
//            ],
//
//            [
//                'region_id' => 7,
//                'name' => [
//                    'ru' => 'Кара-Буура',
//                    'en' => 'Kara-Buura',
//                    'kg' => 'Кара-Буура',
//                ],
//            ],
//
//            [
//                'region_id' => 7,
//                'name' => [
//                    'ru' => 'Манас',
//                    'en' => 'Manas',
//                    'kg' => 'Манас',
//                ],
//            ],

//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Кант',
//                    'en' => 'Kant',
//                    'kg' => 'Кант',
//                ],
//            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Кара-Балта',
//                    'en' => 'Kara-Balta',
//                    'kg' => 'Кара-Балта',
//                ],
//            ],

            [
                'region_id' => 8,
                'name' => [
                    'ru' => 'Токмок',
                    'en' => 'Tokmok',
                    'kg' => 'Токмок',
                ],
            ],

            [
                'region_id' => 8,
                'name' => [
                    'ru' => 'Бишкек',
                    'en' => 'Bishkek',
                    'kg' => 'Бишкек',
                ],
            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Сокулук',
//                    'en' => 'Sokuluk',
//                    'kg' => 'Сокулук',
//                ],
//            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Каинды',
//                    'en' => 'Kaindy',
//                    'kg' => 'Каинды',
//                ],
//            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Орловка',
//                    'en' => 'Orlovka',
//                    'kg' => 'Орловка',
//                ],
//            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Шопоков',
//                    'en' => 'Shopokov',
//                    'kg' => 'Шопоков',
//                ],
//            ],
//
//            [
//                'region_id' => 8,
//                'name' => [
//                    'ru' => 'Кемин',
//                    'en' => 'Kemin',
//                    'kg' => 'Кемин',
//                ],
//            ],

        ];

        $this->city->truncate();

        foreach ($cities as $city) {
            $this->city->create($city);
        }
    }
}
