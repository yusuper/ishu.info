<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * @var \App\Models\News
     */
    private $news;

    public function __construct(\App\Models\News $news)
    {
        $this->news = $news;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = [
            [
                'title' => [
                    'ru' => 'Новости',
                    'en' => 'News',
                    'kg' => 'Жанылыктар',
                ],
                'short_content' => [
                    'ru' => 'Краткое содержание',
                    'en' => 'Short content',
                    'kg' => 'Кыскача жанылык',
                ],
                'long_content' => [
                    'ru' => 'Полное содержание',
                    'en' => 'Long content',
                    'kg' => 'Бардык жанылык',
                ],
                'meta_title' => [
                    'ru' => 'Новости',
                    'en' => 'News',
                    'kg' => 'Жанылык',
                ],
                'meta_keywords' => [
                    'ru' => 'Новости',
                    'en' => 'News',
                    'kg' => 'Жанылык',
                ],
                'meta_description' => [
                    'ru' => 'Описание',
                    'en' => 'Description',
                    'kg' => 'Баяндама',
                ],
                'site_display' => 1,
                'category_id' => 1,
                'slug' => 'news'

            ],
        ];

        $this->news->truncate();

        foreach ($news as $new){
            $this->news->create($new);
        }
    }
}
