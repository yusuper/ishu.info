<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * @var \App\Models\Admin
     */
    private $admin;

    public function __construct(\App\Models\Admin $admin)
    {
        $this->admin = $admin;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->admin->truncate();

        $this->admin->create([
            'name' => 'Dev',
            'email' => 'dev@dev.dev',
            'password' => 'dev',
            'super_user' => true
        ]);
    }
}
