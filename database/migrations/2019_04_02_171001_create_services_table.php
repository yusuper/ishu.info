<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->json('title')->nullable();
            $table->json('full_name')->nullable();
            $table->json('profession')->nullable();
            $table->json('short_desc')->nullable();
            $table->json('desc')->nullable();
            $table->text('slug')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('active')->nullable();
            $table->boolean('is_vip')->default(0);
            $table->unsignedInteger('price')->nullable();
            $table->unsignedInteger('discount')->nullable();
            $table->unsignedBigInteger('views')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->timestamp('up')->nullable();
//            $table->json('address')->nullable();
//            $table->text('phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
