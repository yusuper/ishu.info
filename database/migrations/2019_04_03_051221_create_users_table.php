<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('region_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('full_name', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('phone', 64)->nullable();
            $table->string('address', 64)->nullable();
            $table->string('email', 255)->unique();
            $table->string('password', 100)->nullable();
            $table->text('whatsapp')->nullable();
            $table->text('telegram')->nullable();
            $table->char('gender')->nullable();
            $table->string('avatar')->nullable();
            $table->date('birth_date')->nullable();
            $table->boolean('active')->default(0);
            $table->rememberToken();
            $table->string('api_token',80)->nullable()->default(null);
            $table->string('confirm_code', 32)->nullable();
            $table->string('restore_code', 32)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
